<?php $this->breadcrumbs = array('Thông tin giao hàng'); ?>
<div class="row">

    <div class="span3 progress">
        <h3>Quy trình thanh toán</h3>
        <ul class="rr">
            <li>
                <a href="#">Hình thức thanh toán</a>
            </li>
            <li>
                <a href="#">Thông tin giao hàng</a>
            </li>
            <li>
                <a href="#">Xác nhận đơn hàng</a>
            </li>
        </ul>
    </div>
  
    <div class="span9 checkout-list">
        <h3>Thanh toán</h3>
        <ol class="rr">
            <li>
                <h6>Hình thức thanh toán</h6>
                <div class="row">
                    <div class="span9 content-wrapper">
                        Nội dung
                    </div>
                </div>
            </li>

            <li class="current">
                <h6>Thông tin giao hàng</h6>
                <div class="row">
                    <div class="span9">
                
                        <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id'=>'checkout-form',
                                'action' => Yii::app()->createUrl('checkout/info'),
                                'enableClientValidation'=>true,
                                'clientOptions'=>array(
                                    'validateOnSubmit'=>true,
                                ),
                                'htmlOptions' => array('class' => 'checkout-form')
                            ));
                        ?>
                        <div class="span2 label-input"><span>Họ tên khách hàng</span></div>
                        <div class="span6">
                            <?php echo $form->textField($model,'customer_name', array('placeholder' => 'Tên của bạn ...', 'class' => 'text-input')); ?>
                            <?php echo $form->error($model,'customer_name'); ?>
                        </div>

                        <div class="span2 label-input"><span>Email</span></div>
                        <div class="span6">
                            <?php echo $form->textField($model,'email', array('placeholder' => 'Email...', 'class' => 'text-input')); ?>
                            <?php echo $form->error($model,'email'); ?>
                        </div>

                        <div class="span2 label-input"><span>Số điện thoại</span></div>
                        <div class="span6">
                            <?php echo $form->textField($model,'phone', array('placeholder' => 'Số điện thoại ...', 'class' => 'text-input')); ?>
                            <?php echo $form->error($model,'phone'); ?>
                        </div>

                        <div class="span2 label-input"><span>Địa chỉ giao hàng</span></div>
                        <div class="span6">
                            <?php echo $form->textField($model,'shipping_address', array('placeholder' => 'Địa chỉ giao hàng ...', 'class' => 'text-input')); ?>
                            <?php echo $form->error($model,'shipping_address'); ?>
                        </div>

                        <div class="span2 label-input"><span>Người nhận</span></div>
                        <div class="span6">
                            <?php echo $form->textField($model,'delivery_person', array('placeholder' => 'Tên người nhận ...', 'class' => 'text-input')); ?>
                            <?php echo $form->error($model,'delivery_person'); ?>
                        </div>

                        <div class="span2 label-input"><span>Thanh toán</span></div>
                        <div class="span6">
                            <div class="payment-method">
                                <?php echo $form->radioButton($model,'payment_method', array('value' => Order::PAYMENT_METHOD_CASH, 'uncheckValue' => NULL)); ?> Trực tiếp khi giao hàng
                                <?php echo $form->radioButton($model,'payment_method', array('value' => Order::PAYMENT_METHOD_TRANSFER, 'uncheckValue' => NULL)); ?> Chuyển khoản

                            </div>
                            <?php echo $form->error($model,'payment_method'); ?>
                        </div>

                        <div class="span2 label-input"><span>Ghi chú</span></div>
                        <div class="span6">
                            <?php echo $form->textField($model,'note', array('placeholder' => 'Ghi chú ...', 'class' => 'text-input')); ?>
                        </div>
                  
                        <div class="span2"></div>
                        <div class="span6">
                            <a href="javascript:void(0);" class="sbtn sbtn-normal lnk-submit">
                              <span class="gradient">Tiếp tục</span>
                            </a>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </li>
      
            <li class="last">
            <h6>Xác nhận đơn hàng</h6>
                <div class="row">
                    <div class="span9 content-wrapper">
                        Content
                    </div>
                </div>
            </li>

        </ol>
    </div>
</div>