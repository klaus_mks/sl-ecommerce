<?php
    $info = Yii::app()->session['cart-info'];
?>
<?php if(!empty($product)): ?>
<?php 
    $totalValue = 0; 
    Yii::app()->clientScript->registerScriptFile('/js/shopping-cart.js', CClientScript::POS_BEGIN);
?>

<?php $this->breadcrumbs = array(Yii::t('front', 'Orders review')); ?>

<div class="row">

    <div class="span3 progress">
        <h3><?php echo Yii::t('front', 'Checkout progress'); ?></h3>
        <ul class="rr">
            <li>
                <a href="#"><?php echo Yii::t('front', 'Checkout method'); ?></a>
            </li>
            <li>
                <a href="#"><?php echo Yii::t('front', 'Shipping information'); ?></a>
            </li>
            <li>
                <a href="#"><?php echo Yii::t('front', 'Orders review'); ?></a>
            </li>
        </ul>
    </div>
  
    <div class="span9 checkout-list">
        <h3><?php echo Yii::t('front', 'Checkout'); ?></h3>
        <ol class="rr">
            <li>
                <h6><?php echo Yii::t('front', 'Checkout method'); ?></h6>
                <div class="row">
                    <div class="span9 content-wrapper">
                        Nội dung
                    </div>
                </div>
              </li>
            <li>
                <h6><?php echo Yii::t('front', 'Shipping information'); ?></h6>
                <div class="row">
                    <div class="span9 content-wrapper">
                        Nội dung
                    </div>
                </div>
            </li>
            <li class="current">
                <h6><?php echo Yii::t('front', 'Orders review'); ?></h6>
                <div class="row cart">
                    <div class="span9">
                    <!-- cart content -->
                    <table class="checkout-cart">
                        <tbody>
                      
                      
                    <?php foreach( $cartSession as $cs ): ?>
                    <?php
                        $item = ShoppyHelper::getRecordById( $cs['id'], $product );
                        $img = $item->getActiveProductImage( $item->productImages, 138, 179);
                        if(empty($img)) $img = ShoppyHelper::getNoImage( 138, 179 );
                        $quantity = $cs['quantity'];
                        $totalValue += $quantity * $item->price;
                    ?>
                            <tr>
                            <td class="article clearfix" colspan="3">
                              
                              <figure>
                                <img src="<?php echo $img; ?>" alt="<?php echo $item->name; ?>">
                              </figure>
                              
                              <div class="info-wrapper">
                                <h2><?php echo $item->name; ?></h2>
                                <?php if(!empty($cs['attribute'])) : ?>
                                  <div class="info">
                                    <?php foreach( $cs['attribute'] as $i => $attributeId ): ?>
                                    <?php 
                                      $attrList = CHtml::listData( $attribute, 'id', 'name' );
                                      $attrValList = CHtml::listData( $attributeValue, 'id', 'value' );
                                    ?>
                                        <p class="clearfix">
                                          <span><?php echo $attrList[$attributeId]; ?>:</span>
                                          <span class="value"><?php echo $attrValList[$cs['attribute_value'][$i]]; ?></span>
                                        </p>
                                        <?php endforeach; ?>
                                      </div>
                                  <?php endif; ?>
                                  </div>
                                  
                                </td>
                            </tr>
                            <tr class="headers">
                                <th class="alpha16">
                                  <?php echo Yii::t('front', 'Quantity'); ?>
                                </th>
                                <th class="alpha16 dark">
                                  <?php echo Yii::t('front', 'Unit price'); ?>
                                </th>
                                <th class="alpha16">
                                  <?php echo Yii::t('front', 'Total'); ?>
                                </th>
                            </tr>

                            <tr>
                                <td class="quantity dark">
                                
                                  <div class="quant-input">
                                    <?php echo $quantity; ?>
                                  </div>
                                  
                                </td>
                                <td class="price">
                                
                                  <span class="currency"></span><span class="value"><?php echo number_format($item->price); ?></span>
                                
                                </td>
                                <td class="price dark">
                                
                                  <span class="currency"></span><span class="value"><?php echo number_format($quantity * $item->price); ?></span>
                                
                                </td>
                            </tr>
                        <?php endforeach; ?>
                      
                        <tr>
                            <td colspan="2" class="empty">
                            </td>
                            <td colspan="2" class="total-wrapper">
                                <div class="total clearfix">
                                    <div class="half-col">
                                        <?php echo Yii::t('front', 'Total'); ?>
                                    </div>
                                    <div class="half-col value-wrapper">
                                        <span class="currency"></span><span class="value"><?php echo number_format($totalValue); ?></span>
                                    </div>
                                </div>
                            
                            </td>
                        </tr>
                        </tbody>
                    </table> 
                    <!-- end cart content -->                     
                </div>
            </div>

            <!-- shipping content  -->
            <div class="row">
                <div class="span9 checkout-shipping-detail">
                    
                    <div class="span2">Tên khách hàng</div>  
                    <div class="span6">
                        <?php echo $info['customer_name']; ?>
                    </div>                          

                    <div class="span2">Email</div>  
                    <div class="span6">
                        <?php echo $info['email']; ?>
                    </div>

                    <div class="span2">Số điện thoại</div>  
                    <div class="span6">
                        <?php echo $info['phone']; ?>
                    </div>

                    <div class="span2">Địa chỉ giao hàng</div>  
                    <div class="span6">
                        <?php echo $info['shipping_address']; ?>
                    </div>

                    <div class="span2">Người nhận</div>  
                    <div class="span6">
                        <?php echo $info['delivery_person']; ?>
                    </div>

                    <?php if(!empty($info['note'])): ?>
                    <div class="span2">Ghi chú</div>  
                    <div class="span6">
                        <?php echo $info['note']; ?>
                    </div>
                    <?php endif; ?>
                    
                    <div class="span2"></div>  
                    <div class="span6">
                      <form action="<?php echo Yii::app()->createUrl('checkout/finish'); ?>" method="post">
                        <a href="javascript:void(0);" class="btn secondary lnk-submit">
                          <span class="gradient">Hoàn tất</span>
                        </a>
                        </form>
                    </div>
                    
                </div>                      
            </div>

          </li>
          
        </ol>
    </div>

</div>
<?php endif; ?>