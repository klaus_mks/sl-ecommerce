<h4>Cảm ơn bạn đã mua hàng</h4>
<?php if($order->payment_method == Order::PAYMENT_METHOD_TRANSFER && !empty($bankAccount)): ?>
	Bạn vui lòng chuyển khoản vào <?php if(count($bankAccount) > 1) echo 'một trong những'; ?> tài khoản sau:<br/>
		---------------------------------------------<br/>
	<?php foreach( $bankAccount as $b ): ?>
		Ngân hàng: <?php echo $b->bank_name; ?><br/>
		Chủ tài khoản: <?php echo $b->name; ?><br/>
		Số tài khoản: <?php echo $b->number; ?><br/>
		---------------------------------------------<br/>
	<?php endforeach; ?>
		Vui lòng gửi kèm mã đơn hàng của bạn: <?php echo strtoupper($order->code); ?> trong thông tin chuyển khoản<br/>
		Thông tin chi tiết đơn hàng của bạn đã được gửi đến hộp mail <?php echo $order->email; ?>, nếu bạn chưa nhận được email, vui lòng kiểm tra trong thư mục Spam<br/>
		---------------------------------------------<br/>
<?php endif; ?>
<h4>Chúng tôi sẽ kiểm tra đơn hàng của bạn và sẽ liên hệ với bạn trong thời gian sớm nhất.<br/></h4>