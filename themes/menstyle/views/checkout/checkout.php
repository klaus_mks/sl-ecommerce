<?php $this->breadcrumbs = array(Yii::t('front', 'Checkout method')); 	?>

<div class="row">

    <div class="span3 progress">
        <h3><?php echo Yii::t('front', 'Checkout progress'); ?></h3>
        <ul class="rr">
            <li>
                <a href="#"><?php echo Yii::t('front', 'Checkout method'); ?></a>
            </li>
            <li>
                <a href="#"><?php echo Yii::t('front', 'Shipping information'); ?></a>
            </li>
            <li>
                <a href="#"><?php echo Yii::t('front', 'Orders review'); ?></a>
            </li>
        </ul>
    </div>

    <div class="span9 checkout-list">
        <h3><?php echo Yii::t('front', 'Checkout'); ?></h3>
        <ol class="rr">
            <li class="current">
                <h6><?php echo Yii::t('front', 'Checkout method'); ?></h6>
                <div class="row">
                    <div class="span9 content-wrapper clearfix">
                        <div class="left-col">

                            <h6><?php echo Yii::t('front', 'Checkout as a guest or register'); ?></h6>
                            <p>
                                <?php echo Yii::t('front', 'Register for future convenience'); ?>
                            </p>

                            <form action="<?php echo Yii::app()->createUrl('checkout/checkout'); ?>" method="post" id="form-1">
                                <ul class="rr">
                                    <li>
                                        <label>
                                            <input type="radio" name="checkout_method" value="guest"/>
                                            <span><?php echo Yii::t('front', 'Checkout as a guest'); ?></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" checked="checked" name="checkout_method" value="register"/>
                                            <span><?php echo Yii::t('front', 'Register'); ?></span>
                                        </label>
                                    </li>
                                </ul>
                                
                                <a href="javascript:void(0);" class="sbtn sbtn-normal lnk-submit">
                                    <span class="gradient"><?php echo Yii::t('front', 'Continue'); ?></span>
                                </a>
                            </form>

                        </div>
                        <div class="right-col">

                        <h6><?php echo Yii::t('front', 'Login'); ?></h6>
                        <p>
                            <?php echo Yii::t('front', 'Already registered'); ?>
                        </p>

                        <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id'=>'login-form',
                                'action' => Yii::app()->createUrl('checkout/login'),
                                'enableClientValidation'=>true,
                                'clientOptions'=>array(
                                    'validateOnSubmit'=>true,
                                ),
                                'htmlOptions' => array('class' => 'form-2')
                            ));
                        ?>
                        <ul class="rr">
                            <li>
                                <label>
                                    <?php echo $form->textField($model,'username', array('placeholder' => 'john@mail.com')); ?>
                                </label>
                                <p class="clearfix">
                                    <?php echo $form->error($model,'username'); ?>
                                </p>
                            </li>
                            <li>
                                <label>
                                    <?php echo $form->passwordField($model,'password'); ?>
                                </label>
                                <p class="clearfix">
                                    <?php echo $form->error($model,'password'); ?>
                                </p>
                            </li>
                        </ul>

                        <!-- <a href="#" class="forgot">Forgot your password?</a> -->

                        <a href="javascript:void(0);" class="sbtn sbtn-normal lnk-submit">
                            <span class="gradient"><?php echo Yii::t('front', 'Login'); ?></span>
                        </a>

                        <?php $this->endWidget(); ?>

                    </div>
                    </div>
                </div>
            </li>
            <li>
                <h6><?php echo Yii::t('front', 'Shipping information'); ?></h6>
                <div class="row">
                    <div class="span9 content-wrapper">
                        Content
                    </div>
                </div>
            </li>
            <li class="last">
                <h6><?php echo Yii::t('front', 'Orders review'); ?></h6>
                <div class="row">
                    <div class="span9 content-wrapper">
                        Content
                    </div>
                </div>
            </li>
        </ol>
    </div>

</div>