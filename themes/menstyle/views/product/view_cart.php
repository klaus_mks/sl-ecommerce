<input type="hidden" id="remove-cart-url" value="<?php echo Yii::app()->createUrl('product/removeFromCart'); ?>" />

<?php if(!empty($product)): ?>
<?php 
	$totalValue = 0; 
	Yii::app()->clientScript->registerScriptFile('/js/shopping-cart.js', CClientScript::POS_BEGIN);
?>
	<form refresh-link="<?php echo Yii::app()->createUrl('product/viewCart'); ?>" action="<?php echo Yii::app()->createUrl('product/changeQuantity'); ?>" >
		<table>
		  <tr class="headers">
		    <th class="alpha50 dark">
		      <?php echo Yii::t('front', 'Products'); ?>
		    </th>
		    <th class="alpha16">
		      <?php echo Yii::t('front', 'Quantity'); ?>
		    </th>
		    <th class="alpha16 dark">
		      <?php echo Yii::t('front', 'Unit price'); ?>
		    </th>
		    <th class="alpha16">
		      <?php echo Yii::t('front', 'Subtotal'); ?>
		    </th>
		    <th>

		    </th>
		  </tr>
		  <?php foreach( $cartSession as $cs ): ?>
		  <?php
		  	$item = ShoppyHelper::getRecordById( $cs['id'], $product );
		  	$img = $item->getActiveProductImage( $item->productImages, 138, 179);
		  	if(empty($img)) $img = ShoppyHelper::getNoImage( 138, 179 );
		  	$quantity = $cs['quantity'];
		  	$totalValue += $quantity * $item->price;
		  ?>
		  <tr>
		    <td class="article clearfix">
		      
		      <figure>
		        <img src="<?php echo $img; ?>" alt="<?php echo $item->name; ?>"/>
		      </figure>
		      
		      <div class="info-wrapper">
		        <h2><?php echo $item->name; ?></h2>
		        
		        <?php if(!empty($cs['attribute'])) : ?>
			        <div class="info">
			          <?php foreach( $cs['attribute'] as $i => $attributeId ): ?>
			          <?php 
			          	$attrList = CHtml::listData( $attribute, 'id', 'name' );
			          	$attrValList = CHtml::listData( $attributeValue, 'id', 'value' );
			          ?>
			          <p class="clearfix">
			            <span><?php echo @$attrList[$attributeId]; ?>:</span>
			            <span class="value"><?php echo $attrValList[$cs['attribute_value'][$i]]; ?></span>
			          </p>
			      	  <?php endforeach; ?>
			        </div>
			    <?php endif; ?>
		      </div>
		      
		    </td>
		    <td class="quantity dark">
		    
		      <div class="quant-input">
		        <div class="arrows">
		          <div class="arrow plus gradient"><span class="ir">Plus</span></div>
		          <div class="arrow minus gradient"><span class="ir">Minus</span></div>
		        </div>
		        <input type="text" class="quantity-input" name="quantity[]" value="<?php echo $quantity; ?>"/>
		      </div>
		      
		    </td>
		    <td class="price">
		    
		      <span class="currency"></span><span class="value"><?php echo number_format($item->price); ?></span>
		    
		    </td>
		    <td class="price dark">
		    
		      <span class="currency"></span><span class="value single-value"><?php echo number_format($quantity * $item->price); ?></span>
		    
		    </td>
		    <td>
		    	<form id="frm-remove-from-cart" action="<?php echo Yii::app()->createUrl('product/removeFromCart'); ?>" />
		    		<input type="hidden" name="id" value="<?php echo $cs['id']; ?>"/>
		    		<?php if( !empty($cs['attribute']) ): ?>
		    		<?php foreach( $cs['attribute'] as $i => $a ): ?>
		    			<input type="hidden" name="attribute[<?php echo $a; ?>]" value="<?php echo $cs['attribute_value'][$i]; ?>" />
		    		<?php endforeach; ?>
		    		<?php endif; ?>

		    		<a href="javascript:void(0);" class="remove-from-cart" id="<?php echo $cs['id']; ?>">X</a>
		    	</form>
		    </td>
		  </tr>
		  <?php endforeach; ?>
		  
		  
		  <tr>
		    <td colspan="2" class="empty">
		    </td>
		    <td colspan="2" class="total-wrapper">
		      <div class="total clearfix">
		        <div class="half-col">
		          <?php echo Yii::t('front', 'Total'); ?>
		        </div>
		        <div class="half-col value-wrapper">
		          
		          <span class="currency"></span><span class="value total-value"><?php echo number_format($totalValue); ?></span>
		          
		        </div>
		      </div>
		    
		    </td>
		  </tr>
		</table>
	</form>

	<div class="row-fluid checkout">
	  <div class="span4">
	  	<form action="<?php echo Yii::app()->createUrl('checkout/checkout'); ?>" method="post">
	    	<a href="javascript:void(0);" 
	    		class="sbtn sbtn-normal lnk-submit"
	    		style="margin-top: 20px"
	    	>
	    		<span class="gradient"><?php echo Yii::t('front', 'Checkout'); ?></span>
	    	</a>
	    </form>
	  </div>
	</div>

<?php else: ?>
<?php echo Yii::t('front', 'Your cart has no product'); ?>
<hr/>
<?php endif; ?>