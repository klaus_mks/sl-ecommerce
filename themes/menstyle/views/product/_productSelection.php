<!-- content of "add to cart" modal  -->
<?php
	$img = $product->getActiveProductImage( $product->productImages, 272, 359, array('zc' => 2));
	if( empty( $img)) $img = ShoppyHelper::getNoImage(228, 288); 
?>

<?php if($this->isMobile): ?>
<form id="product-selection" class="form-horizontal product-selection" action="<?php echo Yii::app()->createUrl('product/addToCart'); ?>">
	<input type="hidden" name="id" value="<?php echo $product->id; ?>" />
	<input type="hidden" name="name" value="<?php echo $product->name; ?>" />
	<input type="hidden" name="price" value="<?php echo $product->price; ?>" />

	<table>
        <tbody>
            <tr>
                <th><div class="x-positionR"><?php echo $product->name; ?></div></th>
            </tr>
            <tr>
                <td>
                	<img src="<?php echo $img; ?>" alt="<?php echo $product->name; ?>" />
                </td>
            </tr>
            <?php foreach( $attributes as $i => $a ): ?>
            <tr>
                <th><?php echo $a->name; ?></th>
            </tr>
            <tr>
            	<td class="attribute-group">
		            <?php 
						$cShowed = 0;
						foreach( $product->attributeValues as $j => $v ): 
							if($v->attribute_id != $a->id) 
								continue; 
							else 
								$cShowed++;
					?>
					<label class="checkbox inline selection-attribute">
						<!-- input name:  attribute[attribute_id] = product_attribute_id -->
						<input type="radio" name="attribute[<?php echo $a->id; ?>]" value="<?php echo $v->id; ?>"> <?php echo $v->value; ?>
					</label>
					<?php
						if($cShowed != 0 && $cShowed %3 == 0  )
							echo '<br/>';
					?>
						<?php endforeach; ?>		
						<span class="selection-error"><?php echo Yii::t('validation', 'Please select'); ?> <?php echo strtolower($a->name); ?></span>
				</td>
			</tr>


            <?php endforeach; ?>
            
            <tr>
                <th>
                    <div class="control-group selection-quantity">
						<label class="checkbox inline">
							<span class="title"><?php echo Yii::t('front', 'Quantity'); ?></span>
							<input type="text" name="quantity" value="" /> 
						</label>
						<div class="selection-error"><?php echo Yii::t('validation', 'Please select quantity'); ?></div>
						<div class="numeric-error"><?php echo Yii::t('validation', 'Please input a value which is lesser than 50'); ?></div>
					</div>

                </th>
            </tr>
        </tbody>
    </table>	


    <p>
        <a href="javascript:void(0);" class="sbtn sbtn-normal center-block save-cart-item"
        	<?php if($this->isMobile) echo 'mobile'; ?>
        >
        	<?php echo Yii::t('front', 'Add to cart'); ?>
        </a>
    </p>
</form>

<?php else: ?>
<form id="product-selection" class="form-horizontal product-selection" action="<?php echo Yii::app()->createUrl('product/addToCart'); ?>">
	<input type="hidden" name="id" value="<?php echo $product->id; ?>" />
	<input type="hidden" name="name" value="<?php echo $product->name; ?>" />
	<input type="hidden" name="price" value="<?php echo $product->price; ?>" />

	<div class="row">
		<div class="span4">
			<img src="<?php echo $img ?>" class="img-border" alt="<?php echo $product->name;?>" title="<?php echo $product->name; ?>"/>
			<div class="product-name"><?php echo $product->name; ?></div>
		</div>
		<div class="span7 attribute-block">
			
			<?php foreach( $attributes as $i => $a ): ?>
				<div class="control-group attribute-group attribute-list">
					<h6><?php echo $a->name; ?></h6>
					<?php 
						$cShowed = 0;
						foreach( $product->attributeValues as $j => $v ): 
							if($v->attribute_id != $a->id) 
								continue; 
							else 
								$cShowed++;
					?>
						<label class="checkbox inline selection-attribute">
							<!-- input name:  attribute[attribute_id] = product_attribute_id -->
							<input type="radio" name="attribute[<?php echo $a->id; ?>]" value="<?php echo $v->id; ?>"> <?php echo $v->value; ?>
						</label>

					<!-- show only 3 attributes on a row -->
					<?php
						if($cShowed != 0 && $cShowed %3 == 0  )
							echo '<br/>';
					?>

					<?php endforeach; ?>		
					<span class="selection-error"><?php echo Yii::t('validation', 'Please select'); ?> <?php echo strtolower($a->name); ?></span>
				</div>
			<?php endforeach; ?>
			<div class="control-group selection-quantity">
				<label class="checkbox inline">
					<span class="title"><?php echo Yii::t('front', 'Quantity'); ?></span>
					<input type="text" name="quantity" value="" /> 
				</label>
				<div class="selection-error"><?php echo Yii::t('validation', 'Please select quantity'); ?></div>
				<div class="numeric-error"><?php echo Yii::t('validation', 'Please input a value which is lesser than 50'); ?></div>
			</div>

		</div>
	</div>


	<div class="row">
		<div class="span12">
			<p class="text-center">
				<a href="javascript:void(0);" class="sbtn sbtn-normal save-cart-item"><span class="gradient" style="width: 200px;"><?php echo Yii::t('front', 'Add to cart'); ?></span></a>
			</p>
		</div>
	</div>
</form>
<?php endif; ?>