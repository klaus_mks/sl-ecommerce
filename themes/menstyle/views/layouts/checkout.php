<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gte IE 9]>         <html class="no-js gte-ie9"> <![endif]-->
<!--[if gt IE 99]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
      <?php $this->renderPartial('/site/_ga'); ?>
      <?php
          $themePath = Yii::app()->theme->baseUrl. DIRECTORY_SEPARATOR;
      ?>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
      <title><?php echo $this->pageTitle; ?></title>
      <meta name="description" content="">
  
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

      <?php
        // slide bar must be included first
        Yii::app()->clientScript->registerCssFile( $themePath. 'css/slidebars.css');
        Yii::app()->clientScript->registerCssFile( $themePath. 'css/sb-style.css');

        Yii::app()->clientScript->registerCssFile($themePath. 'css/normalize.min.css');
        Yii::app()->clientScript->registerCssFile($themePath. 'css/main.css');
        Yii::app()->clientScript->registerCssFile($themePath. 'css/media-queries.css');
        Yii::app()->clientScript->registerCssFile($themePath. 'css/bootstrap.css');
        Yii::app()->clientScript->registerCssFile($themePath. 'css/coin-slider-styles.css');
        Yii::app()->clientScript->registerCssFile($themePath. 'css/custom.css');
        Yii::app()->clientScript->registerCssFile('/css/common.css'); 
        Yii::app()->clientScript->registerScriptFile($themePath. 'js/vendor/jquery-1.8.2.min.js');
        Yii::app()->clientScript->registerScriptFile($themePath. 'js/vendor/modernizr-2.6.1.min.js');
        Yii::app()->clientScript->registerScriptFile($themePath. 'js/coin-slider.min.js');
        Yii::app()->clientScript->registerScriptFile($themePath. 'js/bootstrap.min.js');
        Yii::app()->clientScript->registerScriptFile('/js/common.js');
        Yii::app()->clientScript->registerScriptFile($themePath. 'js/accordion.js');

        Yii::app()->clientScript->registerScriptFile($themePath. 'js/slidebars.js');
      ?>

      <script type="text/javascript">
          $(document).ready(function() {
            $.slidebars();
            $('#upper-banner').coinslider({
              width: 940, // width of slider panel
              height: 180,
              navigation: false,
            });
          });
      </script>
      
    </head>
    <body>
      <?php $this->renderPartial('/site/_m_toggle_menu'); ?>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
		  <div id="sb-site">
        <?php $this->renderPartial('/site/_top_bar'); ?>

        <header class="container">      
          <div class="row">
         
          </div>  
          
          <!-- upper banner -->
          <?php $this->renderPartial('/site/_banner'); ?>     

          <div class="row main-menu-wrapper">
            
            <div class="span12">
  			<?php $this->renderPartial('/site/_main_menu'); ?>            
            </div>          
            
          </div>
        </header>
        
        <hr/>
        
        <div class="container breadcrumb-wrapper">
          <div class="row">
            <?php
  	        	$this->widget('zii.widgets.CBreadcrumbs', array(
  	        		'homeLink' => CHtml::link(Yii::t('front', 'Home'), Yii::app()->homeUrl),
  			      	'links' => $this->breadcrumbs,
  			      	'htmlOptions' => array('class' => 'span12 breadcrumb'),
  			      	'separator' => ' / ',

  			 	));
  		 	?>
          </div>
        </div>
        
        
        <div role="main" class="container checkout">        
            <?php echo $content; ?>
        </div>
      
      </div>

      <!-- mobile menu content -->      
      <?php $this->widget('application.extensions.shoppy.widgets.MobileProductListMenu', array(  )); ?>
      
      <?php $this->renderPartial('/site/_footer'); ?>            
        <script src="<?php echo $themePath; ?>js/plugins.js"></script>
        <script src="<?php echo $themePath; ?>js/main.js"></script>
        
    </body>
</html>

