<?php Yii::app()->clientScript->registerScriptFile('/js/validate.js', CClientScript::POS_BEGIN);?>
<?php
$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	GxHtml::valueEx($model) => array('update', 'id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Settings'),
);

?>


<section class="content-header">
    <h1><?php echo Yii::t('app', 'Settings') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>
    <!-- breadcrumbs -->
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			//'tagName' => 'span',
			'links'=>$this->breadcrumbs,
			'htmlOptions' => array(
				'class' => 'breadcrumb'
			)
		)); ?>
	<?php endif?>
</section>
<section class="content">
    <div class="row">
       	<div class="col-md-12">
       		<div class="box box-primary">
                <div class="box-header">
                    <!-- <h3 class="box-title">Quick Example</h3> -->
                </div><!-- /.box-header -->
                <?php
					$this->renderPartial('_form', array('model' => $model));
				?>

                
            </div><!-- /.box -->
       	</div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Settings</h3>
                </div><!-- /.box-header -->
            </div><!-- /.box -->
    	</div>
    </div>
</section>

