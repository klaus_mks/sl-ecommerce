<?php 
    $form = $this->beginWidget('GxActiveForm', array(
		'id' => 'setting-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array('class' => 'form-horizontal', 'role' => "form", 'enctype' => 'multipart/form-data')
	));
?>
    <div class="box-body">
    	<?php if( Yii::app()->user->hasFlash('update-success')) : ?>
    		<div class="alert alert-success alert-dismissable">
	            <i class="fa fa-check"></i>
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <!-- <b>Alert!</b> --> <?php echo Yii::app()->user->getFlash('update-success'); ?> - <a href="<?php echo Yii::app()->createUrl('category/admin'); ?>">back to list</a>
	        </div>
    	<?php endif; ?>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'address', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'address', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'address', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'phone', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'phone', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'phone', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'cellphone', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'cellphone', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'cellphone', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'longtitude', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'longtitude', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'longtitude', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'contact_content', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'contact_content', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'contact_content', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'fb_id', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'fb_id', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'fb_id', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'skype_id', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'skype_id', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'skype_id', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'yahoo_id', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'yahoo_id', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'yahoo_id', array('class' => 'text-red')); ?>
            </div>
        </div>
        
        <div class="form-group">
        	<?php echo $form->labelEx($model,'currency', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'currency', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'currency', array('class' => 'text-red')); ?>
            </div>
        </div>
        
	    

    </div><!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
<!-- </form> -->
<?php $this->endWidget(); ?>

