<style type="text/css">
    small {
        font-size: 100%;
    }
</style>
<div role="main" class="container products grid">  
    <?php $this->breadcrumbs = array('Hình thức thanh toán');   ?>      
    <div class="row">
    
    <?php if(Yii::app()->user->hasFlash('success')) : ?> 
    
    <div class="sl-alert alert-success alert-dismissable">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    
    <?php endif; ?>
        <fieldset>
            <div class="span5 checkout checkout-list right-col input">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'update-password-form',
                    'enableClientValidation'=>false,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>false,
                    ),
                )); 

                ?>
                <input type="hidden" name="updatePassword" value="1" />

                <div class="reg_left fleft">
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Old Password'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->passwordField($model, 'old_password', array('type' => 'password', 'placeholder' => 'Mật khẩu cũ...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'old_password'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Password'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->passwordField($model, 'password', array('type' => 'password', 'placeholder' => 'Mật khẩu mới ...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'password'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Reenter password'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->passwordField($model, 'repeat_password', array('type' => 'password', 'placeholder' => 'Nhập lại mật khẩu...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'repeat_password'); ?>
                        </div>
                    </div>
                    <div class="reg_btn_div">
                        <a style="margin-top: 25px" class="sbtn sbtn-normal lnk-submit" href="javascript:void(0);">
                            <span class="gradient"><?php echo Yii::t('front', 'Update Password'); ?></span>
                        </a>
                    </div>                     
                </div>
                <?php $this->endWidget(); ?>
            </div>
            <div class="span6 offset1 checkout checkout-list right-col input">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'update-account-form',
                    'enableClientValidation'=>false,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>false,
                    ),
                )); ?>

                <div class="reg_left fleft">
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Last name'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->textField($model, 'last_name', array('placeholder' => 'Họ...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'last_name'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'First name'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->textField($model, 'first_name', array('placeholder' => 'Tên...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'first_name'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Phone'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->textField($model, 'phone', array('placeholder' => 'Điện thoại...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'phone'); ?>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Birthday'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->textField($model, 'birthday', array('placeholder' => Yii::t('front', 'Ex: 14-07-1996'). '...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'birthday'); ?>
                        </div>
                    </div>
                    <div class="reg_btn_div">
                        <a style="margin-top: 25px" class="sbtn sbtn-normal lnk-submit" href="javascript:void(0);">
                            <span class="gradient"><?php echo Yii::t('front', 'Update Account'); ?></span>
                        </a>
                    </div> 
                </div>
                <?php $this->endWidget(); ?>
            </div>
    
    </div>
</div>
<?php Yii::app()->clientScript->registerScriptFile('/js/common.js', CClientScript::POS_BEGIN);?>