<?php
    $themePath = Yii::app()->theme->baseUrl. DIRECTORY_SEPARATOR;
?>
<!-- coin slider -->
<?php if( !empty( $banner)): ?>
<div class="main-slideshow hidden-phone">
	<div id="coin-slider" class="container">
		<?php foreach( $banner as $b ): ?>
		<a href="<?php echo $b->url; ?>" target="_blank">
	        <img src="<?php echo ShoppyHelper::getThumbnailPath( Yii::app()->params['general']['banner_path']. '/'. $b->name, 940 ); ?>" >
	        <!-- <span>
	            Description for img01
	        </span>-->
	    </a>
		<?php endforeach; ?>
	</div>
</div>
<?php endif; ?>
<!-- /coin slider -->

<div role="main" class="homepage container">
	<?php $this->renderPartial('_new_arrival', compact('newArrival')); ?>
	<?php $this->renderPartial('_best_seller', compact('bestSeller')); ?>
	<?php $this->renderPartial('_promo_category', compact('promoCategory')); ?>
	<?php $this->renderPartial('_news', compact('news')); ?>
</div>
