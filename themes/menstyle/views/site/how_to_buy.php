<h4>Bước 1:</h4>
<p>
Click link "Thêm vào giỏ hàng" tại sản phẩm bạn chọn
</p>

<h4>Bước 2:</h4>
<p>
Lựa chọn tính chất sản phẩm ( màu sắc, kích thước ... và số lượng ). Sau đó nhấn nút "Thêm vào giỏ hàng" sản phẩm sẽ được thêm vào giỏ hàng của bạn.
</p>

<h4>Bước 3:</h4>
<p>
Bạn có thể xem các nội dung trong giỏ hàng bằng cách click vào link <a href="<?php echo Yii::app()->createUrl('product/viewCart'); ?>">giỏ hàng</a> ở menu phía trên cùng. Tại đây, bạn có thể thêm bớt số lượng hoặc xóa một sản phẩm ra khỏi giỏ hàng. Sau khi hoàn tất lựa chọn, bạn nhấn <a href="<?php echo Yii::app()->createUrl('checkout/checkout'); ?>">thanh toán</a>
</p>

<h4>Bước 4:</h4>
<p>
Tại trang thanh toán, bạn có thể đăng nhập nếu đã có tài khoản, hoặc đăng ký tài khoản mới, hoặc đặt hàng không cần đăng ký tài khoản
</p>

<h4>Bước 5:</h4>
<p>
Bạn nhập tên, số điện thoại, email và địa chỉ sẽ nhận hàng, thêm ghi chú nếu có. Bạn lưu ý nhập đúng những thông tin này, vì chúng tôi sẽ dùng thông tin này để giao hàng đến bạn
</p>

<h4>Bước 6:</h4>
<p>
Bạn kiểm tra lại đơn hàng của mình lần cuối. Nếu mọi thứ đã đúng, bạn nhất hoàn tất. Hệ thống sẽ gửi email chứa thông tin đơn hàng đến email của bạn. Chúng tôi sẽ kiểm tra đơn hàng và liên hệ với bạn trong thời gian sớm nhất
</p>