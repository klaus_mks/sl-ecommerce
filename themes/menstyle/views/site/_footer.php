<footer>
    <div class="top">
      <div class="container">
        <div class="row">
          <div class="span3 offset9 clearfix social-wrapper">
            <ul class="social rr clearfix">
              <li><span>Follow us</span></li>
              <li><a href="<?php echo !empty(Yii::app()->session['setting']['fb_id']) ? Yii::app()->session['setting']['fb_id'] : null ;?>" target="_blank" class="ir icon fb">Facebook</a></li>
              <li><a href="#" class="ir icon rss">RSS</a></li>  -->
            </ul>
          </div>
          
        </div>
      </div>
    </div>
  
    <div class="middle">
      <div class="container">
        <div class="row footer-menu">
        
          <div class="span3">
            <h2>
              <div class="icon icon-bank-account">
                <a href="<?php echo Yii::app()->createUrl('site/contact'); ?>"><?php echo Yii::t('front', 'Account details'); ?></a>
              </div>
            </h2>
          </div>
          
          <div class="span3">
            <h2>
              <div class="icon icon-contact">
                <span class="contact-item"><?php echo !empty(Yii::app()->session['setting']['phone']) ? Yii::app()->session['setting']['phone'] : null; ?></span>
              </div>
            </h2>
          </div>

          <div class="span3">
            <h2>
              <div class="icon icon-cellphone">
                <span class="contact-item"><?php echo !empty(Yii::app()->session['setting']['cellphone']) ? Yii::app()->session['setting']['cellphone'] : null; ?></span>
              </div>
            </h2>
          </div>

          <div class="span3">
            <h2>
              <div class="icon icon-email">
                <span class="contact-item"><?php echo !empty(Yii::app()->session['setting']['email']) ? Yii::app()->session['setting']['email'] : null; ?></span>
              </div>
            </h2>
          </div>
          
        </div>
      </div>

    </div>

    <div class="bottom">
      Copyright &copy; My Company.
    </div>
  </footer>