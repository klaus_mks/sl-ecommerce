<div class="row top-spacing">
    <div class="span5">
        <h1>Về chúng tôi</h1>
        <?php echo nl2br($setting->contact_content);?>
      
        <h1>Thông tin liên hệ</h1>
        <ul class="rr info clearfix">
            <li>
                <?php echo $setting->address;?>
            </li>
            <li>
                Phone: <span class="val"><?php echo $setting->phone;?></span>
            </li>
            <li>
                Email: <span class="val"><?php echo $setting->email;?></span><br/>
            </li>
          
        </ul>
      
        <?php if(!empty($bankAccount)): ?>
        <h1>Tài khoản ngân hàng</h1>
        <?php foreach( $bankAccount as $b ): ?>
        <p>    
            <ul class="rr info clearfix">
                <li>Chủ tài khoản: <b><?php echo $b->name; ?></b></li>
                <li>Ngân hàng: <?php echo $b->bank_name; ?></li>
                <li>Số tài khoản: <b style="color: #cd1231"><?php echo $b->number; ?></b></li>
            </ul>
        </p>
        <?php endforeach; ?>
        <?php endif; ?>


    </div>
  
  
    <div class="span6">
        <div class="google_maps contact-map">
            <?php echo $setting->google_maps; ?>
        </div>
    </div>
    
  
</div>