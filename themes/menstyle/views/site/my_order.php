<?php $this->breadcrumbs = array('Thông tin tài khoản'); 	?>
<?php 
$status = array(
    Order::STATUS_NEW => Yii::t('app', 'New'), 
    Order::STATUS_APPROVED => Yii::t('app', 'Approved'),
    Order::STATUS_DELIVERED => Yii::t('app', 'Delivered'),
    Order::STATUS_PENDING => Yii::t('app', 'Pending'),
);
?>
<div class="row">

  <div class="span3 progress">
    <h3>Thông tin tài khoản</h3>
    <ul class="rr">
        <li>
            <a href="#">Thông tin tài khoản</a>
        </li>
        <li>
            <a href="#">Thông tin đơn hàng</a>
        </li>
        <li>
            <a href="#">Đổi mật khẩu</a>
        </li>
    </ul>
  </div>
  
  <div class="span9 checkout-list">
    <h3>Thông tin đơn hàng</h3>
    <div class="cart">
        <table>
            <tbody>
                <tr class="headers">
                    <th class="alpha6 dark">Mã ĐH</th>
                    <th class="alpha6">Chi tiết</th>
                    <th class="alpha6 dark">Ngày đặt</th>
                    <th class="alpha6">Trang Thái</th>
                    <th class="alpha6 dark">Tiền thanh toán</th>
                </tr>
                <?php foreach ($order as $val) : ?>
                <tr class="headers">
                    <td class="alpha6 dark"><?php echo $val['code']; ?></td>
                    <td class="alpha6" align="left">
                        <?php foreach ($val['product'] as $product) {
                            $attribute = OrderAttribute::model()->getAttributeById($product['order_product_id']);
                            echo $product['name'].
                                $attribute.
                                '<br><b>Price: </b>'. number_format($product['price']).
                                ' / <b>SL:</b> '.$product['quantity'].
                                ' / <b>Tổng giá: </b>'. number_format($product['total_price']). 
                                '<br>----------------------------------------------------------<br>';
                        }?>
                    </td>
                    <td class="alpha6 dark"><?php echo $val['order_date']; ?></td>
                    <td class="alpha6"><?php echo $status[$val['status']]; ?></td>
                    <td class="alpha6 dark"><?php echo number_format($val['total']); ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
  </div>

</div>