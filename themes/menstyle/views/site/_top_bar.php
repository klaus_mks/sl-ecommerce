<?php
    $themePath = Yii::app()->theme->baseUrl. DIRECTORY_SEPARATOR;
?>
<?php if(!$this->isMobile): ?>
<div class="top-bar">
    <div class="container">
      <div class="row">
      
        <div class="span2 shipping">
          <!-- <a href="#">Free shipping &amp; returns</a> -->
          <?php if(!empty($this->meta_tag['image'])): ?>
            <img alt="logo" src="<?php echo $this->meta_tag['image']; ?>" />
          <?php endif; ?>
        </div>
        
        <div class="span10 menu clearfix " data-menu-style="minimal" data-menu-title="Menu">
          <ul class="clearfix rr">
            <li>
              <span class="language"><a href="<?php echo Yii::app()->createUrl("site/changeLanguage", array('l' => 'vi')) ?>"><img src="<?php echo $themePath; ?>img/vn_flag.png" alt="Vietnamese" title="Vietnamese" /></a></span>
              <span class="language"><a href="<?php echo Yii::app()->createUrl("site/changeLanguage", array('l' => 'en')) ?>"><img src="<?php echo $themePath; ?>img/uk_flag.png" alt="English" title="English" /></a></span>
            </li>

            <li>
            	<a href="<?php echo Yii::app()->createUrl('product/viewCart'); ?>">
            		<span class="icon ir shopping-cart-icon"></span>
            		<span><?php echo Yii::t('front', 'Shopping cart'); ?> ( <span class="cart-item-no"> <?php echo !empty(Yii::app()->session['cart-total']) ? Yii::app()->session['cart-total'] : 0 ; ?> </span> )</span>
		              <!-- <span>Giỏ hàng (<span>0</span> items) - </span> -->
		              <!-- <span class="price"><span>$</span><span>0.00</span></span>	 -->
            	</a>
            </li>
            <li>
              <a href="<?php echo Yii::app()->createUrl('checkout/checkout'); ?>">
                <span class="ir icon checkout"></span>
                <span><?php echo Yii::t('front', 'Checkout'); ?></span>
              </a>
            </li>
            <li>
              <?php if(Yii::app()->user->isGuest): ?>
              <a href="<?php echo Yii::app()->createUrl('site/login'); ?>">
                <span class="ir icon log-in"></span>
                <span><?php echo Yii::t('front', 'Login'); ?></span>
              </a>
              <?php else: ?>
                <a href="<?php echo Yii::app()->createUrl('site/updateInfo'); ?>">
                  <?php echo Yii::app()->user->email; ?>
                </a>
                 - <a href="<?php echo Yii::app()->createUrl('site/logout'); ?>">Log out</a>
              <?php endif; ?>

            </li>
            <li>
              <a href="<?php echo Yii::app()->createUrl('site/register'); ?>">
                <span class="ir icon register"></span>
                <span><?php echo Yii::t('front', 'Register'); ?></span>
              </a>
            </li>
          </ul>
        </div>


      </div>
    </div>
</div>
<?php endif; ?>







