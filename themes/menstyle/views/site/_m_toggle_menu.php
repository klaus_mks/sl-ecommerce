<?php if($this->isMobile ): ?>

<?php if(!empty($this->meta_tag['image'])): ?>
<div class="m-logo">
    <img alt="logo" src="<?php echo $this->meta_tag['image']; ?>" />
</div>
<?php endif; ?>

<div class="sb-bar">

    <a class="sb-toggle-left navbar-left" href="javascript:void(0);">
        <div class="navicon-line"></div>
        <div class="navicon-line"></div>
        <div class="navicon-line"></div>
    </a>

    <a class="sb-toggle-right navbar-right" href="javascript:void(0);">
        <div class="navicon-line"></div>
        <div class="navicon-line"></div>
        <div class="navicon-line"></div>
    </a>

</div>
<?php endif; ?>