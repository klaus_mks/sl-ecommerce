<!-- new arrivals -->
<?php if( !empty( $newArrival) ): ?>
<div class="row">
  <div class="span12 main-heading">
    <div class="heading-line"></div>
    <div class="heading-wrapper">
      <h1><?php echo Yii::t('front', 'New arrivals'); ?></h1>
    </div>
  </div>
</div>


<div id="other-prod-slider">
  <div class="navigation"></div>
  <ul class="row-fluid clearfix rr other-products">
  	<?php foreach( $newArrival as $item ): ?>
	<?php
		$img = $item->getActiveProductImage( $item->productImages, 240, 150, array('zc' => 2));
		if( empty( $img)) $img = ShoppyHelper::getNoImage(240,150); 
	?>
    <li class="span3 alpha25 desat">
      <a href="<?php echo Yii::app()->createUrl('list/viewProduct', array('id' => $item->id, 'title' => ShoppyHelper::getSlug( $item->name))); ?>">
        <span class="badge off ir hidden">Off</span>              
        <img src="<?php echo $img ?>" alt="<?php echo $item->name;?>" title="<?php echo $item->name; ?>"/>
      </a>
      <span class="info">
        <span class="title"><?php echo $item->name; ?></span>
        <span class="price">
          <span class="actual"><span><?php echo number_format($item->price); ?></span></span>
        </span>
      </span>
    </li>
    <?php endforeach; ?>
    
  </ul>
</div>

<?php endif; ?>