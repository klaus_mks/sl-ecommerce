<style type="text/css">
    small {
        font-size: 100%;
    }
</style>
<div role="main" class="container products grid">  
    <?php $this->breadcrumbs = array('Hình thức thanh toán');   ?>      
    <div class="row">
    
    <?php if(Yii::app()->user->hasFlash('success')) : ?> 
    
    <div class="sl-alert alert-success alert-dismissable">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    
    <?php endif; ?>
        <fieldset>
            <div class="span8 offset2">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'forgot-password-form',
                    
                    'enableClientValidation'=>false,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>false,
                    ),
                    'htmlOptions'  => array(
                        'class' => 'form-inline',
                    )
                )); 

                ?>
                
                <?php echo $form->textField($model, 'email', array('type' => 'text', 'placeholder' => 'Ex: jondoe@mail.com...','class' => 'text-input input-large', 'style' => 'width: 50%;')); ?>
                
                <a style="margin-left: 25px" class="sbtn sbtn-normal lnk-submit" href="javascript:void(0);">
                    <span class="gradient"><?php echo Yii::t('front', 'Get password'); ?></span>
                </a>
                <?php echo $form->error($model,'email'); ?>
                
                <?php $this->endWidget(); ?>
            </div>
            
        </fieldset>
    </div>
</div>
<?php Yii::app()->clientScript->registerScriptFile('/js/common.js', CClientScript::POS_BEGIN);?>