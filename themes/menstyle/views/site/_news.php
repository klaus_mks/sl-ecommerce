<?php if(!empty($news)): ?>
<div class="row">
  <div class="span12 main-heading">
    <div class="heading-line"></div>
    <div class="heading-wrapper">
      <h1><?php echo Yii::t('front', 'Articles'); ?></h1>
    </div>
  </div>
</div>

<div class="row">
	<?php foreach( $news as $item ): ?>
	<div class="span6">
		<h4><a href="<?php echo Yii::app()->createUrl('list/viewPost', array('id' => $item->id, 'title' => ShoppyHelper::getSlug( $item->title))); ?>" class="btn-link"><?php echo $item->title; ?></a></h4>
		<p>
			<?php
				$img = $item->getPostImage(80, 80 );
				if(empty($img)) $img = ShoppyHelper::getNoImage(80, 80 );
			?>

			<a href="<?php echo Yii::app()->createUrl('list/viewPost', array('id' => $item->id, 'title' => ShoppyHelper::getSlug( $item->title))); ?>">
				<img class="post-image" src="<?php echo $img; ?>" />
			</a>
			<?php echo ShoppyHelper::wordLimit($item->content, 21) ; ?>
			<span class="view-more"><a href="<?php echo Yii::app()->createUrl('list/viewPost', array('id' => $item->id, 'title' => ShoppyHelper::getSlug( $item->title))); ?>"><?php echo Yii::t('front', 'Read more'); ?></a></span>
		</p>
	</div>
	<?php endforeach; ?>


</div>

<?php endif; ?>
