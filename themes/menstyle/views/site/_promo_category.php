<?php if(!empty( $promoCategory)) : ?>
<?php foreach( $promoCategory as $c ): ?>
	<div class="row top-spacing">
	  <div class="span12 main-heading">
	    <div class="heading-line"></div>
	    <div class="heading-wrapper">
	      <h1><?php echo $c->name; ?></h1>
	    </div>
	  </div>
	</div>

	<div id="other-prod-slider2">
	  <div class="navigation"></div>
	  <ul class="row-fluid clearfix rr other-products2">
	  	<?php foreach( $c->products as $item ): ?>
		<?php
			$img = $item->getActiveProductImage( $item->productImages, 240, 150, array('zc' => 2));
			if( empty( $img)) $img = ShoppyHelper::getNoImage(240,150); 
		?>
	    <li class="span3 alpha25 desat">
	      <a href="<?php echo Yii::app()->createUrl('list/viewProduct', array('id' => $item->id, 'title' => ShoppyHelper::getSlug( $item->name))); ?>">
	        <span class="badge off ir hidden">Off</span>              
	        <img src="<?php echo $img; ?>" alt="<?php echo $item->name; ?>"/>
	      </a>
	      <span class="info">
	        <span class="title"><?php echo $item->name; ?></span>
	        <span class="price">
	          <span class="old hidden"><span>$</span><span>100</span> - </span>
	          <span class="actual"><span>$</span><span>100</span></span>
	        </span>
	      </span>
	    </li>
	    <?php endforeach; ?>
	  </ul>
	</div>

<?php endforeach; ?>
<?php endif; ?>


