<!-- this is the view for content of one post -->
<?php 
	$this->breadcrumbs = array('Bài viết' => Yii::app()->createUrl('list/listPost'), $post->title);
?>        
<div class="span9 article">
	<h4><?php echo $post->title; ?>
		<small class="created-date"><?php echo date('d-m-Y H:i:s', strtotime($post->created_at) ); ?></small>
	</h4>
	<?php echo $post->content; ?>
	
</div>
	