<!-- this is the view for product listing ( list random category & list product by category) -->
<?php
    $themePath = Yii::app()->theme->baseUrl. DIRECTORY_SEPARATOR;
    Yii::app()->clientScript->registerScriptFile('/js/lazyload.js', CClientScript::POS_BEGIN);
    Yii::app()->clientScript->registerScriptFile('/js/product-attribute.js', CClientScript::POS_BEGIN);
    Yii::app()->clientScript->registerScriptFile('/js/shopping-cart.js', CClientScript::POS_BEGIN);
    Yii::app()->clientScript->registerScript('productAttribute', 'ProductAttribute.filterProduct();', CClientScript::POS_READY);    
    Yii::app()->clientScript->registerScript('afterLazyLoad', '
        $(document).on("afterLoad", function(){
            $(".grid-display li .info").hide();
        });
    ', CClientScript::POS_READY);    

    if(!empty($category))
        $this->breadcrumbs = $category->getAncestorsBreadCrumb('list/listCategoryProduct');

?>
        
<div class="span9">
    <input type="hidden" id="add-cart-url" value="<?php echo Yii::app()->createUrl('product/getProductSelection'); ?>" />
    <!-- category image -->
    <?php if( !empty($category->image)) : ?>
    <div class="featured clearfix hidden-phone">
      <a href="<?php echo $category->url; ?>">
      	<img src="<?php echo $category->getImage( 700, 172 ); ?>" title="<?php echo $category->name; ?>" />
      </a>
      
    </div>
	<?php endif; ?>

    <!-- end category image -->
    <div class="span3">
    </div>

    <br/>
    <!-- paging -->
    
    <!-- end paging -->


    <!-- product list -->
    <?php if( !empty( $product)): ?>
    <ul class="row-fluid clearfix rr grid-display lazyload-dest">
      <?php foreach( $product as $item ): ?>
      <li class="span4 alpha33 desat">
        <div class="prod-wrapper">
          <!-- <span class="badge corner-badge off-35 ir">Hot</span> -->
          <span class="badge price-badge">
            <span class="value">
              <?php if(!empty($item->old_price)) : ?>
              <span class="old-price">
                <?php echo number_format($item->old_price); ?>
              </span>
              <?php endif; ?>
              
              <span><?php echo number_format($item->price); ?></span>
              <span><?php echo Yii::app()->params['general']['currency']; ?></span>
            </span>
          </span>    
          <?php
          	$img = $item->getActiveProductImage( $item->productImages, 238, 288, array('zc' => 2));
          	if(empty($img)) $img = ShoppyHelper::getNoImage( 238, 288 );
          ?>
          <a href="<?php echo Yii::app()->createUrl('list/viewProduct', array('id' => $item->id, 'title' => ShoppyHelper::getSlug( $item->name))); ?>">
            <img src="<?php echo $img; ?>" class="desat-ie" alt="<?php echo $item->name; ?>"/>
          </a>
        
          <span class="info gradient">
            <span class="title"><?php echo $item->name; ?></span>
            <span class="add-to-cart clearfix">
              <span class="icon ir">Cart</span>
              <a href="javascript:void(0);" class="text add-to-cart"
                <?php if($this->isMobile) echo 'mobile'; ?>
              	id="<?php echo $item->id; ?>"
              	name="<?php echo $item->name; ?>"
              	price="<?php echo $item->price; ?>"
              >
              	<?php echo Yii::t('front', 'Add to cart'); ?>
              </a>
            </span>
          </span>
        </div>
      </li>
	  <?php endforeach; ?>      
    </ul>
	<?php else: ?>
		Chưa có sản phẩm trong danh mục này
	<?php endif; ?>      
	<input type="hidden" id="new-offset" value="<?php echo !empty($offset) ? $offset : NULL; ?>" />
	<input type="hidden" id="url" value="<?php echo !empty($category) ? Yii::app()->createUrl('list/listCategoryProduct', array('id' => $category->id, 'title' => ShoppyHelper::getSlug( $category->name))) : null; ?>" />
    <!-- end product list -->

    <!-- paging -->
    
    <!-- end paging -->
  </div>
  
</div>