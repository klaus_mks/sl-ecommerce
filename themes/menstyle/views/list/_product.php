<?php if( !empty( $product)): ?>
    
      <?php foreach( $product as $item ): ?>
      <li class="span4 alpha33 desat">
        <div class="prod-wrapper">
          <!-- <span class="badge corner-badge off-35 ir">Hot</span> -->
          <span class="badge price-badge">
            <span class="value">
              <?php if(!empty($item->old_price)) : ?>
              <span class="old-price">
                <?php echo number_format($item->old_price); ?>
              </span>
              <?php endif; ?>
              
              <span><?php echo number_format($item->price); ?></span>
              <span><?php echo Yii::app()->params['general']['currency']; ?></span>
            </span>
          </span>    
          <?php
          	$img = $item->getActiveProductImage( $item->productImages, 238, 288, array('zc' => 2));
          	if(empty($img)) $img = ShoppyHelper::getNoImage( 238, 288 );
          ?>
          <a href="<?php echo Yii::app()->createUrl('list/viewProduct', array('id' => $item->id, 'title' => ShoppyHelper::getSlug( $item->name))); ?>">
            <img src="<?php echo $img; ?>" class="desat-ie" alt="<?php echo $item->name; ?>"/>
          </a>
        
          <span class="info gradient">
            <span class="title"><?php echo $item->name; ?></span>
            <span class="add-to-cart clearfix">
              <span class="icon ir">Cart</span>
              <a href="javascript:void(0);" class="text add-to-cart"
              	id="<?php echo $item->id; ?>"
              	name="<?php echo $item->name; ?>"
              	price="<?php echo $item->price; ?>"
              >
              	<?php echo Yii::t('front', 'Add to cart'); ?>
              </a>
            </span>
          </span>
        </div>
      </li>
	  <?php endforeach; ?>      
<?php else: ?>
	<input type="hidden" id="eol" />

<?php endif; ?>