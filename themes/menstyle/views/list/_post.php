<?php if( !empty( $post)): ?>    
  <?php foreach( $post as $item ): ?>
  <div class="row post-content">
	<h4>
		<a href="<?php echo Yii::app()->createUrl('list/viewPost', array('id' => $item->id, 'title' => ShoppyHelper::getSlug( $item->title))); ?>" class="btn-link"><?php echo $item->title; ?></a>
		<small class="created-date"><?php echo date('d-m-Y H:i:s', strtotime($item->created_at) ); ?></small>
	</h4>
	<p>
		<?php
			$img = $item->getPostImage(80, 80 );
			if(empty($img)) $img = ShoppyHelper::getNoImage(80, 80 );
		?>
		<a href="<?php echo Yii::app()->createUrl('list/viewPost', array('id' => $item->id, 'title' => ShoppyHelper::getSlug( $item->title))); ?>">
			<img class="post-image" src="<?php echo $img; ?>" />
		</a>
		<?php echo ShoppyHelper::wordLimit($item->content, 50) ; ?>
		<span class="view-more"><a href="<?php echo Yii::app()->createUrl('list/viewPost', array('id' => $item->id, 'title' => ShoppyHelper::getSlug( $item->title))); ?>">Xem thêm</a></span>
	</p>
	<div class="clearFix"></div>
	<hr/>
	</div>
  <?php endforeach; ?>      

<?php else: ?>
	<input type="hidden" id="eol" />
<?php endif; ?>