<!-- this view is for product detail -->
<?php 
	Yii::app()->clientScript->registerScriptFile('/js/shopping-cart.js', CClientScript::POS_BEGIN);
	$this->breadcrumbs = $model->getAncestorsBreadCrumb('list/listCategoryProduct', true);
?>

<?php $referer = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>
<input type="hidden" id="add-cart-url" value="<?php echo Yii::app()->createUrl('product/getProductSelection'); ?>" />

<div class="product-details ">
 
    <div class="row">              
        <div class="span5 gallery">
            <div class="gallery-sub-wrap clearfix">
                <ul class="rr tabs">
                    <?php
                        $path = YiiBase::getPathOfAlias('webroot'). Yii::app()->params['product']['image_path'].DIRECTORY_SEPARATOR; 
                        $img = ProductImage::model()->getImageForProductId($model->id); 
                        
                    foreach ($img as $k=>$val) :
                        if($k > 3)
                            break;
                    ?>
                        <li class="active <?php if($k==0) echo 'current';?>" id="gal-<?php echo $k;?>">
                            <span class="arrow ir"><?php echo $val['name'];?></span>
                            <?php 
                               $linkImg =  ShoppyHelper::getThumbnailPath(Yii::app()->params['product']['image_path']. '/'.$val['name'], 68, 86) ;
                            ?>        
                            <img alt="" src="<?php echo $linkImg;?>">          
                        </li>
                    <?php endforeach; ?>
                    &nbsp;
                </ul>
                
                <ul class="rr images">
                    <?php 
                    foreach ($img as $k=>$val) :
                        if($k > 3)
                            break;
                    ?>
                        <li class="<?php if($k==0) echo 'current';?> gal-<?php echo $k;?>">
                            <?php $linkImg =  ShoppyHelper::getThumbnailPath($path.$val['name'], $width = '286', $height = '377');  ?>
                            <img alt="" src="<?php echo $linkImg;?>">  
                        </li>
                    <?php endforeach; ?>

                    <!-- show no img picture -->
                    <?php if(empty($img)): ?>
                        <?php $linkImg = ShoppyHelper::getNoImage(286, 377); ?>
                        <img alt="" src="<?php echo $linkImg;?>">  
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    
        <div class="span5 product">
            <h1><?php echo $model->name; ?></h1>
            <!-- SKU -->
            <?php if(!empty($model->sku)): ?>
                <h3>
                  <?php echo Yii::t('front', 'Product code'); ?>: <?php echo $model->sku;?>
                </h3>
            <?php endif; ?>
          
            <p class="description">
                <?php echo nl2br($model->description); ?>
            </p>
          
            <hr/>
          
            <ul class="rr prefs clearfix">
                <?php foreach ($categoryAttribute as $val) : ?>
                    <li class="size clearfix">
                        <span class="info-title"><?php echo $val['name'] ?>:</span>
                        <ul class="rr clearfix">
                            <?php $attr = ProductAttribute::model()->getInfoAttribute($model->id, $val['id']);
                            foreach ($attr as $a) {
                                echo '<li>'.$a['value'].'</li>';
                            }
                            ?>
                        </ul>
                    </li>
                <?php endforeach; ?>
            
            </ul>
          
            <hr/>
          
            <ul class="rr clearfix buy-wrapper">
                <li>            
                    <a href="javascript:void(0);" 
                        class="add-to-cart clearfix"
                      	id="<?php echo $model->id; ?>"
                      	name="<?php echo $model->name; ?>"
                      	price="<?php echo $model->price; ?>"
                        <?php if($this->isMobile) echo 'mobile'; ?>
                    >
                        <span class="icon ir">Cart</span>
                        <span class="text">
                          <?php echo Yii::t('front', 'Add to cart'); ?>
                        </span>
                    </a>
                
                </li>                  
                <li class="price-wrapper">
                
                    <span class="price">
                        <?php if(!empty($model->old_price)) : ?>
                            <span class="old-price"><?php echo number_format($model->old_price); ?></span>
                        <?php endif; ?>

                        <span class="currency"></span><span class="value"><?php echo number_format($model->price); ?></span>
                    </span>
                
                </li>
            </ul>                
          
            <hr/>
            <ul class="rr options clearfix">
                <li>
                    <div 
                        class="fb-like" 
                        data-href="<?php echo Yii::app()->createUrl('list/viewProduct', array('id' => $model->id, 'title' => $model->name)); ?>" 
                        data-layout="standard" 
                        data-action="like" 
                        data-show-faces="false" 
                        data-share="true">
                    </div>
                  
                </li>
            </ul>
          <hr/>
          
       	</div>
    
        <!-- additional random products -->
        <?php if(!empty($randomProduct)): ?>
        <div class="span2 also-like">
          <h5><?php echo Yii::t('front', 'You may also like'); ?></h5>
          <ul class="rr clearfix">
            <?php foreach( $randomProduct as $item ): 
              $img = $item->getActiveProductImage( $item->productImages, 138, 179, array('zc' => 2));
              if(!empty($img)):
            ?>
            <li>
              <a href="<?php echo Yii::app()->createUrl('list/viewProduct', array('id' => $item->id, 'title' => ShoppyHelper::getSlug($item->name) )); ?>">
                <img src="<?php echo $img; ?>" alt="<?php echo $item->name; ?>" title="<?php echo $item->name; ?>">
              </a>
            </li>
            <?php 
              endif;
            endforeach; 
            ?>
          </ul>
        </div>
        <?php endif; ?>

    </div>

    <div class="fb_comment_block">
  	    <div 
    	  	class="fb-comments" 
    	  	data-href="<?php echo Yii::app()->createAbsoluteUrl('list/viewProduct', array('id' => $model->id, 'title' => ShoppyHelper::getSlug($model->name))); ?>" 
    	  	data-numposts="5" 
    	  	width="100%"
    	  	data-colorscheme="light">
	    </div>
    </div>
</div>