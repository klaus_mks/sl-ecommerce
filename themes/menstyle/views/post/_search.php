<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'title_en'); ?>
		<?php echo $form->textField($model, 'title_en', array('maxlength' => 255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'image'); ?>
		<?php echo $form->textField($model, 'image', array('maxlength' => 255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'content_en'); ?>
		<?php echo $form->textArea($model, 'content_en'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'is_published'); ?>
		<?php echo $form->dropDownList($model, 'is_published', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'created_by'); ?>
		<?php echo $form->textField($model, 'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'modified_by'); ?>
		<?php echo $form->textField($model, 'modified_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'modified_at'); ?>
		<?php echo $form->textField($model, 'modified_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'title_vi'); ?>
		<?php echo $form->textField($model, 'title_vi', array('maxlength' => 255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'content_vi'); ?>
		<?php echo $form->textArea($model, 'content_vi'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
