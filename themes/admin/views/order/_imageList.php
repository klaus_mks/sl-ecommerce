<div class="row">
	<?php foreach( $image as $i ): ?>
		<div class="col-sm-6 col-md-2">
		    <div class="thumbnail">
		      <img src="<?php echo Yii::app()->params['product']['image_path']. '/'. $i['name'] ?>" alt="...">
		      <div class="caption">
		        	<ul class="clearFix">
		        		<li class="delete">
		        			<a href="<?php echo Yii::app()->createUrl('product/deleteImage', array('id' => $i['id'])); ?>" class="btn-lg" role="button" alt="Delete" title="Delete">
		        				<span class="glyphicon glyphicon-remove"></span>
		        			</a> 
		        		</li>
		        		<li class="make-active">
		        			<?php 
		        				$link = Yii::app()->createUrl('product/activeImage', array('id' => $i['id']));
		        			?>
		        			<?php if( !$i['is_actived']): ?>
		        			<a href="<?php echo $link  ?>" class="btn-lg" role="button" alt="Make actived" title="Make actived">
		        				<span class="glyphicon glyphicon-<?php echo $i['is_actived'] ? "ok" : "picture"; ?>"></span>
		        			</a> 
		        			<?php else: ?>
		        			<span class="btn-lg">
		        				<span class="glyphicon glyphicon-ok"></span>
		        			</span> 
		        			<?php endif; ?>
		        		</li>
		        	</ul>
		      </div>
		    </div>
	  	</div>

	<?php endforeach; ?>
  
</div>