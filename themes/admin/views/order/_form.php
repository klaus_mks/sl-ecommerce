<?php 
    $form = $this->beginWidget('GxActiveForm', array(
		'id' => 'order-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array('id' =>'frm-order-detail', 'class' => 'form-horizontal', 'role' => "form", 'enctype' => 'multipart/form-data')
	));    
?>
    <input type="hidden" id="order-id" name="order-id" value="<?php echo $model->id; ?>" />
    <div class="box-body">
    	<?php if( Yii::app()->user->hasFlash('update-success')) : ?>
    		<div class="alert alert-success alert-dismissable">
	            <i class="fa fa-check"></i>
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <!-- <b>Alert!</b> --> <?php echo Yii::app()->user->getFlash('update-success'); ?> 
	        </div>
    	<?php endif; ?>

    	<?php if( Yii::app()->user->hasFlash('update-unsuccess')) : ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo Yii::app()->user->getFlash('unpdate-usuccess'); ?> - <a href="<?php echo Yii::app()->createUrl('category/admin'); ?>">back to list</a>
            </div>
        <?php endif; ?>
    	
        <div class="form-group">
        	<?php echo $form->labelEx($model,'order_date', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo GxHtml::encode($model->order_date); ?>
            </div>
        </div>
        
        <div class="form-group">
        	<?php echo $form->labelEx($model,'status', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php #echo $model->changeStatus($model->status, array('id' => 'order-detail-statuses', 'name' => 'Order[status]')); ?>
                <?php echo $model->getStatusName(); ?>
            	<?php echo $form->error($model,'status'); ?>
            </div>
        </div>
        <h3>
        <?php echo Yii::t('Info', 'Product Information') ;?></h3>
        <table class="table table-bordered table-hover dataTable">
            <thead>
                <tr>
                    <td><?php echo Yii::t('App', '#'); ?></td>
                    <td><?php echo Yii::t('Name', 'Name'); ?></td>
                    <td><?php echo Yii::t('Price', 'Price'); ?></td>
                    <td><?php echo Yii::t('Number', 'Number'); ?></td>
                    <td><?php echo Yii::t('Total price', 'Total price'); ?></td>
                </tr>
            </thead>
            <tbody>
                <?php
                setlocale(LC_MONETARY, 'vi_VN');
                $total = 0;
                foreach ($orderProducts as $k => $op) : ?>
                <tr>
                    <td><?php echo $k+1; ?></td>
                    <td><?php echo '<a href = "#">'. $op->product->name .'</a>'?></td>
                    <td><?php echo $op->product->price ?></td>
                    <td><?php echo $op->quantity ?></td>
                    <td><?php echo money_format('%i', $op->total_price );?></td>
                </tr>
                <?php 
                $total = $total + $op->total_price;
                endforeach; ?>
                <tr>
                    <td colspan="5" align='right'><b><?php echo Yii::t('App', 'Total: '); ?></b><?php echo money_format('%i', $total) ;?></td>
                </tr>
            </tbody>
        </table>
        <h3><?php echo Yii::t('Info', 'Customer Information') ;?></h3>
        <table class="table table-bordered table-hover dataTable">
            <thead>
                <tr>
                    <td><?php echo Yii::t('Name', 'Name'); ?></td>
                    <td><?php echo Yii::t('Phone', 'Phone'); ?></td>
                    <td><?php echo Yii::t('Number', 'Shipping Address'); ?></td>
                    <td><?php echo Yii::t('Note', 'Note'); ?></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo GxHtml::encode($model->customer_name); ?></td>
                    <td><?php echo GxHtml::encode($model->phone); ?></td>
                    <td><?php echo GxHtml::encode($model->shipping_address); ?></td>
                    <td><?php echo GxHtml::encode($model->note); ?></td>
                </tr>
            </tbody>
        </table>

    </div><!-- /.box-body -->

    <?php if ($model->status != Order::STATUS_DELIVERED): ?>
    <div class="box-footer">
        <div class="row">
            <div class="col-md-1 col-xs-4">
                <a href="javascript:void(0);" url="<?php echo Yii::app()->createUrl('order/update', array('id' => $model->id)); ?>" new-status="<?php echo Order::STATUS_DELIVERED; ?>" class="btn btn-info change-status">Delivered</a>
            </div>
            
            <?php if ($model->status == Order::STATUS_NEW): ?>
            <div class="col-md-1 col-xs-3">
                <a href="javascript:void(0);" url="<?php echo Yii::app()->createUrl('order/update', array('id' => $model->id)); ?>" new-status="<?php echo Order::STATUS_APPROVED; ?>"  class="btn btn-primary change-status">Approve</a>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>

<!-- </form> -->
<?php $this->endWidget(); ?>

