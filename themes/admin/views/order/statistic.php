<?php
$this->breadcrumbs = array(
	Yii::t('app', 'Order statistic'),
);
// make the pager float right
Yii::app()->clientScript->registerCss('floatRightPager', 'div.dataTables_paginate ul.pagination{ float:right; }');
Yii::app()->clientScript->registerCssFile( '/css/jquery-ui.css');
Yii::app()->clientScript->registerScript('orderStatistic', '

$(document).ready(function(){
	$(".datepicker").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true });
});
'
, CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile( '/js/jquery-ui.js');

?>

<section class="content-header">
    <h1>
        Order Statistic
        <small>order list</small>
    </h1>

    <!-- breadcrumbs -->
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			//'tagName' => 'span',
			'links'=>$this->breadcrumbs,
			'htmlOptions' => array(
				'class' => 'breadcrumb'
			)
		)); ?>
	<?php endif?>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">           
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">

                <!-- ORDER SEARCH FORM -->
                <?php
                	$form = $this->beginWidget('CActiveForm', array(
						'id'=>'statistic-form',
						'action' => Yii::app()->createUrl('order/statistic'),
						'enableClientValidation'=>true,
						'clientOptions'=>array(
							'validateOnSubmit'=>true,
						),
						'htmlOptions' => array('class' => 'form-inline', 'role' => "form")
					)); 
                ?>
				  	<div class="form-group">
					    <label class="sr-only" for="from-date">From</label>
					    <?php echo $form->textField($model,'from_date', array('id' => 'from-date', 'class' => 'form-control datepicker', 'placeholder' => 'Enter begin date')); ?>
					    
				  	</div>
					<div class="form-group">
					    <label class="sr-only" for="to-date">To</label>
					    <?php echo $form->textField($model,'to_date', array('id' => 'to-date', 'class' => 'form-control datepicker', 'placeholder' => 'Enter end date')); ?>
					    
					</div>
				  	<button type="submit" class="btn btn-success btn-flat">Search</button>
				  	
				  	<?php echo $form->error($model,'from_date', array('class' => 'text-red')); ?>
				  	<?php echo $form->error($model,'to_date', array('class' => 'text-red')); ?>
				  	
				<?php $this->endWidget(); ?>

				<!-- /ORDER SEARCH FORM -->

               	<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'data-grid',
						//'htmlOptions' => array('class' => 'table table-bordered table-hover dataTable'),
						'itemsCssClass' => 'table table-bordered table-hover dataTable',
						'dataProvider' => $dataProvider,
						'template' => "{items}\n<div class=\"row\">{summary}\n{pager}</div>",
						'summaryCssClass' => 'col-xs-6',
						'pagerCssClass' => 'col-xs-6 dataTables_paginate paging_bootstrap',
						'pager' => array(
							//class => 'class name'
							'htmlOptions' => array(		// class for ul tag
								'class' => 'pagination',

							),
							'header' => false,
							'firstPageLabel' => '<<',
                            'prevPageLabel'  => '<',
                            'nextPageLabel'  => '>',
                            'lastPageLabel'  => '>>',
                            'firstPageCssClass' => 'hidden',
                            'lastPageCssClass' => 'hidden',
                            'selectedPageCssClass' => 'active',
						),
						// 'ajaxUpdate' => 'category-grid',
						// 'ajaxUrl'=> Yii::app()->request->getUrl(),
						//'filter' => $model,
						'columns' => array(
							array('name' => 'id', 'header' => '#',
								'filter' => false
							),
							array('name' => 'customer_name', 'header' => 'Customer Info', 'type' => 'html', 
								'value' => '$data->getCustomerOrder()', 
							),
							array('name' => 'shipping_address', 'header' => 'Product', 'type' => 'html', 
								'value' => '$data->getInfoOrder()',
								'filter' => false
							),
							array(
								'name' => 'order_date', 'header' => 'Ordered date', 'filter' => false
							),
							// array('name' => 'id', 'header' => 'Total price','type' => 'html', 
							// 	'value' => '$data->getTotalOrder()', 'filter' => false
							// ),
							array('header' => 'Status', 'value' => '$data->getStatusName()',
								'filter' => false
					        ),
						),
						
					)); ?>

	                <div>
	                	<h1>TOTAL: <span  style="color: #CD1231;"><?php echo number_format($total->total); ?></span></h1>
	                </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

</section>




