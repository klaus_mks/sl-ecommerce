<?php
$this->breadcrumbs = array(
	//$model->label(2) => array('index'),
	Yii::t('app', 'Order management'),
);
// make the pager float right
Yii::app()->clientScript->registerCss('floatRightPager', 'div.dataTables_paginate ul.pagination{ float:right; }');
?>

<section class="content-header">
    <h1>
        Order Management
        <small>order list</small>
    </h1>

    <!-- breadcrumbs -->
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			//'tagName' => 'span',
			'links'=>$this->breadcrumbs,
			'htmlOptions' => array(
				'class' => 'breadcrumb'
			)
		)); ?>
	<?php endif?>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">           
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
               	<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'data-grid',
						//'htmlOptions' => array('class' => 'table table-bordered table-hover dataTable'),
						'itemsCssClass' => 'table table-bordered table-hover dataTable',
						'dataProvider' => $model->shopSearch(),
						'template' => "{items}\n<div class=\"row\">{summary}\n{pager}</div>",
						'summaryCssClass' => 'col-xs-6',
						'pagerCssClass' => 'col-xs-6 dataTables_paginate paging_bootstrap',
						'pager' => array(
							//class => 'class name'
							'htmlOptions' => array(		// class for ul tag
								'class' => 'pagination',

							),
							'header' => false,
							'firstPageLabel' => '<<',
                            'prevPageLabel'  => '<',
                            'nextPageLabel'  => '>',
                            'lastPageLabel'  => '>>',
                            'firstPageCssClass' => 'hidden',
                            'lastPageCssClass' => 'hidden',
                            'selectedPageCssClass' => 'active',
						),
						// 'ajaxUpdate' => 'category-grid',
						// 'ajaxUrl'=> Yii::app()->request->getUrl(),
						'filter' => $model,
						'columns' => array(
							array('name' => 'id', 'header' => '#',
								'filter' => false
							),
							array('name' => 'customer_name', 'header' => 'Customer Info', 'type' => 'html', 
								'value' => '$data->getCustomerOrder()', 
							),
							array('name' => 'shipping_address', 'header' => 'Product', 'type' => 'html', 
								'value' => '$data->getInfoOrder()',
								'filter' => false
							),
							array(
								'name' => 'order_date', 'header' => 'Ordered date', 'filter' => false
							),
							// array('name' => 'id', 'header' => 'Total price','type' => 'html', 
							// 	'value' => '$data->getTotalOrder()', 'filter' => false
							// ),
							array(
					            'class'=>'DataColumn',
					            'type'=>'raw',
					            'name'=>'status',
					            'value'=> 'Order::model()->changeStatus($data->status)',
					            'evaluateHtmlOptions'=>true,
					            'htmlOptions'=>array('data-id'=>'"{$data->id}"'),
					            'filter' => array(
									Order::STATUS_NEW => Yii::t('app', 'New'), 
									Order::STATUS_APPROVED => Yii::t('app', 'Approved'),
									Order::STATUS_DELIVERED => Yii::t('app', 'Delivered'),
									Order::STATUS_PENDING => Yii::t('app', 'Pending'),
								),
					        ),
							array(
								'class' => 'CButtonColumn',
								'template' => '{update} {delete}',
								'buttons' => array(
									'delete' => array(
										'click' => 'js:Admin.confirmDelete'
									)
								)
							),
						),
						
					)); ?>

                

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

</section>

<div id="modalDeleteConfirm">
	<form id="frmDelete" action="" method="post">
		<p>Are you sure you want to delete this item?</p>
		<ul class="button clearFix">
			<li class="btn-cancel"><a href="#" class="btn btn-flat btn-default">Cancel</a></li>
			<li class="btn-process"><a href="#" class="btn btn-flat btn-success process-link">Process</a></li>
		</ul>
	</form>
</div>

<div id="modalChangeStatusConfirm">
	<form id="frmDelete" action="" method="post">
		<p>Are you sure you want to change status this item?</p>
		<ul class="button clearFix">
			<li class="btn-cancel"><a href="#" class="btn btn-flat btn-default">Cancel</a></li>
			<li class="btn-process"><a href="#" class="btn btn-flat btn-success process-link">Process</a></li>
		</ul>
	</form>
</div>



