<?php 
    $form = $this->beginWidget('GxActiveForm', array(
		'id' => 'attribute-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array('class' => 'form-horizontal', 'role' => "form")
	));
?>
    <div class="box-body">
    	<?php if( Yii::app()->user->hasFlash('update-success')) : ?>
    		<div class="alert alert-success alert-dismissable">
	            <i class="fa fa-check"></i>
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <!-- <b>Alert!</b> --> <?php echo Yii::app()->user->getFlash('update-success'); ?> - <a href="<?php echo Yii::app()->createUrl('category/admin'); ?>">back to list</a>
	        </div>
    	<?php endif; ?>

    	<?php #echo $form->errorSummary($model); ?>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'name', array('class' => 'col-md-2')); ?>
            <!-- <label for="exampleInputEmail1" class="col-md-2">Email address</label> -->
            <div class="col-md-6">
            	<!-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"> -->
            	<?php echo $form->textField($model, 'name', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'name', array('class' => 'text-red')); ?>
            </div>
        </div>
        
    </div><!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
<!-- </form> -->
<?php $this->endWidget(); ?>

