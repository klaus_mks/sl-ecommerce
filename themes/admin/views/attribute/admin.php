<!-- custom CGridview delete button
http://stackoverflow.com/questions/10542952/yii-cgridview-add-custom-function-to-the-delete-row-button
http://www.yiiframework.com/doc/api/1.1/CButtonColumn#buttons-detail -->

<?php
$this->breadcrumbs = array(
	//$model->label(2) => array('index'),
	Yii::t('app', 'Attribute management'),
);

// make the pager float right
Yii::app()->clientScript->registerCss('floatRightPager', 'div.dataTables_paginate ul.pagination{ float:right; }');
?>

<section class="content-header">
    <h1>
        Attribute Management
        <small>attribute list</small>
    </h1>

    <!-- breadcrumbs -->
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			//'tagName' => 'span',
			'links'=>$this->breadcrumbs,
			'htmlOptions' => array(
				'class' => 'breadcrumb'
			)
		)); ?>
	<?php endif?>

    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol> -->
</section>

<!-- <a href="#" class="btn btn-primary">New Category</a> -->


<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <!-- <h3 class="box-title">Hover Data Table</h3>                                     -->
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                	<?php echo CHtml::link('Add new attribute',array('attribute/create'), array('class'=>'btn btn-primary')); ?>
                	<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'data-grid',
						//'htmlOptions' => array('class' => 'table table-bordered table-hover dataTable'),
						'itemsCssClass' => 'table table-bordered table-hover dataTable',
						'dataProvider' => $model->shopSearch(),
						'template' => "{items}\n<div class=\"row\">{summary}\n{pager}</div>",
						'summaryCssClass' => 'col-xs-6',
						'pagerCssClass' => 'col-xs-6 dataTables_paginate paging_bootstrap',
						'pager' => array(
							//class => 'class name'
							'htmlOptions' => array(		// class for ul tag
								'class' => 'pagination',

							),
							'header' => false,
							'firstPageLabel' => '<<',
                            'prevPageLabel'  => '<',
                            'nextPageLabel'  => '>',
                            'lastPageLabel'  => '>>',
                            'firstPageCssClass' => 'hidden',
                            'lastPageCssClass' => 'hidden',
                            'selectedPageCssClass' => 'active',
						),
						// 'ajaxUpdate' => 'category-grid',
						// 'ajaxUrl'=> Yii::app()->request->getUrl(),
						'filter' => $model,
						'columns' => array(
							//'id',
							'name',
							array(
								'class' => 'CButtonColumn',
								'template' => '{update} {delete}',
								'buttons' => array(
									'delete' => array(
										'click' => 'js:Admin.confirmDelete'
									)
								)
							),
						),
						
					)); ?>

                

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

</section>

<div id="modalDeleteConfirm">
	<form id="frmDelete" action="" method="post">
		<p>Are you sure you want to delete this item?</p>
		<ul class="button clearFix">
			<li class="btn-cancel"><a href="#" class="btn btn-flat btn-default">Cancel</a></li>
			<li class="btn-process"><a href="#" class="btn btn-flat btn-success process-link">Process</a></li>
		</ul>
	</form>
</div>




