<?php

$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	Yii::t('app', 'New'),
);

// $this->menu = array(
// 	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url' => array('index')),
// 	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
// );
?>

<section class="content-header">
    <h1>
        New Attribute
        <small>Add new attribute</small>
    </h1>

    <!-- breadcrumbs -->
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			//'tagName' => 'span',
			'links'=>$this->breadcrumbs,
			'htmlOptions' => array(
				'class' => 'breadcrumb'
			)
		)); ?>
	<?php endif?>

    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol> -->
</section>

<section class="content">
    <div class="row">
       	<div class="col-md-12">
       		<div class="box box-primary">
                <div class="box-header">
                    <!-- <h3 class="box-title">Quick Example</h3> -->
                </div><!-- /.box-header -->
                <!-- form start -->
                <!-- <form role="form" class="form-horizontal"> -->
                <?php $this->renderPartial('_form', array('model' => $model )); ?>

                
            </div><!-- /.box -->
       	</div>
    </div>
</section>

