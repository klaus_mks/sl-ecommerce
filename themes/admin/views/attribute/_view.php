<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('name')); ?>:
	<?php echo GxHtml::encode($data->name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('created_by')); ?>:
	<?php echo GxHtml::encode($data->created_by); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('modified_by')); ?>:
	<?php echo GxHtml::encode($data->modified_by); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('created_at')); ?>:
	<?php echo GxHtml::encode($data->created_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('modified_at')); ?>:
	<?php echo GxHtml::encode($data->modified_at); ?>
	<br />

</div>