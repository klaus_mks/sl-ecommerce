<?php

$baseUrl = Yii::app()->baseUrl. DIRECTORY_SEPARATOR;
Yii::app()->clientScript->registerScriptFile($baseUrl. 'js/dropzone.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl. DIRECTORY_SEPARATOR. 'js/image-upload.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerCssFile($baseUrl. 'css/dropzone.css');


$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	GxHtml::valueEx($model) => array('update', 'id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Update'),
);

$this->menu = array(
	array('label' => Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label' => Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label' => Yii::t('app', 'View') . ' ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
	array('label' => Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>


<section class="content-header">
    <h1><?php echo Yii::t('app', 'Update') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>
    <!-- breadcrumbs -->
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			//'tagName' => 'span',
			'links'=>$this->breadcrumbs,
			'htmlOptions' => array(
				'class' => 'breadcrumb'
			)
		)); ?>
	<?php endif?>

    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol> -->
</section>
<section class="content">
    <div class="row">
       	<div class="col-md-12">
       		<div class="box box-primary">
                <div class="box-header">
                    <!-- <h3 class="box-title">Quick Example</h3> -->
                </div><!-- /.box-header -->
                <!-- form start -->
                <!-- <form role="form" class="form-horizontal"> -->
                <?php
					$this->renderPartial('_form', array(
							'model' => $model, 'pAttribute' => $pAttribute, 'pAttributeData' => $pAttributeData));
				?>

                
            </div><!-- /.box -->
       	</div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Product Images</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <!-- <form role="form" class="form-horizontal"> -->
                <?php
					$this->renderPartial('_image', array(
							'model' => $model));
				?>

                
            </div><!-- /.box -->
    	</div>
    </div>
</section>

<div id="modalDeleteConfirm">
	<form id="frmDelete" action="" method="post">
		<p>Are you sure you want to delete this item?</p>
		<ul class="button clearFix">
			<li class="btn-cancel"><a href="#" class="btn btn-flat btn-default">Cancel</a></li>
			<li class="btn-delete"><a href="#" class="btn btn-flat btn-danger process-link">Delete</a></li>
		</ul>
	</form>
</div>

<div id="modalConfirmDialog">
	<form id="frmDelete" action="" method="post">
		<p>Are you sure you want to set this image as active?</p>
		<ul class="button clearFix">
			<li class="btn-cancel"><a href="#" class="btn btn-flat btn-default">Cancel</a></li>
			<li class="btn-make-active"><a href="#" class="btn btn-flat btn-success process-link">Process</a></li>
		</ul>
	</form>
</div>
