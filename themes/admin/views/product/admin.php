<?php
#Yii::import('application.helpers.ShoppyHelper');
$this->breadcrumbs = array(
	//$model->label(2) => array('index'),
	Yii::t('app', 'Product management'),
);

// make the pager float right
Yii::app()->clientScript->registerCss('floatRightPager', 'div.dataTables_paginate ul.pagination{ float:right; }');
?>

<section class="content-header">
    <h1>
        Product Management
        <small>product list</small>
    </h1>

    <!-- breadcrumbs -->
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			//'tagName' => 'span',
			'links'=>$this->breadcrumbs,
			'htmlOptions' => array(
				'class' => 'breadcrumb'
			)
		)); ?>
	<?php endif?>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <!-- <h3 class="box-title">Hover Data Table</h3>                                     -->
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                <?php echo CHtml::link('Add new product',array('product/create'), array('class'=>'btn btn-primary')); ?>
                <?php $this->widget('zii.widgets.grid.CGridView', array(
						'id' => 'data-grid',
						//'htmlOptions' => array('class' => 'table table-bordered table-hover dataTable'),
						'itemsCssClass' => 'table table-bordered table-hover dataTable',
						'dataProvider' => $model->shopSearch(),
						'template' => "{items}\n<div class=\"row\">{summary}\n{pager}</div>",
						'summaryCssClass' => 'col-xs-6',
						'pagerCssClass' => 'col-xs-6 dataTables_paginate paging_bootstrap',
						'pager' => array(
							//class => 'class name'
							'htmlOptions' => array(		// class for ul tag
								'class' => 'pagination',

							),
							'header' => false,
							'firstPageLabel' => '<<',
                            'prevPageLabel'  => '<',
                            'nextPageLabel'  => '>',
                            'lastPageLabel'  => '>>',
                            'firstPageCssClass' => 'hidden',
                            'lastPageCssClass' => 'hidden',
                            'selectedPageCssClass' => 'active',
						),
						// 'ajaxUpdate' => 'category-grid',
						// 'ajaxUrl'=> Yii::app()->request->getUrl(),
						'filter' => $model,
						'columns' => array(
							array('name' => '', 'header' => "#", 'type' => 'html', 'sortable' => false, 'filter' => false,
								'value' => '$data->getActiveProductImage($data->productImages, 80, 80 ) ? CHtml::image($data->getActiveProductImage($data->productImages, 80, 80 )) : CHtml::image(ShoppyHelper::getNoImage(80, 80 ))'
							),
							//'id',
							array('name' => 'name', 'type' => 'html', 
								'value' => '$data->getDetails()'
							),
							//'name',
							//'shop_id',
							array(
								'name' => 'description',
								'value' => 'ShoppyHelper::wordLimit($data->description, 7, " ...");'
							),
							array(            
	           					'name'=>'category_id',
								'value'=>'GxHtml::valueEx($data->category)',
								'filter'=>GxHtml::listDataEx(Category::model()->findAllAttributes(null, true)),

					        ),

							array(
								'name' => 'is_published', 'header' => 'Published',
								'value' => '($data->is_published == false) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
								//'filter' => false
								'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
							),
							array(
								'class' => 'CButtonColumn',
								'template' => '{update} {delete}',
								'buttons' => array(
									'delete' => array(
										'click' => 'js:Admin.confirmDelete'
									)
								)
							),
						),
						
					)); ?>

                

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

</section>

<div id="modalDeleteConfirm">
	<form id="frmDelete" action="" method="post">
		<p>Are you sure you want to delete this item?</p>
		<ul class="button clearFix">
			<li class="btn-cancel"><a href="#" class="btn btn-flat btn-default">Cancel</a></li>
			<li class="btn-process"><a href="#" class="btn btn-flat btn-success process-link">Process</a></li>
		</ul>
	</form>
</div>



