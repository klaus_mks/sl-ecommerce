<?php 
    $form = $this->beginWidget('GxActiveForm', array(
		'id' => 'product-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array('class' => 'form-horizontal', 'role' => "form")
	));
?>
    <div class="box-body">
    	<?php if( Yii::app()->user->hasFlash('update-success')) : ?>
    		<div class="alert alert-success alert-dismissable">
	            <i class="fa fa-check"></i>
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <!-- <b>Alert!</b> --> <?php echo Yii::app()->user->getFlash('update-success'); ?> - <a href="<?php echo Yii::app()->createUrl('product/admin'); ?>">back to list</a>
	        </div>
    	<?php endif; ?>
    	<?php if( Yii::app()->user->hasFlash('update-unsuccess')) : ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo Yii::app()->user->getFlash('update-unsuccess'); ?> - <a href="<?php echo Yii::app()->createUrl('product/admin'); ?>">back to list</a>
            </div>
        <?php endif; ?>

    	<?php //echo $form->errorSummary($model); ?>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'name_en', array('class' => 'col-md-2')); ?>
            <!-- <label for="exampleInputEmail1" class="col-md-2">Email address</label> -->
            <div class="col-md-6">
            	<!-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"> -->
            	<?php echo $form->textField($model, 'name_en', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'name_en', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'name_vi', array('class' => 'col-md-2')); ?>
            <!-- <label for="exampleInputEmail1" class="col-md-2">Email address</label> -->
            <div class="col-md-6">
                <!-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"> -->
                <?php echo $form->textField($model, 'name_vi', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'name_vi', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'Category', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->dropDownList($model,'category_id', Category::model()->listCate(),
              array('empty' => 'Select category', 'class' => 'form-control')); 
		
              ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'is_published', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->checkbox($model, 'is_published', array('class' => 'form-control')); ?>
            	<?php echo $form->error($model,'is_published'); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'is_new', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->checkbox($model, 'is_new', array('class' => 'form-control')); ?>
            	<?php echo $form->error($model,'is_new'); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'is_on_sale', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->checkbox($model, 'is_on_sale', array('class' => 'form-control')); ?>
            	<?php echo $form->error($model,'is_on_sale'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'is_promoted', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->checkbox($model, 'is_promoted', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'is_promoted'); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'sku', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'sku', array('class' => 'form-control')); ?>
            </div>
        </div>
        <?php /*
        <div class="form-group">
        	<?php echo $form->labelEx($model,'size', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'size', array('class' => 'form-control')); ?>
            	<?php echo $form->error($model,'size'); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'color', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'color', array('class' => 'form-control')); ?>
            	<?php echo $form->error($model,'color'); ?>
            </div>
        </div>
        */ ?>
        <div class="form-group">
            <?php echo $form->labelEx($model,'old_price', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'old_price', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'price', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'price', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'description_en', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textArea($model, 'description_en', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'description_vi', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textArea($model, 'description_vi', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'seo_description', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textArea($model, 'seo_description', array('class' => 'form-control')); ?>
            </div>
        </div>

        <!-- product attributes -->
        <?php if(!empty($pAttributeData)):?>
        <?php foreach( $pAttributeData as $a ): ?>
	    <div class="form-group attr-group">
        	<div class="col-md-2">
        	</div>
            <div class="col-md-3">
            	<!-- for pre selected option, see CHtml class, line 1799 -->
            	<?php echo $form->dropDownList($pAttribute,'attribute_id[]', GxHtml::listDataEx(Attribute::model()->findAllAttributes(null, true )),
              		array(
              			'empty' => '(Select an attribute)	', 'class' => 'form-control',
              			'options' => array( $a->attribute_id => array('selected' => "selected"))
              		)); 		
              	?>
            </div>
            <div class="col-md-2">
            	<?php echo $form->textField($pAttribute, 'value[]', array('class' => 'form-control', 'value' => $a->value )); ?>
            </div>
            <div class="col-md-1">
            	<a href="#" class="delete-attr">X</a>
            </div>
        </div>	
	    <?php endforeach; ?>
		<?php endif; ?>

        <div class="form-group attr-group">
        	<div class="col-md-2">
        	</div>
            <div class="col-md-3">
            	<?php echo $form->dropDownList($pAttribute,'attribute_id[]', GxHtml::listDataEx(Attribute::model()->findAllAttributes(null, true )),
              		array('empty' => '(Select an attribute)	', 'class' => 'form-control')); 		
              	?>
            </div>
            <div class="col-md-2">
            	<?php echo $form->textField($pAttribute, 'value[]', array('class' => 'form-control')); ?>
            </div>
            <div class="col-md-1">
            	<a href="#" class="delete-attr">X</a>
            </div>
        </div>

        <div class="form-group">
        	<div class="col-md-3">
        	</div>
            <div class="col-md-3">
            </div>
            <div class="col-md-2">
            	<a href="javascript:void(0);" class="add-attr">Add new attribute</a>
            </div>
        </div>


		

    </div><!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
<!-- </form> -->
<?php $this->endWidget(); ?>




<?php
/* PRODUCT
<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'product-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList($model, 'category_id', GxHtml::listDataEx(Category::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'category_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'shop_id'); ?>
		<?php echo $form->dropDownList($model, 'shop_id', GxHtml::listDataEx(Shop::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'shop_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sku'); ?>
		<?php echo $form->textField($model, 'sku', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'sku'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model, 'price'); ?>
		<?php echo $form->error($model,'price'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'is_published'); ?>
		<?php echo $form->checkBox($model, 'is_published'); ?>
		<?php echo $form->error($model,'is_published'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
		<?php echo $form->error($model,'description'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'seo_description'); ?>
		<?php echo $form->textField($model, 'seo_description', array('maxlength' => 2048)); ?>
		<?php echo $form->error($model,'seo_description'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model, 'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'modified_by'); ?>
		<?php echo $form->textField($model, 'modified_by'); ?>
		<?php echo $form->error($model,'modified_by'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'modified_at'); ?>
		<?php echo $form->textField($model, 'modified_at'); ?>
		<?php echo $form->error($model,'modified_at'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('comments')); ?></label>
		<?php echo $form->checkBoxList($model, 'comments', GxHtml::encodeEx(GxHtml::listDataEx(Comment::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('productImages')); ?></label>
		<?php echo $form->checkBoxList($model, 'productImages', GxHtml::encodeEx(GxHtml::listDataEx(ProductImage::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->

*/?>