<?php
	$c = (int)ceil(count($image) / 6 + 0.5 );
	for( $j = 0; $j < $c; $j ++ ) :		
?>
<div class="row">
	<?php 
	for( $k = 0; $k < 6 && $j * 6 + $k < count($image); $k ++ ): 
		$img = ShoppyHelper::getThumbnailPath(Yii::app()->params['product']['image_path']. '/'. $image[$j * 6 + $k]['name'], 145);
	?>			
		<div class="col-sm-6 col-md-2">
		    <div class="thumbnail">
		      <img src="<?php echo $img; ?>" alt="...">
		      <div class="caption">
		        	<ul class="clearFix">
		        		<li class="delete">
		        			<a href="<?php echo Yii::app()->createUrl('product/deleteImage', array('id' => $image[$j * 6 + $k]['id'])); ?>" class="btn-lg" role="button" alt="Delete" title="Delete">
		        				<span class="glyphicon glyphicon-remove"></span>
		        			</a> 
		        		</li>
		        		<li class="make-active">
		        			<?php 
		        				$link = Yii::app()->createUrl('product/activeImage', array('id' => $image[$j * 6 + $k]['id']));
		        			?>
		        			<?php if( !$image[$j * 6 + $k]['is_actived']): ?>
		        			<a href="<?php echo $link  ?>" class="btn-lg" role="button" alt="Make actived" title="Make actived">
		        				<span class="glyphicon glyphicon-<?php echo $image[$j * 6 + $k]['is_actived'] ? "ok" : "picture"; ?>"></span>
		        			</a> 
		        			<?php else: ?>
		        			<span class="btn-lg">
		        				<span class="glyphicon glyphicon-ok"></span>
		        			</span> 
		        			<?php endif; ?>
		        		</li>
		        	</ul>
		      </div>
		    </div>
	  	</div>
	<?php endfor; ?>
</div>
<?php endfor; ?>