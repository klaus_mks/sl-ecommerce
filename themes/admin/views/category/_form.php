<?php 
    $form = $this->beginWidget('GxActiveForm', array(
		'id' => 'category-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array('class' => 'form-horizontal', 'role' => "form", 'enctype' => 'multipart/form-data')
	));
?>
    <div class="box-body">
    	<?php if( Yii::app()->user->hasFlash('update-success')) : ?>
    		<div class="alert alert-success alert-dismissable">
	            <i class="fa fa-check"></i>
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <!-- <b>Alert!</b> --> <?php echo Yii::app()->user->getFlash('update-success'); ?> - <a href="<?php echo Yii::app()->createUrl('category/admin'); ?>">back to list</a>
	        </div>
    	<?php endif; ?>

    	<?php if( Yii::app()->user->hasFlash('update-unsuccess')) : ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo Yii::app()->user->getFlash('unpdate-usuccess'); ?> - <a href="<?php echo Yii::app()->createUrl('category/admin'); ?>">back to list</a>
            </div>
        <?php endif; ?>
    	

    	<?php #echo $form->errorSummary($model); ?>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'name_en', array('class' => 'col-md-2')); ?>
            <!-- <label for="exampleInputEmail1" class="col-md-2">Email address</label> -->
            <div class="col-md-6">
            	<!-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"> -->
            	<?php echo $form->textField($model, 'name_en', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'name_en', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'name_vi', array('class' => 'col-md-2')); ?>
            <!-- <label for="exampleInputEmail1" class="col-md-2">Email address</label> -->
            <div class="col-md-6">
            	<!-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"> -->
            	<?php echo $form->textField($model, 'name_vi', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'name_vi', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'url', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'url', array('maxlength' => 200, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'url', array('class' => 'text-red')); ?>
            </div>
        </div>
        
        <div class="form-group">
        	<?php echo $form->labelEx($model,'is_published', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->checkbox($model, 'is_published', array('class' => 'form-control')); ?>
            	<?php echo $form->error($model,'is_published'); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'is_promoted', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->checkbox($model, 'is_promoted', array('class' => 'form-control')); ?>
            	<?php echo $form->error($model,'is_promoted'); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'parent_id', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->dropDownList($model,'parent_id', $model->listCate(), array('empty'=>'Select container category', 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'parent_id'); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'description_en', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textArea($model, 'description_en', array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'description_vi', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textArea($model, 'description_vi', array('class' => 'form-control')); ?>
            </div>
        </div>

        <?php if($model->image): ?>
        <div class="form-group">
        	<div class="col-md-2"></div>
        	<div class="col-md-2">
            	<img src="<?php echo Yii::app()->params['category']['image_path']. DIRECTORY_SEPARATOR. $model->image; ?>" style="max-width: 200px" />
            </div>
            <br/>
	    </div>
	    <?php endif; ?>
	    <div class="form-group">
	    	<div class="col-md-2">
	    	</div>
	    	<div class="col-md-6">
	    		<?php echo $form->fileField($model, 'image'); ?>
	    	</div>
	    </div>
	    

    </div><!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
<!-- </form> -->
<?php $this->endWidget(); ?>

<?php /*

<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'category-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'is_published'); ?>
		<?php echo $form->checkBox($model, 'is_published'); ?>
		<?php echo $form->error($model,'is_published'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'shop_id'); ?>
		<?php echo $form->textField($model, 'shop_id'); ?>
		<?php echo $form->error($model,'shop_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
		<?php echo $form->error($model,'description'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'seo_description'); ?>
		<?php echo $form->textField($model, 'seo_description', array('maxlength' => 2048)); ?>
		<?php echo $form->error($model,'seo_description'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model, 'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'modified_by'); ?>
		<?php echo $form->textField($model, 'modified_by'); ?>
		<?php echo $form->error($model,'modified_by'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'modified_at'); ?>
		<?php echo $form->textField($model, 'modified_at'); ?>
		<?php echo $form->error($model,'modified_at'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('products')); ?></label>
		<?php echo $form->checkBoxList($model, 'products', GxHtml::encodeEx(GxHtml::listDataEx(Product::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->

*/ ?>