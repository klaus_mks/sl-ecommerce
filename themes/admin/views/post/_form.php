<?php 
    Yii::import('ext.redactor.ERedactorWidget');
    $form = $this->beginWidget('GxActiveForm', array(
		'id' => 'category-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array('class' => 'form-horizontal', 'role' => "form", 'enctype' => 'multipart/form-data')
	));
?>
    <div class="box-body">
    	<?php if( Yii::app()->user->hasFlash('update-success')) : ?>
    		<div class="alert alert-success alert-dismissable">
	            <i class="fa fa-check"></i>
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <!-- <b>Alert!</b> --> <?php echo Yii::app()->user->getFlash('update-success'); ?> - <a href="<?php echo Yii::app()->createUrl('post/admin'); ?>">back to list</a>
	        </div>
    	<?php endif; ?>

    	<?php if( Yii::app()->user->hasFlash('update-unsuccess')) : ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo Yii::app()->user->getFlash('update-usuccess'); ?> - <a href="<?php echo Yii::app()->createUrl('post/admin'); ?>">back to list</a>
            </div>
        <?php endif; ?>
    	

    	<?php #echo $form->errorSummary($model); ?>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'title_en', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textField($model, 'title_en', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'title_en', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'title_vi', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">            
            	<?php echo $form->textField($model, 'title_vi', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'title_vi', array('class' => 'text-red')); ?>
            </div>
        </div>

        <!-- EN CONTENT -->
        <div class="form-group">
            <?php echo $form->labelEx($model,'content_en', array('class' => 'col-md-2')); ?>

            <div class="col-md-6">
            <?php
            $attribute = 'content_en';
            $this->widget('ext.redactor.ERedactorWidget',array(
                'model' => $model,
                'attribute' => $attribute,
                'options'=>array(
                    'fileUpload'=>Yii::app()->createUrl('post/fileUpload',array(
                        'attr' => $attribute
                    )),
                    'fileUploadErrorCallback'=>new CJavaScriptExpression(
                        'function(obj,json) { alert(json.error); }'
                    ),
                    'imageUpload'=>Yii::app()->createUrl('post/imageUpload',array(
                        'attr' => ""
                    )),
                    'imageGetJson'=>Yii::app()->createUrl('post/imageList',array(
                        'attr' => $attribute
                    )),
                    'imageUploadErrorCallback'=>new CJavaScriptExpression(
                        'function(obj,json) { alert(json.error); }'
                    ),
                ),
            ));
            ?>
            </div>
        </div>

        <!-- VI CONTENT -->
        <div class="form-group">
        	<?php echo $form->labelEx($model,'content_vi', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            <?php
            $attribute = 'content_vi';
            $this->widget('ext.redactor.ERedactorWidget',array(
                'model' => $model,
                'attribute' => $attribute,
                'options'=>array(
                    'fileUpload'=>Yii::app()->createUrl('post/fileUpload',array(
                        'attr' => $attribute
                    )),
                    'fileUploadErrorCallback'=>new CJavaScriptExpression(
                        'function(obj,json) { alert(json.error); }'
                    ),
                    'imageUpload'=>Yii::app()->createUrl('post/imageUpload',array(
                        'attr' => ""
                    )),
                    'imageGetJson'=>Yii::app()->createUrl('post/imageList',array(
                        'attr' => $attribute
                    )),
                    'imageUploadErrorCallback'=>new CJavaScriptExpression(
                        'function(obj,json) { alert(json.error); }'
                    ),
                ),
            ));
            ?>
            </div>
        </div>

        
        <div class="form-group">
        	<?php echo $form->labelEx($model,'is_published', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->checkbox($model, 'is_published', array('class' => 'form-control')); ?>
            	<?php echo $form->error($model,'is_published'); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'is_on_homepage', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->checkbox($model, 'is_on_homepage', array('class' => 'form-control')); ?>
            	<?php echo $form->error($model,'is_on_homepage'); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'description', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textArea($model, 'description', array('class' => 'form-control', 'rows' => 7)); ?>
            	<?php echo $form->error($model,'description', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'seo_description', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->textArea($model, 'seo_description', array('class' => 'form-control', 'rows' => 3)); ?>
            	<?php echo $form->error($model,'seo_description', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
        	<?php echo $form->labelEx($model,'image', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php if($model->image): ?>
			        <div class="controls">
			        	<?php echo CHtml::image($model->getPostImage(80, 80 )); ?>
			        </div>
			        <br/>
			    <?php endif; ?>
			    <?php echo $form->fileField($model, 'image'); ?>
            </div>
        </div>

        

	

    </div><!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-primary submit-post">Submit</button>
    </div>
<!-- </form> -->
<?php $this->endWidget(); ?>
