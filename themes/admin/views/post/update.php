<?php

//Yii::app()->clientScript->registerCssFile('/css/prettify.css');

Yii::app()->clientScript->registerCss('bootstrap-editor', 
    '#bootstrap-editor1, #bootstrap-editor2 {overflow:scroll; min-height:400px; padding: 5px; border: 1px solid #ddd;}'
);

Yii::app()->clientScript->registerScriptFile('/js/jquery.hotkeys.js', CClientScript::POS_BEGIN);
//Yii::app()->clientScript->registerScriptFile('/js/prettify.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile('/js/bootstrap-wysiwyg.js', CClientScript::POS_BEGIN);

Yii::app()->clientScript->registerScript('init_wysiwyg','
$(document).ready(function(){
	$("#bootstrap-editor1").wysiwyg({ toolbarSelector: "[data-role=editor-toolbar1]"} );
	$("#bootstrap-editor2").wysiwyg({ toolbarSelector: "[data-role=editor-toolbar2]"} );
});

// get editor content
$(".submit-post").click(function(e){
    $("textarea.content_en").html($("#bootstrap-editor1").html());
    $("textarea.content_vi").html($("#bootstrap-editor2").html());
});

', CClientScript::POS_READY);

// $themePath = Yii::app()->theme->baseUrl. DIRECTORY_SEPARATOR;
// Yii::app()->clientScript->registerCssFile($themePath. 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.css');
// //Yii::app()->clientScript->registerScriptFile($themePath. 'js/plugins/ckeditor/ckeditor.js', CClientScript::POS_END);
// Yii::app()->clientScript->registerScriptFile($themePath. 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js', CClientScript::POS_END);

// Yii::app()->clientScript->registerScript('init_wysiwyg','
// $(document).ready(function(){
// 	$("#bootstrap-editor1, #bootstrap-editor2").wysihtml5();
// });
// ', CClientScript::POS_READY);

$this->breadcrumbs = array(
    $model->label(2) => array('admin'),
    GxHtml::valueEx($model) => array('update', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);?>

<section class="content-header">
    <h1>
        Update Post
        <small>Update post</small>
    </h1>

    <!-- breadcrumbs -->
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			//'tagName' => 'span',
			'links'=>$this->breadcrumbs,
			'htmlOptions' => array(
				'class' => 'breadcrumb'
			)
		)); ?>
	<?php endif?>

    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol> -->
</section>

<section class="content">
    <div class="row">
       	<div class="col-md-12">
       		<div class="box box-primary">
                <div class="box-header">
                    <!-- <h3 class="box-title">Quick Example</h3> -->
                </div><!-- /.box-header -->
                <!-- form start -->
                <!-- <form role="form" class="form-horizontal"> -->
                <?php $this->renderPartial('_form', array('model' => $model )); ?>

                
            </div><!-- /.box -->
       	</div>
    </div>
</section>
