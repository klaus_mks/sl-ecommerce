<?php

$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	Yii::t('app', 'New'),
);
?>

<section class="content-header">
    <h1>
        New Product
        <small>Add new product</small>
    </h1>

    <!-- breadcrumbs -->
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			//'tagName' => 'span',
			'links'=>$this->breadcrumbs,
			'htmlOptions' => array(
				'class' => 'breadcrumb'
			)
		)); ?>
	<?php endif?>
</section>

<section class="content">
    <div class="row">
       	<div class="col-md-12">
       		<div class="box box-primary">
                <div class="box-header">
                    <!-- <h3 class="box-title">Quick Example</h3> -->
                </div><!-- /.box-header -->
                <!-- form start -->
                <!-- <form role="form" class="form-horizontal"> -->
                <?php $this->renderPartial('_form', array('model' => $model )); ?>

                
            </div><!-- /.box -->
       	</div>
    </div>
</section>

