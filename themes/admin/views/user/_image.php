<div id="image-list-container">	
	<div id="image-list">
		<div class="row">
		  <div class="col-sm-6 col-md-2">
		    <div class="thumbnail">
		      <img src="/images/product/sample.jpg" alt="...">
		      <div class="caption">
		        	<a href="#" class="btn btn-flat btn-sm btn-danger" role="button">Delete</a> 
		        	<a href="#" class="btn btn-flat btn-sm btn-default" role="button">Button</a>
		      </div>
		    </div>
		  </div>

		  <div class="col-sm-6 col-md-2">
		    <div class="thumbnail">
		      <img src="/images/product/sample.jpg" alt="...">
		      <div class="caption">
		        	<a href="#" class="btn btn-flat btn-sm btn-danger" role="button">Delete</a> 
		        	<a href="#" class="btn btn-flat btn-sm btn-default" role="button">Button</a>
		      </div>
		    </div>
		  </div>

		  <div class="col-sm-6 col-md-2">
		    <div class="thumbnail">
		      <img src="/images/product/sample.jpg" alt="...">
		      <div class="caption">
		        	<a href="#" class="btn btn-flat btn-sm btn-danger" role="button">Delete</a> 
		        	<a href="#" class="btn btn-flat btn-sm btn-default" role="button">Button</a>
		      </div>
		    </div>
		  </div>

		  <div class="col-sm-6 col-md-2">
		    <div class="thumbnail">
		      <img src="/images/product/sample.jpg" alt="...">
		      <div class="caption">
		        	<a href="#" class="btn btn-flat btn-sm btn-danger" role="button">Delete</a> 
		        	<a href="#" class="btn btn-flat btn-sm btn-default" role="button">Button</a>
		      </div>
		    </div>
		  </div>

		  <div class="col-sm-6 col-md-2">
		    <div class="thumbnail">
		      <img src="/images/product/sample.jpg" alt="...">
		      <div class="caption">
		        	<a href="#" class="btn btn-flat btn-sm btn-danger" role="button">Delete</a> 
		        	<a href="#" class="btn btn-flat btn-sm btn-default" role="button">Button</a>
		      </div>
		    </div>
		  </div>

		</div>
	</div>
</div>

<form action="<?php echo Yii::app()->createUrl('product/imageUpload'); ?>"
      class="dropzone"
      image-url="<?php echo Yii::app()->createUrl('product/imageLoad'); ?>"
      id="my-awesome-dropzone">

      <input type="hidden" name="id" value="<?php echo $model->id; ?>" />
</form>
