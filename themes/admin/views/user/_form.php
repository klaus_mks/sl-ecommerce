<?php 
    $form = $this->beginWidget('GxActiveForm', array(
		'id' => 'product-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array('class' => 'form-horizontal', 'role' => "form")
	));
?>
    <div class="box-body">
    	<?php if( Yii::app()->user->hasFlash('update-success')) : ?>
    		<div class="alert alert-success alert-dismissable">
	            <i class="fa fa-check"></i>
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <!-- <b>Alert!</b> --> <?php echo Yii::app()->user->getFlash('update-success'); ?> - <a href="<?php echo Yii::app()->createUrl('product/admin'); ?>">back to list</a>
	        </div>
    	<?php endif; ?>
        <?php if( Yii::app()->user->hasFlash('update-unsuccess')) : ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo Yii::app()->user->getFlash('update-unsuccess'); ?> - <a href="<?php echo Yii::app()->createUrl('user/admin'); ?>">back to list</a>
            </div>
        <?php endif; ?>
    	

    	<?php //echo $form->errorSummary($model); ?>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'Email', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
             	<?php echo $form->textField($model, 'email', array('maxlength' => 255, 'class' => 'form-control')); ?>
            	<?php echo $form->error($model,'email', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'is_published', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->checkbox($model, 'is_published', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'is_published'); ?>
            </div>
        </div>
        <div class="form-group">
        	<?php echo $form->labelEx($model,'Group', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
            	<?php echo $form->dropDownList($model,'group_id', Group::model()->listGroup(),
              		array('empty' => '(Select a group')); 
              	?>
              	<?php echo $form->error($model,'group_id', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'First name', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'first_name', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'first_name', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'Last name', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'last_name', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'last_name', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'Description', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textArea($model, 'description', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'description', array('class' => 'text-red')); ?>
            </div>
        </div>
    </div><!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
<!-- </form> -->
<?php $this->endWidget(); ?>




<?php
/* PRODUCT
<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'product-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList($model, 'category_id', GxHtml::listDataEx(Category::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'category_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'shop_id'); ?>
		<?php echo $form->dropDownList($model, 'shop_id', GxHtml::listDataEx(Shop::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'shop_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sku'); ?>
		<?php echo $form->textField($model, 'sku', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'sku'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model, 'price'); ?>
		<?php echo $form->error($model,'price'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'is_published'); ?>
		<?php echo $form->checkBox($model, 'is_published'); ?>
		<?php echo $form->error($model,'is_published'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
		<?php echo $form->error($model,'description'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'seo_description'); ?>
		<?php echo $form->textField($model, 'seo_description', array('maxlength' => 2048)); ?>
		<?php echo $form->error($model,'seo_description'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model, 'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'modified_by'); ?>
		<?php echo $form->textField($model, 'modified_by'); ?>
		<?php echo $form->error($model,'modified_by'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'modified_at'); ?>
		<?php echo $form->textField($model, 'modified_at'); ?>
		<?php echo $form->error($model,'modified_at'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('comments')); ?></label>
		<?php echo $form->checkBoxList($model, 'comments', GxHtml::encodeEx(GxHtml::listDataEx(Comment::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('productImages')); ?></label>
		<?php echo $form->checkBoxList($model, 'productImages', GxHtml::encodeEx(GxHtml::listDataEx(ProductImage::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->

*/?>