<?php

$baseUrl = Yii::app()->baseUrl. DIRECTORY_SEPARATOR;
Yii::app()->clientScript->registerScriptFile($baseUrl. 'js/dropzone.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl. DIRECTORY_SEPARATOR. 'js/image-upload.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl. DIRECTORY_SEPARATOR. 'js/banner.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerCssFile($baseUrl. 'css/dropzone.css');


$this->breadcrumbs = array(
	//$model->label(2) => array('admin'),	
	//GxHtml::valueEx($model) => array('update', 'id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Banner'),
);
?>

<?php
	#Yii::app()->clientScript->registerScript('bannerTooltip', '$("#image-list .caption ul.desc li.tooltip").tooltip();');
?>

<section class="content-header">
    <h1>
    	<?php echo Yii::t('app', 'Banner'); ?>
    	<small>Your banner image size should be 940x180 pixels (upper) 940x450 pixels (lower)</small>
    </h1>
    <!-- breadcrumbs -->
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			//'tagName' => 'span',
			'links'=>$this->breadcrumbs,
			'htmlOptions' => array(
				'class' => 'breadcrumb'
			)
		)); ?>
	<?php endif?>

    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol> -->
</section>
<section class="content">
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="box box-primary">
                <!-- <div class="box-header">
                    <h3 class="box-title">Banners</h3>
                </div> --><!-- /.box-header -->
                <!-- form start -->
                <!-- <form role="form" class="form-horizontal"> -->
                <?php
					$this->renderPartial('_image', array(
							'model' => $model));
				?>

				<select class="banner-position" name="position" style="display: none;">
					<?php foreach( $position as $k => $text ): ?>
					<option value="<?php echo $k; ?>"><?php echo $text; ?></option>
					<?php endforeach; ?>
				</select>
                
            </div><!-- /.box -->
    	</div>
    </div>
</section>

<div id="modalDeleteConfirm">
	<form id="frmDelete" action="" method="post">
		<p>Are you sure you want to delete this item?</p>
		<ul class="button clearFix">
			<li class="btn-cancel"><a href="#" class="btn btn-flat btn-default">Cancel</a></li>
			<li class="btn-delete"><a href="#" class="btn btn-flat btn-danger process-link">Delete</a></li>
		</ul>
	</form>
</div>

<div id="modalConfirmDialog">
	<form id="frmDelete" action="" method="post">
		<p>Are you sure you want to set this image as active?</p>
		<ul class="button clearFix">
			<li class="btn-cancel"><a href="#" class="btn btn-flat btn-default">Cancel</a></li>
			<li class="btn-make-active"><a href="#" class="btn btn-flat btn-success process-link">Process</a></li>
		</ul>
	</form>
</div>
