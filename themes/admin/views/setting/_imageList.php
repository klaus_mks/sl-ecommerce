<!-- this is the image list for banners -->
<?php if(!empty($upperBanner)): ?>
<h3>Upper banners</h3>
<?php $this->renderPartial('_banner', array('image' => $upperBanner)); ?>
<?php endif; ?>

<?php if(!empty($lowerBanner)): ?>
<h3>Lower banners</h3>
<?php $this->renderPartial('_banner', array('image' => $lowerBanner)); ?>
<?php endif; ?>

<div class="pop-content">
	<form class="frm-edit-banner" role="form" action="<?php echo Yii::app()->createUrl('setting/updateBanner'); ?>" method="post">
		<div class="form-group">
        	<input type="text" name="url" class="form-control" placeholder="http:// ..." value="">
        </div>
        <!-- <div class="form-group">
            <input type="text" name="description" class="form-control" placeholder="description ...">
        </div> -->
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-update-banner">Save</button>
        </div>
	</form>
	
</div>