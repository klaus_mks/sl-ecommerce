<?php
	$c = (int)ceil(count($image) / 6 + 0.5 );
	for( $j = 0; $j < $c; $j ++ ) :
		
?>
<div class="row">
	<?php 
	for( $k = 0; $k < 6 && $j * 6 + $k < count($image); $k ++ ): 
		$img = ShoppyHelper::getThumbnailPath(Yii::app()->params['general']['banner_path']. '/'. $image[$j * 6 + $k]['name'], 145);
	?>			
	<div class="col-sm-6 col-md-2">
	    <div class="thumbnail">
	      <img src="<?php echo $img; ?>" alt="...">
	      <div class="caption">
	      		<ul class="clearFix desc">
	      			<li><a href="javascript:void(0);" class="tz" data-toggle="tooltip" data-placement="top" title="<?php echo $image[$j * 6 + $k]['url']; ?>">url</a></li>
	      			<?php /*
	      			<li><a href="javascript:void(0);" class="tz" data-toggle="tooltip" data-placement="top" title="<?php echo $image[$j * 6 + $k]['description']; ?>">description</a></li>
	      			*/ ?>
	      		</ul>
	        	<ul class="clearFix">
	        		<li class="delete">
	        			<a href="<?php echo Yii::app()->createUrl('setting/deleteBanner', array('id' => $image[$j * 6 + $k]['id'])); ?>" class="btn-lg" role="button" alt="Delete" title="Delete">
	        				<span class="glyphicon glyphicon-remove"></span>
	        			</a> 
	        		</li>
	        		
	        		<li class="">
	        			<?php 
	        				$link = Yii::app()->createUrl('setting/updateBanner', array('id' => $image[$j * 6 + $k]['id']));
	        			?>
	        			<a href="javascript:void(0);" class="update-banner btn-lg" role="button" alt="Update" title="Update"
	        				banner-id="<?php echo $image[$j * 6 + $k]['id']; ?>"
	        				banner-url="<?php echo $image[$j * 6 + $k]['url']; ?>"
	        				banner-position="<?php echo $image[$j * 6 + $k]['position']; ?>"
	        			>
	        				<span class="glyphicon glyphicon-pencil"></span>
	        			</a> 

	        			
	        			
	        		</li>
	        		
	        	</ul>
	      </div>
	    </div>
  	</div>

  	<?php endfor; ?>
</div>
<?php endfor; ?>