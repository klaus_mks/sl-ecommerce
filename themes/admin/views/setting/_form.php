<?php 
    $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'setting-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('class' => 'form-horizontal', 'role' => "form", 'enctype' => 'multipart/form-data')
    ));
?>
    <div class="box-body">
        <?php if( Yii::app()->user->hasFlash('update-success')) : ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <!-- <b>Alert!</b> --> <?php echo Yii::app()->user->getFlash('update-success'); ?>
            </div>
        <?php endif; ?>

        <?php if( Yii::app()->user->hasFlash('update-unsuccess')) : ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo Yii::app()->user->getFlash('unpdate-usuccess'); ?>
            </div>
        <?php endif; ?>
        

        <?php #echo $form->errorSummary($model); ?>
        <div class="form-group">
            <?php echo $form->labelEx($model,'name', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'name', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'name', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'address', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'address', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'address', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'phone', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'phone', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'phone', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'cellphone', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'cellphone', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'cellphone', array('class' => 'text-red')); ?>
            </div>
        </div>
        <!-- <div class="form-group">
            <?php echo $form->labelEx($model,'longtitude', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'longtitude', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'longtitude', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'latitude', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'latitude', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'latitude', array('class' => 'text-red')); ?>
            </div>
        </div> -->
        <div class="form-group">
            <?php echo $form->labelEx($model,'fb_id', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'fb_id', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'fb_id', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'email', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'email', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'email', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'skype_id', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'skype_id', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'skype_id', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'yahoo_id', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'yahoo_id', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'yahoo_id', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'currency', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textField($model, 'currency', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'currency', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'google_maps', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textarea($model, 'google_maps', array('maxlength' => 500, 'class' => 'form-control', 'rows' => 4)); ?>
                <?php echo $form->error($model,'google_maps', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'About us', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textarea($model, 'contact_content', array('maxlength' => 355, 'id' => 'editor1', 'class' => 'form-control', 'rows' => 4)); ?>
                <?php echo $form->error($model,'contact_content', array('class' => 'text-red')); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'Meta Description', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textarea($model, 'meta_description', array('maxlength' => 355, 'class' => 'form-control', 'rows' => 4)); ?>
                <?php echo $form->error($model,'meta_description', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'Meta Keyword', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textarea($model, 'meta_keyword', array('maxlength' => 355, 'class' => 'form-control', 'rows' => 4)); ?>
                <?php echo $form->error($model,'meta_keyword', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'Meta Title', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->textarea($model, 'meta_title', array('maxlength' => 355, 'class' => 'form-control', 'rows' => 4)); ?>
                <?php echo $form->error($model,'meta_title', array('class' => 'text-red')); ?>
            </div>
        </div>

        <?php if($model->logo): ?>
        <div class="form-group">
            <div class="col-md-2"></div>
            <div class="col-md-2">
                <img src="<?php echo Yii::app()->params['general']['logo_path']. DIRECTORY_SEPARATOR. $model->logo; ?>" style="max-width: 200px" />
            </div>
            <br/>
        </div>
        <?php endif; ?>
        <div class="form-group">
            <?php echo $form->labelEx($model,'Logo', array('class' => 'col-md-2')); ?>
            <div class="col-md-6">
                <?php echo $form->fileField($model, 'logo'); ?>
                <?php echo Yii::t('backend', 'Your logo size should be 200x200 pixels'); ?>
            </div>
        </div>

    </div><!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
<!-- </form> -->
<?php $this->endWidget(); ?> 
<script>
    CKEDITOR.replace( 'editor1' );
</script>
