var ImageUpload = {
	loadImage: function(){
		$.ajax({
			url: $('#my-awesome-dropzone').attr('image-url'),
			type: "post",
			data: $('#my-awesome-dropzone').serialize(),
			dataType: "html",
			success: function(response){
				$('#image-list').html(response).fadeIn();
				$('body').trigger('loadImageComplete');						
			}
		});

	},
	
	bindDeleteImageEvent:function(){
		$('#image-list-container').on('click', '#image-list .caption li.delete a', function(e){
			Admin.confirmDelete(e);
		});

		$('body').on('click', '#modalDeleteConfirm li.btn-delete a', function(e){
			$(this).trigger('delete');
			e.preventDefault();
		});
	},

	bindActiveImageEvent:function(){
		$('#image-list-container').on('click', '#image-list .caption li.make-active a', function(e){
			Admin.confirmDialog(e, '#modalConfirmDialog');
		});

		$('body').on('click', '#modalConfirmDialog li.btn-make-active a', function(e){
			$(this).trigger('make-active');
			e.preventDefault();
		});
	},

	registerDeleteEventHandler: function(){
		$('body').on('delete', '#modalDeleteConfirm li.btn-delete a', function(e){
			ImageUpload.execDelete(e);			
		});
	},

	registerActiveImageEventHandler: function(){
		$('body').on('make-active', '#modalConfirmDialog li.btn-make-active a', function(e){
			ImageUpload.execMakeActive(e);			
		});
	},

	execDelete:function(e){
		var th = e.currentTarget;
		var url = $(th).attr('href');

		$.ajax({
			url: url,
			type: "post",
			//data: $('#my-awesome-dropzone').serialize(),
			//dataType: "html",
			success: function(response){
				$.modal.close();
				ImageUpload.loadImage();
			}
		});		
	},

	execMakeActive:function(e){
		var th = e.currentTarget;
		var url = $(th).attr('href');

		$.ajax({
			url: url,
			type: "post",
			//data: $('#my-awesome-dropzone').serialize(),
			//dataType: "html",
			success: function(response){
				$.modal.close();
				ImageUpload.loadImage();
			}
		});		
	}	

	

};

$(document).ready(function(){
	ImageUpload.loadImage();
	ImageUpload.bindDeleteImageEvent();
	ImageUpload.bindActiveImageEvent();

	ImageUpload.registerDeleteEventHandler();
	ImageUpload.registerActiveImageEventHandler();

	Dropzone.options.myAwesomeDropzone = {
		init: function(){
			var th = this;
			this.on('queuecomplete', function(){
				ImageUpload.loadImage();
				setTimeout(function(){
					th.removeAllFiles();
				},5000);
			})
		},
	  	paramName: "file", // The name that will be used to transfer the file
	  	maxFilesize: 2, // MB
	  	// accept: function(file, done) {
	   //  	if (file.name == "justinbieber.jpg") {
	   //    		done("Naha, you don't.");
	   //  	}
	   //  	else { done(); }
	  	// }
	  	acceptedFiles: 'image/*',
	  	
	};
});

