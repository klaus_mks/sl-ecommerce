var ChangeStatus = {
    bindChangeStatusEvent:function(){
        $(document).on('change', 'select#changeStatus', function(e){
            ChangeStatus.confirmChangeStatus(e);
        });

        $('body').on('click', '#modalChangeStatusConfirm li.btn-process a', function(e){
            $(this).trigger('change_status');
            e.preventDefault();
        });

        $(document).on('click', 'a.change-status', function(e){
            ChangeStatus.confirmChangeStatus(e);
        });
    },

    confirmChangeStatus: function(e){
        // in case of changing status when in the list page
        var id = $(e.currentTarget).parents('td').attr('data-id');
        var new_status = $(e.currentTarget).val();

        // in case of changing status when in the detail page
        if(id == undefined || new_status == undefined)
        {
            id = $('#order-id').val();
            new_status = $(e.currentTarget).attr('new-status');            
        }

        var url = '/order/update/' + id;
        $('#modalChangeStatusConfirm a.process-link').attr('href', url);
        $('#modalChangeStatusConfirm a.process-link').attr('new_status', new_status);
        $("#modalChangeStatusConfirm").modal({
            escClose: true,
            overlayClose: true,
            autoResize: true,       
        });
        e.preventDefault();
    },

    // register event hander for event 'make-active'
    changeStatusEventHandler: function(){
        $('body').on('change_status', '#modalChangeStatusConfirm li.btn-process a', function(e){
            ChangeStatus.execChangeStatus(e);            
        });
    },

    execChangeStatus:function(e){
        var th = e.currentTarget;
        var url = $(th).attr('href');
        var new_status = $(th).attr('new_status');
        $.ajax({
            url: url,
            type: "post",
             data: {"Order[status]": new_status},
            //dataType: "html",
            success: function(response){
                if($('#data-grid').length)
                {
                    $.modal.close();
                    jQuery('#data-grid').yiiGridView('update');    
                }
                else
                    window.location.reload();
                
            }
        });     
    },

    execApproval: function(e){

    }   
};

$(document).ready(function(){
    ChangeStatus.bindChangeStatusEvent();
    ChangeStatus.changeStatusEventHandler();
});

