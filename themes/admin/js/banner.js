$('body').on('loadImageComplete', function(){
	$("#image-list .caption .tz").tooltip();
	//console.log( $("#image-list .caption .update-banner") )
	$("#image-list .caption .update-banner").popover({
		title: 'update banner',
		//content: $("#image-list .caption .update-banner").next().html(),
		content: $('.pop-content').html(),
		html: true
		//trigger: 'focus'
	});

	//$('.frm-edit-banner').submit(function(e){
	$(document).on('submit', '.frm-edit-banner', function(e){
		var form = $(this);
		$.ajax({
			url: form.attr('action'),
			type: "post",
			data: form.serialize(),
			dataType: "json",
			success: function(response){
				if(response.success)
				{
					ImageUpload.loadImage();
					Common.showAlert('.global-alert', '.update-success');
				}
			}
		});
		e.preventDefault();
	});

	$("#image-list .caption .update-banner").on('shown.bs.popover', function(){
		$('.popover .popover-content form').append('<input type="hidden" name="id" value="' + $(this).attr('banner-id') + '" />');
		$('.popover .popover-content form input[name="url"]').val($(this).attr('banner-url'));
		
		var wrapper = $('<div class="form-group"></div>');
		var positionSelect = $('.banner-position').clone();
		positionSelect.val($(this).attr('banner-position'));
		positionSelect.addClass('form-control');
		positionSelect.show();

		wrapper.append(positionSelect);
		wrapper.insertBefore($('.popover .popover-content form button.btn-update-banner'));
	});
});
