var Admin = {

	confirmDelete: function(e){
		var url = $(e.currentTarget).attr('href');
		$('#modalDeleteConfirm a.process-link').attr('href', url);
		$("#modalDeleteConfirm").modal({
			escClose: true,
			overlayClose: true,
			autoResize: true,		
			// onOpen: function(dialog){
			// 	dialog.overlay.fadeIn(function(){
			// 		dialog.container.show();
			// 		dialog.data.show();	
			// 	});
			// },
			// onClose: function(dialog){
			// 	dialog.data.fadeOut(function(){
			// 		dialog.overlay.hide();	
			// 		dialog.container.fadeOut();
			// 	});	
			// }


		});
		e.preventDefault();
	},

	confirmDialog: function(e, dialogId){
		var url = $(e.currentTarget).attr('href');
		$(dialogId + ' a.process-link').attr('href', url);
		$(dialogId).modal({
			escClose: true,
			overlayClose: true,
			autoResize: true,		
		});
		e.preventDefault();
	},


	registerCancelEvent:function(){
		$('body').on('click', '#modalDeleteConfirm li.btn-cancel a, #modalConfirmDialog li.btn-cancel a', function(e){
			$.modal.close();
			e.preventDefault();
		});
	},
	registerDeleteEvent:function(){		
		$('body').on('click', '#modalDeleteConfirm li.btn-process a', function(e){
			$(this).trigger('delete');
			e.preventDefault();
		});
	},

	registerDeleteEventHandler: function(){
		$(document).on('delete', '#modalDeleteConfirm li.btn-process a', function(e){
			Admin.execDelete(e);			
		});
	},
	execDelete: function(e){
		var th = e.currentTarget,
        afterDelete = function(){};
	    jQuery('#data-grid').yiiGridView('update', {
	        type: 'POST',
	        url: jQuery(th).attr('href'),
	        success: function(data) {
	            jQuery('#data-grid').yiiGridView('update');
	            afterDelete(th, true, data);
	            $.modal.close();
	            Common.showAlert('.global-alert', '.delete-success');
	        },
	        error: function(XHR) {
	            return afterDelete(th, false, XHR);
	        }
	    });
	    return false;

	}
};

var Common = {
	showAlert: function( containerClass, dataClass ){
		$(containerClass + ', ' + dataClass).fadeIn();
		setTimeout(function(){
			$(containerClass + ', ' + dataClass).fadeOut();
		}, 3000 );
	}
};

var Product = {
	addAttribute: function(){
		$('.add-attr').click(function(e){
			var newRow = $('.attr-group').first().clone();
			newRow.find('input[type="text"]').val("");
			newRow.insertAfter($('.attr-group').last());
			e.preventDefault();

		});
		
	},
	deleteAttribute: function(){
		//$('.delete-attr').click(function(e){
		$('body').on('click', '.delete-attr', function(e){
			$(this).closest('.attr-group').remove();
			e.preventDefault();

		});
	}
};


$(document).ready(function(){
	Admin.registerCancelEvent();
	Admin.registerDeleteEvent();
	Admin.registerDeleteEventHandler();

	Product.addAttribute();
	Product.deleteAttribute();
});	