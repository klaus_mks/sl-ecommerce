<?php

class CartHelper
{
	/**
	*
	* the $cartItem is as follow:
	* array(
	*	'id' => $product_id,
	*	'name' => 'product name',
	*	'price' => $product_price,
	*	'quantity' => $quantity,
	*	'attribute' => array( 1, 2 , 3 ),	// attribute id
	*	'attribute_value' => array(2, 3, 4 ) 	// product attribute id
	* )
	*
	* the $cartData is as follow:
	* array(
	*	array(
	* 	 	'id' => $product_id,
	* 	 	'name' => 'product name',
	* 	 	'price' => $product_price,
	*		'quantity' => $quantity,
	* 	 	'attribute' => array( 1, 2 , 3 ),	// attribute id
	* 	 	'attribute_value' => array(2, 3, 4 ) 	// product attribute id
	*	),
	*	array(
	* 	 	'id' => $product_id,
	* 	 	'name' => 'product name',
	* 	 	'price' => $product_price,
	*		'quantity' => $quantity,
	* 	 	'attribute' => array( 1, 2 , 3 ),	// attribute id
	* 	 	'attribute_value' => array(2, 3, 4 ) 	// product attribute id
	*	),
	*	..........
	*	
	* )
	*
	* @author 	Klaus
	* @since 	2014 - 07 - 25
	* @param 	array $cartItem
	* @param    array $cartData
	* @return   boolean
	*
	*/
	public static function isInCart( $cartItem, $cartData )
	{
		if( !is_array($cartItem) || !is_array( $cartData))
			return false;
		foreach( $cartData as $c )
		{
			// compare product id
			if(self::compareItem( $cartItem, $c ))
				return true;
		}

		return false;
	}

	public static function compareItem( $item1, $item2 )
	{

		if($item1['id'] == $item2['id'])
		{
			if(empty($item1['attribute']) || empty($item2['attribute']))
				return true;

			$cAttr = count($item1['attribute']);
			$cAttrVal = count($item1['attribute_value']);

			$attrIntersect = array_intersect( $item1['attribute'], $item2['attribute'] );
			$attrValIntersect = array_intersect( $item1['attribute_value'], $item2['attribute_value'] );

			if( count($attrIntersect) == $cAttr && count($attrValIntersect) == $cAttrVal)
				return true;

		}
		return false;
	}

	// public static function getProductQuantity( $productId, $cartData )
	// {
	// 	if(empty($cartData))
	// 		return 0;
	// 	foreach( $cartData as $c )
	// 		if($productId == $c['id'])
	// 			return $c['quantity'];
	// 	return 0;	
	// }

	public static function getProductAttributeIdList( $cartData )
	{
		if(empty($cartData))
			return NULL;
		$res = array();
		foreach( $cartData as $c )
			if(!empty($c['attribute_value']))
				$res = array_merge($res, $c['attribute_value']);
		return array_unique($res);	
	}

	public static function getAttributeIdList( $cartData )
	{
		if(empty($cartData))
			return NULL;
		$res = array();
		foreach( $cartData as $c )
			if(!empty($c['attribute']))
				$res = array_merge($res, $c['attribute']);
		return $res;	
	}

	public static function getCartIdList( $cartData)
	{
		if(empty($cartData))
			return NULL;
		$res = array();
		foreach( $cartData as $c )
			$res[] = $c['id'];
		return $res;

	}

	public static function getItemIndex( $item, $cartData )
	{
		if(empty($cartData))
			return NULL;
		foreach( $cartData as $index => $c )
			if(self::compareItem($item, $c))
				return $index;
		return NULL;
	}


	// public static function addOneItem( $cartItem )
	// {
	// 	$cartData = Yii::app()->session['cart'];
	// 	foreach( $cartData as &$c )
	// 		//if(self::isInCart( $cartItem, $cartData ))
	// 		if(self::compareItem($cartItem, $c))
	// 			$c['quantity']++;

	// 	Yii::app()->session['cart'] = $cartData;
	// }

	public static function changeQuantity( $newQuantity, &$cartData )
	{
		if(!is_array( $newQuantity) || !is_array( $cartData ))
			return false;
		foreach( $cartData as $i => &$c )
			$c['quantity'] = $newQuantity[$i];
		return true;
	}

	public static function getTotalCartItem( $cart)
	{
		if(empty($cart))
			return 0;
		$c = 0;
		foreach( $cart as $item )
			$c += 1;
		return $c;
	}
}