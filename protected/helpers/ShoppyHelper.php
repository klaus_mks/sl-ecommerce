<?php 

class ShoppyHelper
{
    public $html = NULL;
	/**
    * @author 	Klaus
    * @since 	2014 - 07 - 03
    */
	public static function getNoImage($width = 0, $height = 0)
	{
		return '/'. Yii::app()->params['general']['thumb_filename']. "?src=/images/nocover.png&w=$width&h=$height";
	}
    
	/**
    * @author 	Klaus
    * @since 	2014 - 07 - 03
    */
	public static function getThumbnailPath($url, $width = 0, $height = 0, $options = array())
	{
        $link = '/'. Yii::app()->params['general']['thumb_filename']. "?src=$url&w=$width&h=$height"; 
        if(!empty($options))
            foreach( $options as $key => $val )
                $link .= "&$key=$val";
		return $link;
	}

	/**
    * @author 
    * @since 	2014 - 06 - 27
    */
    public static function getSlug($string) 
    {

        //remove any '-' from the string they will be used as concatonater
		$str = str_replace('-', ' ', $string);
		$str = self::removeAccent( $string );

		// remove any duplicate white, and ensure all characters are alphanumeric
		$str = preg_replace( array('/\s+/', '/[^A-Za-z0-9\-]/'), array('-', ''), $str);
		
		// lowercase and trim
		$str = trim(strtolower($str));
		return $str;
	}

	/**
    * @author 	Klaus
    * @since 	2014 - 07 - 26
    */
    public static function getRecordById($id, $objList)
    {
    	foreach( $objList as $obj )
    		if($id == $obj->id)
				return $obj;
		return NULL;
    }

    public static function wordLimit($str, $limit = 100, $end_char = '&#8230;') 
    {
	  	if (trim($str) == '') return $str;

		// always strip tags for text
	  	$str = strip_tags($str);

	  	$find = array("/\r|\n/","/\t/","/\s\s+/");
	  	$replace = array(" "," "," ");
	  	$str = preg_replace($find,$replace,$str);

	  	preg_match('/\s*(?:\S*\s*){'.(int) $limit.'}/', $str, $matches);

	  	if (strlen($matches[0]) == strlen($str))
	      	$end_char = '';
	  	return rtrim($matches[0]).$end_char;
	}

	public static function removeAccent( $str )
    {
    	$trans = array(
            "đ" => "d", "ă" => "a", "â" => "a", "á" => "a", "à" => "a", "ả" => "a", "ã" => "a", "ạ" => "a",
            "ấ" => "a", "ầ" => "a", "ẩ" => "a", "ẫ" => "a", "ậ" => "a", "ắ" => "a", "ằ" => "a", "ẳ" => "a",
            "ẵ" => "a", "ặ" => "a", "é" => "e", "è" => "e", "ẻ" => "e", "ẽ" => "e", "ẹ" => "e", "ế" => "e",
            "ề" => "e", "ể" => "e", "ễ" => "e", "ệ" => "e", "í" => "i", "ì" => "i", "ỉ" => "i", "ĩ" => "i",
            "ị" => "i", "ư" => "u", "ô" => "o", "ơ" => "o", "ê" => "e", "Ư" => "u", "Ô" => "o", "Ơ" => "o",
            "Ê" => "e", "ú" => "u", "ù" => "u", "ủ" => "u", "ũ" => "u", "ụ" => "u", "ứ" => "u", "ừ" => "u",
            "ử" => "u", "ữ" => "u", "ự" => "u", "ó" => "o", "ò" => "o", "ỏ" => "o", "õ" => "o", "ọ" => "o",
            "ớ" => "o", "ờ" => "o", "ở" => "o", "ỡ" => "o", "ợ" => "o", "ố" => "o", "ồ" => "o", "ổ" => "o",
            "ỗ" => "o", "ộ" => "o", "ú" => "u", "ù" => "u", "ủ" => "u", "ũ" => "u", "ụ" => "u", "ứ" => "u",
            "ừ" => "u", "ử" => "u", "ữ" => "u", "ự" => "u", 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y',
            'ỵ' => 'y', 'Ý' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y', "Đ" => "D", "Ă" => "A",
            "Â" => "A", "Á" => "A", "À" => "A", "Ả" => "A", "Ã" => "A", "Ạ" => "A", "Ấ" => "A", "Ầ" => "A",
            "Ẩ" => "A", "Ẫ" => "A", "Ậ" => "A", "Ắ" => "A", "Ằ" => "A", "Ẳ" => "A", "Ẵ" => "A", "Ặ" => "A",
            "É" => "E", "È" => "E", "Ẻ" => "E", "Ẽ" => "E", "Ẹ" => "E", "Ế" => "E", "Ề" => "E", "Ể" => "E",
            "Ễ" => "E", "Ệ" => "E", "Í" => "I", "Ì" => "I", "Ỉ" => "I", "Ĩ" => "I", "Ị" => "I", "Ư" => "U",
            "Ô" => "O", "Ơ" => "O", "Ê" => "E", "Ư" => "U", "Ô" => "O", "Ơ" => "O", "Ê" => "E", "Ú" => "U",
            "Ù" => "U", "Ủ" => "U", "Ũ" => "U", "Ụ" => "U", "Ứ" => "U", "Ừ" => "U", "Ử" => "U", "Ữ" => "U",
            "Ự" => "U", "Ó" => "O", "Ò" => "O", "Ỏ" => "O", "Õ" => "O", "Ọ" => "O", "Ớ" => "O", "Ờ" => "O",
            "Ở" => "O", "Ỡ" => "O", "Ợ" => "O", "Ố" => "O", "Ồ" => "O", "Ổ" => "O", "Ỗ" => "O", "Ộ" => "O",
            "Ú" => "U", "Ù" => "U", "Ủ" => "U", "Ũ" => "U", "Ụ" => "U", "Ứ" => "U", "Ừ" => "U", "Ử" => "U",
            "Ữ" => "U", "Ự" => "U",
            );

		return strtr($str, $trans);

    }

    public static function getCheckboxChecked($checkboxValue, $arr)
    {
    	if(empty($checkboxValue) || empty($arr))
    		return false;

    	return in_array($checkboxValue, $arr) ? 'checked="checked"' : NULL;
    }

    public static function renderMenu($menuTree){
        $html = NULL;
        if(!empty( $menuTree['sub_items'])) {
            $html = '<ul class="colour-list child">';
            foreach( $menuTree['sub_items'] as $ssc ){
            $html .= '<li>
                    <a href="'.$ssc['url'].'">'.$ssc['name'].'</a>
                    </li>';
            $html .= self::renderMenu($ssc);
            }
            $html .= '</ul>';
        }
        return $html;
    }
    

}