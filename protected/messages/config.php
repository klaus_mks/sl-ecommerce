<?php
/**
 * This is the configuration for generating message translations
 * for the Yii requirement checker. It is used by the 'yiic message' command.
 * Generate the message file:   
 * in protected foler, run command: ./yiic message messages/config.php
 * translation category:
 * - front: text messages in frontend
 * - validation: validation messages ( in models ... )
 * - notice: notifications to end users ( save successfully ... )
 */
return array(
	'sourcePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..'. DIRECTORY_SEPARATOR. '..',
	'messagePath'=>dirname(__FILE__),
	'languages'=>array('vi'),
	'fileTypes'=>array('php'),
	'overwrite'=>true,
	'removeOld' => true,
	'translator'=>'t',
	'exclude'=>array(
		'.gitignore',
		'requirements',
		'/protected/messages',
		'/protected/tests',
		'/protected/vendor/neam/',
		'/protected/extensions/yii-mail',
		'/protected/extensions/yii-debug-toolbar',
		'/protected/extensions/giix-core',
		'/protected/extensions/giix-components',
		'/protected/modules/auth',
	),
);
