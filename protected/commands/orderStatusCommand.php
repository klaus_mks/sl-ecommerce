<?php
class orderStatusCommand extends CConsoleCommand {

	/**
	* @author 	Oanh Le
	* @author 	Klaus
	* @since 	2014 - 07 - 17
	* ./yiic orderStatus updateStatusPending
	*/
    public function actionUpdateStatusPending() {

        $dateFrom = $dateTo = strtotime(date('Y-m-d') . " -1 days");
        $dateTo += 86399;
        $dateFrom  = date('Y-m-d H:i:s', $dateFrom);
        $dateTo  = date('Y-m-d H:i:s', $dateTo);
        
        //get list order has status = STATUS_NEW, STATUS_APPROVED
        $listId = CHtml::listData(
            Order::model()->findAll(array(
                'select' => 'id',
                'condition' => '(status = '.Order::STATUS_NEW.' or status = '.Order::STATUS_APPROVED.') and order_date BETWEEN \''.$dateFrom .'\' and \''. $dateTo .'\''
            )), 
            'id', 'id'
        );

        if(!empty($listId)){
            //convert array id to string
            $ids = "('".implode("','",$listId)."')";
            //update status
            Order::model()->updateAll(
                array('status'=> Order::STATUS_PENDING),
                'id in'.$ids
            );
        }
    }
    
}
?>