<?php

class CategoryWidget extends CWidget
{
	// public $currentCategory;	// this variable is passed from controller -> layout -> this widget class
	// /**
	//  * @author 	Klaus
	//  * @since 	2014 - 07 - 06
	//  */
	// public function run()
	// {
	// 	$category = Category::model()->with('categories')->findAll('t.parent_id is null and t.is_published = 1');
	// 	$this->render('category', array('category' => $category, 'currentCategory' => $this->currentCategory));
	// }
	private static $menuTree = array();
	public $currentCategory;

	public static function getMenuTree() {
        if (empty(self::$menuTree)) {
            $rows = Category::model()->findAll('parent_id is NULL');
            foreach ($rows as $item) {
                self::$menuTree[] = self::getMenuItems($item);
            }
        }
        return self::$menuTree;
    }

    private static function getMenuItems($modelRow) {
 
        if (!$modelRow)
            return;
 
        if (isset($modelRow->categories)) {
            $chump = self::getMenuItems($modelRow->categories);
            if ($chump != null)
                $res = array('id' => $modelRow->id, 'name' => $modelRow->name, 'sub_items' => $chump, 'url' => Yii::app()->createUrl('list/listCategoryProduct', array('id' => $modelRow->id, 'title' => ShoppyHelper::getSlug( $modelRow->name)) ));
            else
                $res = array('id' => $modelRow->id, 'name' => $modelRow->name, 'url' => Yii::app()->createUrl('list/listCategoryProduct', array('id' => $modelRow->id, 'title' => ShoppyHelper::getSlug( $modelRow->name)) ));
            return $res;
        } else {
            if (is_array($modelRow)) {
                $arr = array();
                foreach ($modelRow as $leaves) {
                    $arr[] = self::getMenuItems($leaves);
                }
                return $arr;
            } else {
                return array('id' => $modelRow->id, 'name' => ($modelRow->name), 'url' => Yii::app()->createUrl('list/listCategoryProduct', array('id' => $modelRow->id, 'title' => ShoppyHelper::getSlug( $modelRow->name)) ));
            }
        }
    }

    public function run(){
    	$categoryBox = $this->getMenuTree();
    	$this->render('category', array('category' => $this->menuTree, 'currentCategory' => $this->currentCategory));
    }
}
