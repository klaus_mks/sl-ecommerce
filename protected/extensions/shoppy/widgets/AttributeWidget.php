<?php

class AttributeWidget extends CWidget
{
	public $category;
	public function run()
	{
		if(!$this->category)
			return false;

		$attribute = Yii::app()->db->createCommand()
		    ->select('a.name, pa.value')
		    ->from('product_attribute pa')
		    ->join('attribute a', 'pa.attribute_id = a.id')
		    ->join('product p', 'pa.product_id = p.id')
		    ->join('category c', 'p.category_id = c.id')
		    ->where('c.id = :categoryId', array(':categoryId' => $this->category->id ))
		    ->group('a.name, pa.value')
		    ->queryAll();
		
		if(empty($attribute))
			return false;

		$attr = array();
		foreach( $attribute as $a )
			if(!array_key_exists( $a['name'], $attr))
				$attr[$a['name']] = array( $a['value'] );
			else
				array_push( $attr[$a['name']] , $a['value']);
			

		//echo '<pre>'; var_dump( $attr );exit;
		$this->render('attribute', array('attribute' => $attr, 'category' => $this->category ));
	}
}