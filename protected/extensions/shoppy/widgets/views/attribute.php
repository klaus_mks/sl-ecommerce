<?php 
Yii::app()->clientScript->registerCss('attribute', '
    .attribute-list .attribute-item 
    {
        margin-top: 5px;        
    }

    .attribute-list .attribute-title
    {
        margin-top: 10px !important;
    }
');
?>

<div class="row">
    <div class="span3">
    	<?php
    		$url = Yii::app()->createUrl('list/listCategoryProduct', array('id' => $category->id, 'title' => $category->name ));
    	?>
        <form class="form-horizontal attribute-list" role="form" action="<?php echo $url; ?>" method="post">
            <?php foreach( $attribute as $name => $value ): ?>
            <h5 class="attribute-title"><?php echo $name; ?></h5>
            <hr>
            <?php foreach( $value as $k => $v ): ?>
            <div class="attribute-item control-group">
                <div class="controls">
                  <label class="checkbox">
                  <?php
                 	$checked = false;
                 	if( !empty(Yii::app()->session['attribute'][$name]))
                 	{
                 		//var_dump( $name );
                 		//var_dump('expression');exit;
                 		$checked = ShoppyHelper::getCheckboxChecked($v, Yii::app()->session['attribute'][$name]);
                 	}
                 	//var_dump( $checked );exit;
                  ?>
                    <input type="checkbox" class="attribute" 
                    	name="attribute[<?php echo $name; ?>][]" 
                    	value="<?php echo $v; ?>"
                    	<?php echo $checked; ?>
                    > 
                    	<?php echo $v; ?>
                  </label>
                </div>
            </div>
            <?php endforeach; ?>
            <?php endforeach; ?>
        </form>
    </div>
</div>