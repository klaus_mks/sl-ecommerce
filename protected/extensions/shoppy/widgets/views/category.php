<!-- this view is for left side menu -->
<div class="row">
    <input type="hidden" id="current-category" value="<?php echo !empty($currentCategory->id) ? $currentCategory->id : NULL; ?>" />
  <div class="span3">
    <h5>Category</h5>
    
    <hr>
    
    <dl class="accordion">

        <ul class="colour-list">
            <?php foreach( $category as $c ): ?>
            <li class="g-parent">
                <!-- level 1 -->
                <dt>
                    <a 
                        href="<?php echo $c['url'] ?>"
                        category-id="<?php echo $c['id']; ?>"
                        class="g-parent"
                    >
                        <i class="icon-pencil"></i><?php echo $c['name']; ?>
                    </a>
                </dt>
                <?php if(!empty( $c['sub_items'])): ?>
                <dd>
                    <ul class="colour-list">
                        <?php foreach( $c['sub_items'] as $sc ): ?>
                        <li>
                            <!-- level 2 -->
                            <dt>
                                <a 
                                    href="<?php echo $sc['url']; ?>"
                                    category-id="<?php echo $sc['id']; ?>"
                                >
                                    <?php echo $sc['name']; ?>
                                </a>
                            </dt>
                        </li>
                            
                            <?php if(!empty($sc['sub_items'])): ?>
                            <ul class="colour-list">
                                <?php foreach( $sc['sub_items'] as $ssc ): ?>
                                <li>
                                    <!-- level 3 -->
                                    <a 
                                        href="<?php echo $ssc['url'] ; ?>"
                                        category-id="<?php echo $ssc['id']; ?>"
                                    >
                                        <?php echo $ssc['name']; ?>
                                    </a>
                                    <?php if(!empty($sc['sub_items'])) echo ShoppyHelper::renderMenu($ssc);?>
                                </li>
                                
                                <?php endforeach; ?>
                            </ul>           

                            <?php endif; ?>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </dd>
                <?php endif; ?>
            </li>


            <?php endforeach; ?>
            
        </ul>

    </dl>

  </div>
  
</div>