<!-- left menu -->
<div class="sb-slidebar sb-left sb-style-overlay">
    <nav>
        <ul class="sb-menu">
            <li><div>Menu</div></li>
            <li><a href="<?php echo Yii::app()->createUrl('site/home'); ?>" class="current"><?php echo Yii::t('front', 'Home'); ?></a></li>
            <li><a href="<?php echo Yii::app()->createUrl('list/listRandomCategory'); ?>" id="shop-submenu-trigger"><?php echo Yii::t('front', 'Products'); ?></a></li>
            <li><a href="<?php echo Yii::app()->createUrl('list/listPost'); ?>"><?php echo Yii::t('front', 'Articles'); ?></a></li>
            <li><a href="<?php echo Yii::app()->createUrl('site/howToBuy'); ?>"><?php echo Yii::t('front', 'How to buy'); ?></a></li>
            <li><a href="<?php echo Yii::app()->createUrl('product/viewCart'); ?>"><?php echo Yii::t('front', 'Shopping cart'); ?></a></li>
            <li><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>"><?php echo Yii::t('front', 'Contact'); ?></a></li>
        </ul>
    </nav>
</div>


<!-- right menu -->
<?php if(!empty($category)): ?>
<div class="sb-slidebar sb-right sb-style-overlay">
    <nav>
        <ul class="sb-menu">
            <li><div><?php echo Yii::t('front', 'Products'); ?></div></li>
            <?php foreach($category as $c ): ?>
            <li><a href="<?php echo Yii::app()->createUrl('list/listCategoryProduct', array('id' => $c->id, 'title' => ShoppyHelper::getSlug( $c->name)) ); ?>"><?php echo $c->name; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </nav>
</div>
<?php endif; ?>