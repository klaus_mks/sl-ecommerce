<ul class="main-menu clearfix rr" id="main-menu">
  <li><a href="<?php echo Yii::app()->createUrl('site/home'); ?>" class="current"><?php echo Yii::t('front', 'Home'); ?></a></li>
  <li id="shop-submenu-area">
    <a href="<?php echo Yii::app()->createUrl('list/listRandomCategory'); ?>" id="shop-submenu-trigger"><?php echo Yii::t('front', 'Products'); ?></a>
  	<ul class="row shop-submenu rr sub-menu" style="display: none;">
  		<li class="arrow ir">Arrow</li>
        <?php foreach ($menuTree as $parent) : ?>
        <li class="span2 beta">
            <ul class="rr sub-menu">
                <li>
                    <span class="ir icon">Icon</span>
                    <span class="category">
                      <a href="<?php echo Yii::app()->createUrl('list/listCategoryProduct', array('id' => $parent['id'], 'title' => ShoppyHelper::getSlug($parent['label']))) ; ?>">
                        <?php echo $parent['label']; ?>
                      </a>
                      
                    </span>
                </li>
                <?php  if(!empty($parent['sub_items'])) :
                    foreach ($parent['sub_items'] as $sub) : ?>
                        <li>
                            <a href="#"><?php echo $sub['label'];?></a>
                        </li>
                        <?php  if(!empty($sub['sub_items'])) :
                            foreach ($sub['sub_items'] as $sub) : ?>
                                <li>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo $sub['url'];?>"><?php echo $sub['label'];?></a>
                                </li>
                        <?php endforeach; 
                        endif; ?>
                <?php endforeach; 
                endif; ?>
            </ul>
        </li>
        <?php endforeach; ?>
  	</ul>
  </li>
  <li><a href="<?php echo Yii::app()->createUrl('list/listPost'); ?>"><?php echo Yii::t('front', 'Articles'); ?></a></li>
  <li><a href="<?php echo Yii::app()->createUrl('site/howToBuy'); ?>"><?php echo Yii::t('front', 'How to buy'); ?></a></li>
  <li><a href="<?php echo Yii::app()->createUrl('product/viewCart'); ?>"><?php echo Yii::t('front', 'Shopping cart'); ?></a></li>
  <li><a href="<?php echo Yii::app()->createUrl('site/contact'); ?>"><?php echo Yii::t('front', 'Contact'); ?></a></li>
</ul>
