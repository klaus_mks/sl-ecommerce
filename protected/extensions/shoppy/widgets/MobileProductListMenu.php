<?php

class MobileProductListMenu extends CWidget
{
	public $category = array();

    /**
     * @author   Klaus
     * @since    2014 - 10 - 18
     */
	public function getMenuTree() 
    {
        if(empty(Yii::app()->session['category-list']))
        {
            $this->category = Category::model()->findAll('parent_id is NULL');   
            Yii::app()->session['category-list'] = $this->category;
        }
        else
            $this->category = Yii::app()->session['category-list'];
    }

    /**
     * @author   Klaus
     * @since    2014 - 10 - 18
     */
    public function run(){
    	$this->getMenuTree();
    	$this->render('mobileProductListMenu', array('category' => $this->category));
    }
}
