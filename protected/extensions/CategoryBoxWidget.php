<?php 

/*
* this widget is used for the hover category box 
* on the main menu
*/
class CategoryBoxWidget  extends CWidget {
	private static $menuTree = array();

	public static function categoryBoxWidget() {
        if (empty(self::$menuTree)) {
            $rows = Category::model()->findAll('parent_id = true');
            foreach ($rows as $item) {
                self::$menuTree[] = self::getMenuItems($item);
            }
        }
        return self::$menuTree;
    }

    private static function getMenuItems($modelRow) {
 
        if (!$modelRow)
            return;
 
        if (isset($modelRow->Childs)) {
            $chump = self::getMenuItems($modelRow->Childs);
            if ($chump != null)
                $res = array('label' => $modelRow->title, 'items' => $chump, 'url' => Yii::app()->createUrl('yourcontroller/youraction', array('id' => $modelRow->id)));
            else
                $res = array('label' => $modelRow->title, 'url' => Yii::app()->createUrl('yourcontroller/youraction', array('id' => $modelRow->id)));
            return $res;
        } else {
            if (is_array($modelRow)) {
                $arr = array();
                foreach ($modelRow as $leaves) {
                    $arr[] = self::getMenuItems($leaves);
                }
                return $arr;
            } else {
                return array('label' => ($modelRow->title), 'url' => Yii::app()->createUrl('yourcontroller/youraction', array('id' => $modelRow->id)));
            }
        }
    }
    public function run(){
	    $this->widget('system.web.widgets.CategoryBoxWidget', array(
	      'tabs' => $this->data,
	      'htmlOptions' => $this->htmlOptions,
	      'activeTabs' => $this->activeTabs
	    ));
	  }
}
?>