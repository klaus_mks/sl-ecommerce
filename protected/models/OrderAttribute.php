<?php

Yii::import('application.models._base.BaseOrderAttribute');

class OrderAttribute extends BaseOrderAttribute
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function getAttributeById($order_product_id){
		$att = Yii::app()->db->createCommand()
		   	->select('a.name, pa.value')
		   	->leftJoin('product_attribute pa', 'pa.id=oa.product_attribute_id')
		   	->leftJoin('attribute a', 'a.id=pa.attribute_id')
		   
		   	->from('order_attribute oa')
		   	->where('oa.order_product_id=:order_product_id', array(':order_product_id'=>$order_product_id))
		   	->queryAll();
		$html = '';
		if(!empty($att)){
			foreach ($att as $val)
				$html .= '<br>'.$val['name'].':'.$val['value'];
		}
		return $html;
	}
}