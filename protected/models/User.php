<?php

Yii::import('application.models._base.BaseUser');

class User extends BaseUser
{
	const MAIL_SUBJECT_REGISTER = 'Tài khoản của bạn vừa được tạo';
	const MAIL_SUBJECT_GOT_NEW_PASSWORD = "Mật khuẩn mới của bạn vừa được tạo";

	public $repeat_password;
	public $old_password;
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules()
	{
	    return array(
	    	array('email', 'required', 'on' => 'insert', 'message'=> 'Vui lòng nhập địa chỉ email'),
	    	array('email', 'email', 'message'=> 'Vui lòng nhập đúng địa chỉ email'),
	    	array('email', 'checkExistence', 'on' => 'insert', 'message'=> 'Tài khoản này đã được sử dụng'),
	    	array('email', 'checkForgotPassword', 'on' => 'forgotPassword', 'message'=> 'Email không tồn tại trong hệ thống'),
	    	
	    	array('password', 'required', 'on'=>'insert, updatePassword', 'message' => 'Vui lòng nhập password'),
	    	array('repeat_password', 'required', 'on'=>'insert, updatePassword', 'message' => 'Vui lòng nhập lại password'),
	    	array('old_password', 'required', 'on'=>'updatePassword', 'message' => 'Vui lòng nhập password'),
	    	array('old_password', 'checkOldPassword', 'on'=>'updatePassword'),

            array('password', 'length', 'min' => 5, 'tooShort'=> 'Password ít nhất 5 kí tự'),
            array('repeat_password', 'length', 'min' => 5, 'tooShort'=> 'Password ít nhất 5 kí tự'),
            array('old_password', 'length', 'min' => 5, 'tooShort'=> 'Password ít nhất 5 kí tự'),

            array('repeat_password', 'compare', 'on' => 'insert, updatePassword', 'compareAttribute'=>'password', 'message'=> 'Password nhập lại không khớp'),
	        array('phone', 'required', 'on' => 'insert, updateInfo', 'message'=> 'Vui lòng nhập số điện thoại'),
	        array('last_name', 'required', 'on' => 'insert, updateInfo', 'message'=> 'Vui lòng nhập họ'),
	        array('first_name', 'required', 'on' => 'insert, updateInfo', 'message'=> 'Vui lòng nhập tên'),
	        array('birthday', 'required', 'on' => 'insert, updateInfo', 'message'=> 'Vui lòng nhập ngày sinh'),
	        array('birthday', 'date', 'on' => 'insert, updateInfo', 'format'=>'dd-MM-yyyy', 'message'=> 'Vui lòng nhập đúng ngày tháng năm (dd-MM-yyyy)'),
	   	);
	}

	public function checkForgotPassword($attribute, $params )
	{
		$user = User::model()->find('email=\''. $this->email. '\'');
		if(empty($user))
			$this->addError($attribute, 'Email không tồn tại');	
	}

	public function checkExistence($attribute, $params)
	{
		$user = User::model()->find('email=\''. $this->email. '\'');
		if(!empty($user))
			$this->addError($attribute, 'Tài khoản này đã được sử dụng');
	}

	public function checkOldPassword($attribute,$params)
	{
		$user = User::model()->findByPk($this->id);
		if($user->password !== crypt($this->old_password, $user->password))
			$this->addError($attribute, 'Password cũ không đúng');
	}

	public function generateNewPassword()
	{

		$this->scenario = 'forgotPassword';		
		$newPassword = substr(md5(time()), 0, 6);
		$this->password = $newPassword;
		if($this->save())
			return $newPassword;
		return false;
	}

	

	public static  function generateSalt()
	{
		// For Blowfish hashing, the format is: 
		// "$2a$", a two digit cost parameter, "$", and 22 digits from the alphabet "./0-9A-Za-z"
		return '$2a$10$'. sha1(time());
	}
	public function beforeValidate()
	{
		if( empty( $this->created_by) )
			$this->created_by = 1;
		if( empty( $this->created_at) )
			$this->created_at = new CDbExpression('NOW()');

		return parent::beforeValidate();
	}
	
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Verification Code',
		);
	}
	public function beforeSave() 
	{
	    if ($this->isNewRecord)
	    {
	    	$this->created_by = Yii::app()->user->id ? Yii::app()->user->id : 0;
	        $this->created_at = new CDbExpression('NOW()');
	    }

	    $newDate = DateTime::createFromFormat('d-m-Y', $this->birthday);
	    $this->birthday = $newDate->format('Y-m-d H:i:s');

	    if(in_array($this->scenario, array('insert', 'updatePassword', 'forgotPassword')))
	    {
	    	$this->salt = User::generateSalt();
	    	$this->password =  crypt($this->password, User::generateSalt() );	
	    }
	    
	 	$this->modified_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	    $this->modified_at = new CDbExpression('NOW()');
	 
	    return parent::beforeSave();
	}

	public function afterFind()
	{
		if(!empty( $this->birthday))
		{
			$newDate = DateTime::createFromFormat('Y-m-d H:i:s', $this->birthday);
	    	$this->birthday = $newDate->format('d-m-Y');	
		}
	    return parent::afterFind();
	}

	public function updatePassword($data)
	{
		// check old password
		$this->scenario = 'updatePassword';
		$this->setAttributes($data['User']);
		return $this->save();
	}

	public function generateTestUser()
	{
		for( $i = 0; $i < 5; $i ++ )
		{
			$salt = sha1(time());
			$salt = '$2a$10$'. $salt;
			
			$user = new User();
			$user->first_name = 'First name' . ( $i + 1 );
			$user->last_name = 'Last name' . ( $i + 1 );
			$user->email = 'user'. ($i +1 ). '@mail.com';
			$user->salt = $salt;
			$user->password = crypt( '12345', $salt );
			$user->save();
			sleep(1);
		}
	}

	public function assignPermission()
	{
		$auth=Yii::app()->authManager;
  		$admin = $auth->createRole('admin');
  		$auth->assign('admin', 'admin@mail.com');
		$shop = $auth->createRole('shop');
		$auth->assign('shop', 'shop1@mail.com');
		$auth->assign('shop', 'shop2@mail.com');

		$auth->createOperation('createPost','create a post');
		$auth->createOperation('readPost','read a post');
		$auth->createOperation('updatePost','update a post');
		$auth->createOperation('deletePost','delete a post');
		 
		$bizRule='return Yii::app()->user->id==$params["post"]->authID;';
		$task=$auth->createTask('updateOwnPost','update a post by author himself',$bizRule);
		$task->addChild('updatePost');
		 
		$role=$auth->createRole('reader');
		$role->addChild('readPost');
		 
		$role=$auth->createRole('author');
		$role->addChild('reader');
		$role->addChild('createPost');
		$role->addChild('updateOwnPost');
		 
		$role=$auth->createRole('editor');
		$role->addChild('reader');
		$role->addChild('updatePost');
		 
		$role=$auth->createRole('admin');
		$role->addChild('editor');
		$role->addChild('author');
		$role->addChild('deletePost');
		 
		$auth->assign('reader','readerA');
		$auth->assign('author','authorB');
		$auth->assign('editor','editorC');
		$auth->assign('admin','adminD');

	}
}