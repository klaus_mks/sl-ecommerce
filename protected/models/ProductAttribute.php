<?php

Yii::import('application.models._base.BaseProductAttribute');

class ProductAttribute extends BaseProductAttribute
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


	public function beforeValidate()
	{
		if( empty( $this->created_by) )
			$this->created_by = 1;
		if( empty( $this->created_at) )
			$this->created_at = new CDbExpression('NOW()');

		return parent::beforeValidate();
	}

	public function beforeSave() 
	{
	    if ($this->isNewRecord)
	    {
	    	$this->created_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	        $this->created_at = new CDbExpression('NOW()');
	    }
	 
	 	$this->modified_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	    $this->modified_at = new CDbExpression('NOW()');
	 
	    return parent::beforeSave();
	}

	public function getInfoAttribute($product_id, $attribute_id){
		return Yii::app()->db->createCommand()
		   	->select('pa.value')
		   	->from(' product_attribute pa')
		   	->where('pa.product_id=:product_id and  pa.attribute_id=:attribute_id', array(':product_id'=>$product_id, ':attribute_id'=>$attribute_id))
		   	->queryAll();
	}
	public function getAttributeById($order_product_id){
		return Yii::app()->db->createCommand()
		   	->select('pa.product_attribute_id')
		   	->from('product_attribute pa')
		   	->where('pa.order_product_id=:order_product_id', array(':order_product_id'=>$order_product_id))
		   	->queryAll();
	}
}