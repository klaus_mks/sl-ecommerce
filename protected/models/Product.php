<?php

Yii::import('application.models._base.BaseProduct');

class Product extends BaseProduct
{
	public static function model($className=__CLASS__) 
	{
		return parent::model($className);
	}

	public function behaviors()
	{
	    return array(
	        'i18n-columns' => array(
	             'class' => 'I18nColumnsBehavior',
	             /* The multilingual attributes */
	             'translationAttributes' => array(
	                  'name',
	                  'description'
	             ),
	        ),
	    );
	}

	public function beforeValidate()
	{
		if( empty( $this->created_by) )
			$this->created_by = 1;
		if( empty( $this->created_at) )
			$this->created_at = new CDbExpression('NOW()');

		return parent::beforeValidate();
	}
	
	public function beforeSave() 
	{
	    if ($this->isNewRecord)
	    {
	    	$this->created_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	        $this->created_at = new CDbExpression('NOW()');
	    }
	 
	 	$this->modified_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	    $this->modified_at = new CDbExpression('NOW()');
	 
	    return parent::beforeSave();
	}

	public function relations()
	{
		return array_merge(
			parent::relations(), 
			array(
				'attributeValues' => array(self::HAS_MANY, 'ProductAttribute', 'product_id'),
			)
		);
	}

	public function getAncestorsBreadCrumb( $route, $currentToLink = false )
	{
		if(!empty( $this->category ))
			return array_merge($this->category->getAncestorsBreadCrumb( $route, $currentToLink ), array($this->name));
		return array($this->name);		
	}



	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 03
	* @param 	ProductImage 	$images
	*/
	public function getActiveProductImage($images, $width = 0, $height = 0, $options = array())
	{
		if(!is_array($images))
			return false;
		$link = "";
		foreach( $images as $image )
			if( $image->is_actived )
				return ShoppyHelper::getThumbnailPath(Yii::app()->params['product']['image_path']. DIRECTORY_SEPARATOR. $image->name, 
					$width, $height, $options);
		return $link;
	}

	public function getDetails()
	{
		$str = '<b>'. $this->name. '</b>';
		if(is_array($this->attributeValues))
		{
			$attributes = Yii::app()->db->createCommand()
			    ->select('id, name')
			    ->from('attribute')
			   	->queryAll();
			$attributes = CHtml::listData($attributes, 'id', 'name');

			foreach( $this->attributeValues as $i => $a )
				$str .= '<br/>'. $attributes[$a->attribute_id] . ': '. $a->value ;
		}

		return $str;
	}

	public function shopSearch() 
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('shop_id', $this->shop_id);
		$criteria->compare('sku', $this->sku, true);
		$criteria->compare('size', $this->size, true);
		$criteria->compare('color', $this->color, true);
		$criteria->compare('t.name_en', $this->name_en, true);
		$criteria->compare('t.name_vi', $this->name_vi, true);
		$criteria->compare('price', $this->price);
		$criteria->compare('is_published', $this->is_published);
		$criteria->compare('is_new', $this->is_new);
		$criteria->compare('is_on_sale', $this->is_on_sale);
		$criteria->compare('is_promoted', $this->is_promoted);
		$criteria->compare('description_en', $this->description_en, true);
		$criteria->compare('description_vi', $this->description_vi, true);
		$criteria->compare('seo_description', $this->seo_description, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('modified_by', $this->modified_by);
		$criteria->compare('created_at', $this->created_at, true);
		$criteria->compare('modified_at', $this->modified_at, true);

		$cri = array('order' => 'modified_at DESC');

		if( !empty(Yii::app()->user->shop_id))
		{
			$cri = array_merge($cri, array(
				'condition' => 'shop_id = :shopID',
            	'params' => array(':shopID' => Yii::app()->user->shop_id)	
			));
		}

		$criteria->mergeWith($cri);
        
        return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function getGenderOptions(){
    	return  Category::model()->findAll(
                 array('select' => 'id,name','order' => 'name'));
	}

	public function generateTestData()
	{
		for( $i = 0; $i < 50; $i ++ )
		{
			
			$item = new Product();
			$item->name = 'Product Name' . ( $i + 1 );
			$item->shop_id = Yii::app()->user->shop_id;
			$item->description = 'description' . md5(rand());
			$item->seo_description = 'seo_description' . md5(rand());
			$item->created_by = Yii::app()->user->id;
			$item->created_at = date('Y-m-d H:i:s', time());
			$item->save();
		}
	}

	public function getAttributeProduct($id){
		return Yii::app()->db->createCommand()
		   	->select('a.name, pa.value')
		   	->from(' product p')
		   	->join('product_attribute pa', 'pa.product_id = p.id')
		   	->join('attribute a', 'pa.attribute_id = a.id')
		   	->where('p.id=:id', array(':id'=>$id))
		   	->queryAll();
	}
}