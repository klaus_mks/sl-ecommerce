<?php

/**
 * This is the model base class for the table "order_product".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "OrderProduct".
 *
 * Columns in table "order_product" available as properties of the model,
 * followed by relations of table "order_product" available as properties of the model.
 *
 * @property string $id
 * @property integer $order_id
 * @property string $product_id
 * @property integer $price
 * @property integer $quantity
 * @property integer $total_price
 * @property integer $created_by
 * @property integer $modified_by
 * @property string $created_at
 * @property string $modified_at
 *
 * @property OrderAttribute[] $orderAttributes
 * @property Product $product
 * @property Order $order
 */
abstract class BaseOrderProduct extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'order_product';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'OrderProduct|OrderProducts', $n);
	}

	public static function representingColumn() {
		return 'created_at';
	}

	public function rules() {
		return array(
			array('order_id, product_id, created_by, created_at', 'required'),
			array('order_id, price, quantity, total_price, created_by, modified_by', 'numerical', 'integerOnly'=>true),
			array('product_id', 'length', 'max'=>11),
			array('modified_at', 'safe'),
			array('price, quantity, total_price, modified_by, modified_at', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, order_id, product_id, price, quantity, total_price, created_by, modified_by, created_at, modified_at', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'orderAttributes' => array(self::HAS_MANY, 'OrderAttribute', 'order_product_id'),
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
			'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'order_id' => null,
			'product_id' => null,
			'price' => Yii::t('app', 'Price'),
			'quantity' => Yii::t('app', 'Quantity'),
			'total_price' => Yii::t('app', 'Total Price'),
			'created_by' => Yii::t('app', 'Created By'),
			'modified_by' => Yii::t('app', 'Modified By'),
			'created_at' => Yii::t('app', 'Created At'),
			'modified_at' => Yii::t('app', 'Modified At'),
			'orderAttributes' => null,
			'product' => null,
			'order' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('order_id', $this->order_id);
		$criteria->compare('product_id', $this->product_id);
		$criteria->compare('price', $this->price);
		$criteria->compare('quantity', $this->quantity);
		$criteria->compare('total_price', $this->total_price);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('modified_by', $this->modified_by);
		$criteria->compare('created_at', $this->created_at, true);
		$criteria->compare('modified_at', $this->modified_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}