<?php

/**
 * This is the model base class for the table "user".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "User".
 *
 * Columns in table "user" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property integer $group_id
 * @property integer $shop_id
 * @property string $email
 * @property string $phone
 * @property string $birthday
 * @property string $salt
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property integer $is_published
 * @property string $description
 * @property integer $created_by
 * @property integer $modified_by
 * @property string $created_at
 * @property string $modified_at
 *
 */
abstract class BaseUser extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'user';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'User|Users', $n);
	}

	public static function representingColumn() {
		return 'email';
	}

	public function rules() {
		return array(
			array('email', 'required'),
			array('group_id, shop_id, is_published, created_by, modified_by', 'numerical', 'integerOnly'=>true),
			array('email, password, first_name, last_name', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>20),
			array('salt', 'length', 'max'=>70),
			array('birthday, description, created_at, modified_at', 'safe'),
			array('group_id, shop_id, phone, birthday, salt, password, first_name, last_name, is_published, description, created_by, modified_by, created_at, modified_at', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, group_id, shop_id, email, phone, birthday, salt, password, first_name, last_name, is_published, description, created_by, modified_by, created_at, modified_at', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'group_id' => Yii::t('app', 'Group'),
			'shop_id' => Yii::t('app', 'Shop'),
			'email' => Yii::t('app', 'Email'),
			'phone' => Yii::t('app', 'Phone'),
			'birthday' => Yii::t('app', 'Birthday'),
			'salt' => Yii::t('app', 'Salt'),
			'password' => Yii::t('app', 'Password'),
			'first_name' => Yii::t('app', 'First Name'),
			'last_name' => Yii::t('app', 'Last Name'),
			'is_published' => Yii::t('app', 'Is Published'),
			'description' => Yii::t('app', 'Description'),
			'created_by' => Yii::t('app', 'Created By'),
			'modified_by' => Yii::t('app', 'Modified By'),
			'created_at' => Yii::t('app', 'Created At'),
			'modified_at' => Yii::t('app', 'Modified At'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('group_id', $this->group_id);
		$criteria->compare('shop_id', $this->shop_id);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('birthday', $this->birthday, true);
		$criteria->compare('salt', $this->salt, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('first_name', $this->first_name, true);
		$criteria->compare('last_name', $this->last_name, true);
		$criteria->compare('is_published', $this->is_published);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('modified_by', $this->modified_by);
		$criteria->compare('created_at', $this->created_at, true);
		$criteria->compare('modified_at', $this->modified_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}