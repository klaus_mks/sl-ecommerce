<?php

/**
 * This is the model base class for the table "category".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Category".
 *
 * Columns in table "category" available as properties of the model,
 * followed by relations of table "category" available as properties of the model.
 *
 * @property integer $id
 * @property string $name_en
 * @property string $name_vi
 * @property string $image
 * @property string $url
 * @property integer $parent_id
 * @property integer $is_published
 * @property integer $shop_id
 * @property integer $is_promoted
 * @property string $description_en
 * @property string $description_vi
 * @property string $seo_description
 * @property integer $created_by
 * @property integer $modified_by
 * @property string $created_at
 * @property string $modified_at
 *
 * @property Category $parent
 * @property Category[] $categories
 * @property Product[] $products
 */
abstract class BaseCategory extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'category';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Category|Categories', $n);
	}

	public static function representingColumn() {
		return 'name_en';
	}

	public function rules() {
		return array(
			array('name_en, name_vi, created_by, created_at', 'required'),
			array('parent_id, is_published, shop_id, is_promoted, created_by, modified_by', 'numerical', 'integerOnly'=>true),
			array('name_en, name_vi', 'length', 'max'=>255),
			array('image, url', 'length', 'max'=>200),
			array('seo_description', 'length', 'max'=>2048),
			array('description_en, description_vi, modified_at', 'safe'),
			array('image, url, parent_id, is_published, shop_id, is_promoted, description_en, description_vi, seo_description, modified_by, modified_at', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name_en, name_vi, image, url, parent_id, is_published, shop_id, is_promoted, description_en, description_vi, seo_description, created_by, modified_by, created_at, modified_at', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'parent' => array(self::BELONGS_TO, 'Category', 'parent_id'),
			'categories' => array(self::HAS_MANY, 'Category', 'parent_id'),
			'products' => array(self::HAS_MANY, 'Product', 'category_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'name_en' => Yii::t('app', 'Name En'),
			'name_vi' => Yii::t('app', 'Name Vi'),
			'image' => Yii::t('app', 'Image'),
			'url' => Yii::t('app', 'Url'),
			'parent_id' => null,
			'is_published' => Yii::t('app', 'Is Published'),
			'shop_id' => Yii::t('app', 'Shop'),
			'is_promoted' => Yii::t('app', 'Is Promoted'),
			'description_en' => Yii::t('app', 'Description En'),
			'description_vi' => Yii::t('app', 'Description Vi'),
			'seo_description' => Yii::t('app', 'Seo Description'),
			'created_by' => Yii::t('app', 'Created By'),
			'modified_by' => Yii::t('app', 'Modified By'),
			'created_at' => Yii::t('app', 'Created At'),
			'modified_at' => Yii::t('app', 'Modified At'),
			'parent' => null,
			'categories' => null,
			'products' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name_en', $this->name_en, true);
		$criteria->compare('name_vi', $this->name_vi, true);
		$criteria->compare('image', $this->image, true);
		$criteria->compare('url', $this->url, true);
		$criteria->compare('parent_id', $this->parent_id);
		$criteria->compare('is_published', $this->is_published);
		$criteria->compare('shop_id', $this->shop_id);
		$criteria->compare('is_promoted', $this->is_promoted);
		$criteria->compare('description_en', $this->description_en, true);
		$criteria->compare('description_vi', $this->description_vi, true);
		$criteria->compare('seo_description', $this->seo_description, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('modified_by', $this->modified_by);
		$criteria->compare('created_at', $this->created_at, true);
		$criteria->compare('modified_at', $this->modified_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}