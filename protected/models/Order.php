<?php

Yii::import('application.models._base.BaseOrder');

class Order extends BaseOrder
{
	const STATUS_NEW = 1;
	const STATUS_APPROVED = 2;
	const STATUS_DELIVERED = 3;
	const STATUS_PENDING = 4;

	const MAIL_SUBJECT_CHECKOUT = 'Cảm ơn bạn đã mua hàng';
	const MAIL_SUBJECT_APPROVED = 'Đơn hàng của bạn đã được duyệt';
	const MAIL_OWNER_SUBJECT_CHECKOUT = 'Hệ thống ghi nhận đơn hàng mới';	// notify to shop owner


	const PAYMENT_METHOD_CASH = 1;
	const PAYMENT_METHOD_TRANSFER = 2;
	
	public $statusText = array(
		self::STATUS_NEW => 'New',
		self::STATUS_APPROVED => 'Approved',
		self::STATUS_DELIVERED => 'Delivered',
		self::STATUS_PENDING => 'Pending',
	);

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function beforeSave() 
	{
	    if ($this->isNewRecord)
	    {
	    	$this->created_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	        $this->created_at = new CDbExpression('NOW()');
	    }
	 
	 	$this->modified_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	    $this->modified_at = new CDbExpression('NOW()');
	 
	    return parent::beforeSave();
	}

	public function relations()
	{
		return array_merge(
			parent::relations(), 
			array(
				'orderProductValues' => array(self::HAS_MANY, 'OrderProduct', 'order_id'),
				'products' => array(self::MANY_MANY, 'Product', 'order_product(order_id, product_id)'),
			)
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'code' => Yii::t('app', 'Code'),
			'order_date' => Yii::t('app', 'Order Date'),
			'approved_date' => Yii::t('app', 'Approved Date'),
			'approved_person' => Yii::t('app', 'Approved Person'),
			'estimated_delivery_time' => Yii::t('app', 'Estimated Delivery Time'),
			'actual_delivery_time' => Yii::t('app', 'Actual Delivery Time'),
			'delivery_person' => Yii::t('app', 'Delivery Person'),
			'shipping_address' => Yii::t('app', 'Shipping Address'),
			'status' => Yii::t('app', 'Status'),
			'payment_method' => Yii::t('app', 'Phương thức thanh toán'),
			'user_id' => Yii::t('app', 'User'),
			'customer_name' => Yii::t('app', 'Customer Name'),
			'email' => Yii::t('app', 'Email'),
			'phone' => Yii::t('app', 'Phone'),
			'note' => Yii::t('app', 'Note'),
			'created_by' => Yii::t('app', 'Created By'),
			'modified_by' => Yii::t('app', 'Modified By'),
			'created_at' => Yii::t('app', 'Created At'),
			'modified_at' => Yii::t('app', 'Modified At'),
		);
	}

	public function search() 
	{
		$dataProvider = parent::search();
        return $dataProvider;
	}

	public function shopSearch() 
	{
		$dataProvider = parent::search();
		$criteria = $dataProvider->getCriteria();
		$cri = array('order' => 'modified_at DESC');

		if( !empty(Yii::app()->user->shop_id))
		{
			$cri = array_merge($cri, array(
				'condition' => 'shop_id = :shopID',
            	'params' => array(':shopID' => Yii::app()->user->shop_id)	
			));
		}

		$criteria->with = array('products');
		$criteria->mergeWith($cri);
		$dataProvider->setCriteria( $criteria );
        
        return $dataProvider;
	}

	public function generateOrderCode()
	{
		$random_hash = substr(md5(uniqid(rand(), true)), -31, 8);	// get 8 characters from the end of the string
		$found = $this->findByAttributes(array('code' => $random_hash));
		while(!empty($found))
		{
			$random_hash = substr(md5(uniqid(rand(), true)), -31, 8);
			$found = $this->findByAttributes(array('code' => $random_hash));
		}
		return $random_hash;
	}

	public function getStatusText(){
		$status =  array(
			Order::STATUS_NEW => Yii::t('app', 'New'), 
			Order::STATUS_APPROVED => Yii::t('app', 'Approved'),
			Order::STATUS_DELIVERED => Yii::t('app', 'Delivered'),
			Order::STATUS_PENDING => Yii::t('app', 'Pending'),
		);
		return $status;
	}

	/**
	*	this function is no longer in use
	*
	*/
	public function getProductByOrder($id)
	{
		return Yii::app()->db->createCommand()
		   ->select('p.name, op.price,op.id as order_product_id, op.quantity, op.total_price, op.id')
		   ->from('order_product op')
		   ->leftJoin('product p', 'p.id=op.product_id')
		   ->leftJoin('order_attribute oa', 'oa.order_product_id=op.id')
		   ->where('op.order_id=:id', array(':id'=>$id))
		   ->queryAll();

	}

	public function changeStatus($status = NULL, $htmlOptions = array())
	{
		$statusText =  array(
				Order::STATUS_NEW => Yii::t('app', 'New'), 
				Order::STATUS_APPROVED => Yii::t('app', 'Approved'),
				Order::STATUS_DELIVERED => Yii::t('app', 'Delivered'),
				Order::STATUS_PENDING => Yii::t('app', 'Pending'),
			);
		if($status == Order::STATUS_PENDING || $status == Order::STATUS_DELIVERED)
			return isset($statusText[$status]) ? $statusText[$status] : NULL;
		if($status == Order::STATUS_NEW)
			return CHtml::dropDownList('changeStatus',$statusText[$status], array(
					'current' => Yii::t('app', 'New'), 
					Order::STATUS_APPROVED => Yii::t('app', 'Approved'),
					Order::STATUS_DELIVERED => Yii::t('app', 'Delivered')
				),
				$htmlOptions
			);
		if($status == Order::STATUS_APPROVED)
			return CHtml::dropDownList('changeStatus',$statusText[$status], array(
					'current' => Yii::t('app', 'Approved'),
					Order::STATUS_NEW => Yii::t('app', 'New'), 
					Order::STATUS_DELIVERED => Yii::t('app', 'Delivered')
				),
				$htmlOptions
			);
	}
	public function getInfoOrder()
	{
		$str = '';
		if(is_array($this->products) && is_array($this->orderProductValues))
		{
			foreach( $this->products as $i => $a )
			{
				$str .= '<br/>'. $a->name . '<br/>';
				$priceInfo =  
					'<b>Price: </b>'. number_format($this->orderProductValues[$i]->price) . 
					'<b> / Quantity: </b>'. $this->orderProductValues[$i]->quantity. 
					'<b> / Subtotal: </b>'.  number_format($this->orderProductValues[$i]->quantity * $this->orderProductValues[$i]->price). '<br/>' ;
				if(is_array($this->orderProductValues[$i]->orderAttributes))
				{
					$productAttributeIds = array();
					foreach($this->orderProductValues[$i]->orderAttributes as $oa )
						$productAttributeIds[] = $oa->product_attribute_id;
					
					$attrCriteria = new CDbCriteria;
					$attrCriteria->addInCondition('t.id', $productAttributeIds);

					$productAttribute = ProductAttribute::model()->with('attribute')->findAll($attrCriteria);
					foreach( $productAttribute as $pa )
						$str .= $pa->attribute->name. ': '. $pa->value. '<br/>';
				}

				$str .= $priceInfo;
				$str .='-----------------------------------------------------------------------------------------------------------------';

			}
		}
		return $str;
	}

	public function getCustomerOrder()
	{
		
		$str = '<b>Code: '. '<span  style="color: #CD1231;">'. strtoupper($this->code). '</span></b><br/>';
		$str .= '<b>Customer: </b>'. $this->customer_name;
		$str .= '<br/><b>Phone: </b>'. $this->phone . '<br><b>Email: </b>'. $this->email ;
		$str .= '<br/><b>Shipping Address: </b>'. $this->shipping_address;
		if(!empty($this->note))
			$str .= '<br/><b>Note</b>: '. $this->note;
		$str .= '<br/><b>TOTAL: <span  style="color: #CD1231;">'. number_format($this->total) .'</span></b>';
		return $str;
	}

	public function getTotalOrder()
	{
		$price = Yii::app()->db->createCommand()
							   ->select('sum(op.total_price) as total_price')
							   ->from('order_product op')
							   ->where('op.order_id=:id', array(':id'=>$this->id))
							   ->queryRow();
		setlocale(LC_MONETARY, 'vi_VN');
		return empty($price['total_price']) ? '0VND' : money_format('%i', $price['total_price']);
	}

	public function getStatusName()
	{
		if($this->status)
			return $this->statusText[$this->status];
		return false;
	}

	public function getOrderByUserId($userId)
	{
		$orderDetail = array();
		$order = Yii::app()->db->createCommand()
								->select('o.id, o.code, o.order_date, o.status, total')
								->from('order o')
								->where('o.user_id=:user_id', array(':user_id' => $userId))
								->queryAll();
		if(!empty($order)){
			foreach ($order as $k=>$val) {
				$product['product'] = $this->getProductByOrder($val['id']);
				$order[$k] = array_merge($order[$k], $product);
			}
		}
		return $order;
	}
}