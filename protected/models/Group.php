<?php

Yii::import('application.models._base.BaseGroup');

class Group extends BaseGroup
{
	const GROUP_ADMIN = 1;
	const GROUP_SHOP = 2;
	const GROUP_USER = 1;
	
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function listGroup($condition = NULL)
	{
		return CHtml::listData(Group::model()->findAll(array('select' => 'id,name', )) ,'id', 'name');
	}
}