<?php

Yii::import('application.models._base.BaseProductImage');

class ProductImage extends BaseProductImage
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function beforeValidate()
	{
		if( empty( $this->created_by) )
			$this->created_by = 1;
		if( empty( $this->created_at) )
			$this->created_at = new CDbExpression('NOW()');

		return parent::beforeValidate();
	}

	public function beforeSave() 
	{
	    if ($this->isNewRecord)
	    {
	    	$this->created_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	        $this->created_at = new CDbExpression('NOW()');
	    }
	 
	 	$this->modified_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	    $this->modified_at = new CDbExpression('NOW()');
	 
	    return parent::beforeSave();
	}

	


	/**
	* @author 	Klaus
	* @since 	2014 - 06 - 24
	* @param 	$id : image id
	*/
	public function makeActive( $id )
	{
		$image = $this->findByPk( $id );
		$this->updateAll(
			array('is_actived' => 0),
			'product_id=:productId',
			array(':productId' => $image->product_id)
		);
		$this->updateByPk($id, array('is_actived' => 1));
	}	

	/**
	* @author 	Oanh Le Thuy
	* @since 	2014 - 06 - 24
	*/
	public function getImageForProductId($product_id){
		return Yii::app()->db->createCommand()
		   ->select('pi.name')
		   ->from(' product_image pi')
		   ->where('pi.product_id=:product_id', array(':product_id'=>$product_id))
		   ->order('pi.modified_at desc')
		   ->queryAll();
	}
}