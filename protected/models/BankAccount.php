<?php

Yii::import('application.models._base.BaseBankAccount');

class BankAccount extends BaseBankAccount
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
			array('name, bank_name, number, created_by, created_at', 'required'),
			array('created_by, modified_by', 'numerical', 'integerOnly'=>true),
			array('name, bank_name', 'length', 'max'=>100),
			array('number', 'length', 'max'=>50),
			array('modified_at', 'safe'),
			array('modified_by, modified_at', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name, bank_name, number, created_by, modified_by, created_at, modified_at', 'safe', 'on'=>'search'),
		);
	}

	public function beforeValidate()
	{
		if( empty( $this->created_by) )
			$this->created_by = 1;
		if( empty( $this->created_at) )
			$this->created_at = new CDbExpression('NOW()');

		return parent::beforeValidate();
	}
	
	public function beforeSave() 
	{
	    if ($this->isNewRecord)
	    {
	    	$this->created_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	        $this->created_at = new CDbExpression('NOW()');
	    }
	 
	 	$this->modified_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	    $this->modified_at = new CDbExpression('NOW()');
	 
	    return parent::beforeSave();
	}

	public function shopSearch() 
	{
		$dataProvider = parent::search();
		$cri = array(
			'order' => 'created_at ASC'
		);

		if( !empty(Yii::app()->user->shop_id))
			$cri = array_merge($cri, array(
				'condition' => 'shop_id = :shopID',
            	'params' => array(':shopID' => Yii::app()->user->shop_id)	
			));

        $dataProvider->getCriteria()->mergeWith($cri);
        return $dataProvider;
	}
}