<?php

Yii::import('application.models._base.BaseSetting');

class Setting extends BaseSetting
{
	public function rules()
	{
	    return array(
	    	array('name', 'required'),
	    	array('google_maps', 'required'),
	        array('address', 'required', 'on' => 'reset'),
	        array('email', 'email'),
	        // array('skype_id', 'email'),
	        array('fb_id', 'url', 'defaultScheme' => 'http'),
	        array('yahoo_id', 'required', 'on' => 'reset'),
	        array('phone', 'required', 'on' => 'reset'),
	        array('phone', 'numerical'),
	        array('cellphone', 'numerical'),
	        array('contact_content', 'required', 'on' => 'reset'),
	        array('contact_content, meta_description, meta_keyword, meta_title', 'safe'),
	        array('address, phone, cellphone, longtitude, google_maps,  latitude, contact_content, fb_id, skype_id, yahoo_id, currency', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name, address, phone, cellphone, google_maps, longtitude, latitude, contact_content, fb_id, skype_id, yahoo_id, currency', 'safe', 'on'=>'search'),
	    );
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function generateData(){
		$setting = new Setting();
		$setting->id = 1;
		$setting->address = '97 Đông Nhì, phường Lái Thiêu, thị xã Thuận An, Bình Dương ';
		$setting->contact_content = "<p>Chất lượng và Đúng hẹn / Quality and On time</p><p>Phục vụ vì chữ Tâm và chữ Tín / Served for Humanity and On time</p>";
		$setting->google_maps = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7835.3545937725685!2d106.7086312277326!3d10.912111205938444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d79b9764894f%3A0x51d86bd9af584108!2zOTcgxJDDtG5nIE5ow6wsIEzDoWkgVGhpw6p1LCB0eC4gVGh14bqtbiBBbiwgQsOsbmggRMawxqFuZywgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1406775936134" width="400" height="400" frameborder="0" style="border:0"></iframe>';
		$setting->phone = '0987654321';
		$setting->email = 'contact@anastores.vn';
		$setting->name = Yii::app()->name;
		$setting->save();
	}
}