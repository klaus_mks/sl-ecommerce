<?php

class StatisticForm extends CFormModel
{
	public $from_date;
	public $to_date;

	public function rules()
	{
		return array(
			//array('from_date, to_date', 'required', 'message' => 'Vui lòng nhập ngày tháng thống kê'),
			array('from_date', 'date', 'format'=> 'dd-MM-yyyy', 'message' => 'Please enter proper format (dd-mm-YYYY) for beginning date'),
			array('to_date', 'date', 'format'=> 'dd-MM-yyyy', 'message' => 'Please enter proper format (dd-mm-YYYY) for ending date'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			//'rememberMe'=>'Remember me next time',
		);
	}

}
