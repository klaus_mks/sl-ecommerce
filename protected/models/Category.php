<?php

Yii::import('application.models._base.BaseCategory');

class Category extends BaseCategory
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function beforeValidate()
	{
		if( empty( $this->created_by) )
			$this->created_by = 1;
		if( empty( $this->created_at) )
			$this->created_at = new CDbExpression('NOW()');

		return parent::beforeValidate();
	}

	public function beforeSave() 
	{
	    if ($this->isNewRecord)
	    {
	    	$this->created_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	        $this->created_at = new CDbExpression('NOW()');
	    }
	 
	 	$this->modified_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	    $this->modified_at = new CDbExpression('NOW()');
	 
	    return parent::beforeSave();
	}

	public function behaviors()
	{
	    return array(
	        'i18n-columns' => array(
	             'class' => 'I18nColumnsBehavior',
	             /* The multilingual attributes */
	             'translationAttributes' => array(
	                  'name',
	                  'description'
	             ),
	        ),
	    );
	}

	public function getAncestorsBreadCrumb($route, $currentToLink = false)
	{
		$res = $currentToLink ? 
			array( $this->name => Yii::app()->createUrl($route, array('id' => $this->id, 'title' => $this->name))) 
				: array($this->name);
		if(!empty($this->parent))
			$res = array_merge( $res, 
				array(
					$this->parent->name => Yii::app()->createUrl($route, array('id' => $this->parent->id, 'title' => $this->parent->name))
				)
			);

		if(!empty($this->parent->parent))
			$res = array_merge( $res, array($this->parent->parent->name => Yii::app()->createUrl($route, array('id' => $this->parent->parent->id, 'title' => $this->parent->parent->name))));
		$res = array_reverse( $res );

		return $res;

	}


	public function getImage( $width = 0, $height = 0)
	{
		if(empty($this->image))
			return ShoppyHelper::getNoImage( $width, $height );
		else
			return ShoppyHelper::getThumbnailPath( Yii::app()->params['category']['image_path']. DIRECTORY_SEPARATOR. $this->image, $width, $height ); 
	}

	public function shopSearch() 
	{
		$dataProvider = parent::search();
		$cri = array(
			'order' => 'parent_id ASC'
		);

		if( !empty(Yii::app()->user->shop_id))
			$cri = array_merge($cri, array(
				'condition' => 'shop_id = :shopID',
            	'params' => array(':shopID' => Yii::app()->user->shop_id)	
			));

        $dataProvider->getCriteria()->mergeWith($cri);
        return $dataProvider;
	}

	public function listCate($condition = NULL, $options = array())
	{
		return CHtml::listData(Category::model()->findAll(array(
			//'select' => 'id,name',
			'condition' => $condition != NULL ? $condition : 'is_published = true'
		)) , 'id', 'name');
	}

	public function generateTestData()
	{
		for( $i = 0; $i < 50; $i ++ )
		{
			
			$cat = new Category();
			$cat->name = 'Category Name' . ( $i + 1 );
			$cat->description = 'description' . md5(rand());
			$cat->seo_description = 'seo_description' . md5(rand());
			$cat->created_by = 1;
			$cat->created_at = date('Y-m-d H:i:s', time());
			$cat->save();
			//sleep(1);
		}
	}
}	