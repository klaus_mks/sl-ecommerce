<?php

Yii::import('application.models._base.BaseBanner');

class Banner extends BaseBanner
{
	const POS_1 = 1;
	const POS_2 = 2;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function beforeValidate()
	{
		if( empty( $this->created_by) )
			$this->created_by = 1;
		if( empty( $this->created_at) )
			$this->created_at = new CDbExpression('NOW()');

		return parent::beforeValidate();
	}
	
	public function beforeSave() 
	{
	    if ($this->isNewRecord)
	    {
	    	$this->created_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	        $this->created_at = new CDbExpression('NOW()');
	    }
	 
	 	$this->modified_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	    $this->modified_at = new CDbExpression('NOW()');
	 
	    return parent::beforeSave();
	}

	public function getPosition()
	{
		return array(self::POS_1 => 'Upper', self::POS_2 => 'Lower');
	}
}