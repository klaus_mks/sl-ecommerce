<?php

Yii::import('application.models._base.BasePost');

class Post extends BasePost
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function beforeValidate()
	{
		if( empty( $this->created_by) )
			$this->created_by = 1;
		if( empty( $this->created_at) )
			$this->created_at = new CDbExpression('NOW()');

		return parent::beforeValidate();
	}

	public function beforeSave() 
	{
	    if ($this->isNewRecord)
	    {
	    	$this->created_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	        $this->created_at = new CDbExpression('NOW()');
	    }
	 
	 	$this->modified_by = Yii::app()->user->id ? Yii::app()->user->id : NULL;
	    $this->modified_at = new CDbExpression('NOW()');
	 
	    return parent::beforeSave();
	}

	public function behaviors()
	{
	    return array(
	        'i18n-columns' => array(
	             'class' => 'I18nColumnsBehavior',
	             /* The multilingual attributes */
	             'translationAttributes' => array(
	                  'title',
	                  'content'
	             ),
	        ),
	    );
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 03
	*
	*/
	public function getPostImage($width = 0, $height = 0)
	{
		if(!$this->image)
			return false;
		return '/'. Yii::app()->params['general']['thumb_filename']. '?src='. 
					Yii::app()->params['post']['image_path']. DIRECTORY_SEPARATOR. $this->image. "&w=$width&h=$height";
	}
	
	public function shopSearch() 
	{
		$dataProvider = parent::search();
		$cri = array(
			'order' => 'id ASC'
		);

		if( !empty(Yii::app()->user->shop_id))
			$cri = array_merge($cri, array(
				'condition' => 'shop_id = :shopID',
            	'params' => array(':shopID' => Yii::app()->user->shop_id)	
			));

        $dataProvider->getCriteria()->mergeWith($cri);
        return $dataProvider;
	}

}