<?php

class CheckoutForm extends CFormModel
{
	public $customer_name;
	public $email;
	public $phone;
	public $shipping_address;
	public $delivery_person;
	public $payment_method;
	public $note;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('customer_name', 'required', 'message' => 'Vui lòng nhập tên khách hàng'),
			array('email', 'required', 'message' => 'Vui lòng nhập email'),
			array('phone', 'required', 'message' => 'Vui lòng nhập số điện thoại'),
			array('shipping_address', 'required', 'message' => 'Vui lòng nhập địa chỉ giao hàng'),
			array('delivery_person', 'required', 'message' => 'Vui lòng nhập tên người nhận'),
			array('payment_method', 'required', 'message' => 'Vui lòng nhập phương thức thanh toán'),

			array('phone', 'numerical')
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			//'rememberMe'=>'Remember me next time',
		);
	}

	public function getLoginInfo()
	{
		if(Yii::app()->user->isGuest)
			return false;

		$this->customer_name = Yii::app()->user->first_name. ' '. Yii::app()->user->last_name;
		$this->email = Yii::app()->user->email;						
	}

}
