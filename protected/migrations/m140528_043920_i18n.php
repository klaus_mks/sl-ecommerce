<?php
class m140528_043920_i18n extends CDbMigration
{
    public function up()
    {
        $this->renameColumn('post', 'title', 'title_en');
        $this->renameColumn('post', 'content', 'content_en');
        $this->addColumn('post', 'title_vi', 'varchar(255) not null');
        $this->addColumn('post', 'content_vi', 'text null');
    }

    public function down()
    {
      $this->renameColumn('post', 'title_en', 'title');
      $this->renameColumn('post', 'content_en', 'content');
      $this->dropColumn('post', 'title_vi');
      $this->dropColumn('post', 'content_vi');
    }
}
