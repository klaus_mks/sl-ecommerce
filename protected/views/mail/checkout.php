<!-- this email is sent to customers as soon as they have finished checking out -->
Xin chào <?php echo $order->customer_name; ?>,<br/>
<br/>
Cảm ơn bạn đã mua hàng tại <?php echo Yii::app()->name; ?>.<br/>
THÔNG TIN ĐƠN HÀNG CỦA BẠN:<br/>
---------------------------------------------<br/>
Mã đơn hàng: <?php echo strtoupper($order->code); ?><br/>
---------------------------------------------<br/>
<?php 
	$totalValue = 0;
	foreach( $cartSession as $cs ): 
		$totalValue += $cs['quantity'] * $cs['price'];
?>
<?php echo $cs['name']; ?><br/>
<?php if(!empty($cs['attribute'])) : ?>
      <?php foreach( $cs['attribute'] as $i => $attributeId ): ?>
      <?php 
      	$attrList = CHtml::listData( $attribute, 'id', 'name' );
      	$attrValList = CHtml::listData( $attributeValue, 'id', 'value' );
      ?>
        <span><?php echo $attrList[$attributeId]; ?>:</span>
        <span class="value"><?php echo $attrValList[$cs['attribute_value'][$i]]; ?></span>
        <br/>
  	  <?php endforeach; ?>
<?php endif; ?>
Số lượng: <?php echo $cs['quantity']; ?><br/>
Đơn giá: <?php echo number_format($cs['price']); ?><br/>
Số tiền: <?php echo number_format($cs['price'] * $cs['quantity']); ?><br/>
---------------------------------------------<br/>
<?php endforeach; ?>
TỔNG SỐ TIỀN: <?php echo number_format($totalValue); ?><br/>
---------------------------------------------<br/><br/>
THÔNG TIN GIAO HÀNG: <br/>
<br/>
Tên khách hàng: <?php echo $order->customer_name; ?><br/>
Email: <?php echo $order->email; ?><br/>
Số điện thoại: <?php echo $order->phone; ?><br/>
Địa chỉ giao hàng: <?php echo $order->shipping_address; ?><br/>
Người nhận : <?php echo $order->delivery_person; ?><br/>
<?php if(!empty($order->note)): ?>
	Ghi chú: <?php echo $order->note; ?><br/>
<?php endif; ?>
<br/>
<?php if($order->payment_method == Order::PAYMENT_METHOD_TRANSFER && !empty($bankAccount)): ?>
	Bạn vui lòng chuyển khoản vào <?php if(count($bankAccount) > 1) echo 'một trong những'; ?> tài khoản sau:<br/>
		---------------------------------------------<br/>
	<?php foreach( $bankAccount as $b ): ?>
		Ngân hàng: <?php echo $b->bank_name; ?><br/>
		Chủ tài khoản: <?php echo $b->name; ?><br/>
		Số tài khoản: <?php echo $b->number; ?><br/>
		---------------------------------------------<br/>
	<?php endforeach; ?>
		Vui lòng gửi kèm mã đơn hàng của bạn: <?php echo strtoupper($order->code); ?> trong thông tin chuyển khoản<br/>
		---------------------------------------------<br/>
<?php endif; ?>
Chúng tôi sẽ kiểm tra đơn hàng của bạn và sẽ liên hệ với bạn trong thời gian sớm nhất.<br/>
<br/>
Trân trọng,<br/>
<?php echo Yii::app()->name; ?>.
