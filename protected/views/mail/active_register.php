<pre>
Dear <?php echo $dataMail['name']?>, </br> 
</br>
Your membership with <?php echo $dataMail['link'];?> requires you to validate your email address. </br>

Please click on the link below to validate your email address. </br>

<?php echo $dataMail['linkActive']?></br>
(If the link does not work, copy the full address and paste it to your Internet browser) </br>
</br>
Our email validation is intended to confirm that your email is authentic and working. </br>
</br>
Best Regards,</br>
<?php echo $dataMail['link'];?> </br>
</pre>