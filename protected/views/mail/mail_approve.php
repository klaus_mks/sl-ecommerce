Thông báo đặt hàng thành công tại <?php echo Yii::app()->name;?><br/>
<br/>
Xin chào <?php echo $model->customer_name; ?>,<br/>
<br/>
Cảm ơn bạn đã mua hàng tại <?php echo Yii::app()->name; ?>.<br/>
THÔNG TIN ĐƠN HÀNG CỦA BẠN<br/>
---------------------------------------------<br/>
Mã đơn hàng: <?php echo strtoupper($model->code); ?><br/>
---------------------------------------------<br/>
<?php 
	$total = 0;
	foreach ($products as $k=>$product) : 
		$total = $total + $product['total_price'];
		$productAttribute = ProductAttribute::model()->getAttributeById($product['id']);
?>
<?php echo $product['name']; ?><br/>
Số lượng: <?php echo $product['quantity']; ?><br/>
Đơn giá: <?php echo number_format($product['price']); ?><br/>
Số tiền: <?php echo number_format($product['total_price']); ?><br/>
---------------------------------------------<br/>
<?php endforeach; ?>
TỔNG SỐ TIỀN: <?php echo number_format($totalValue); ?><br/>
---------------------------------------------<br/><br/>
THÔNG TIN GIAO HÀNG: <br/>
<br/>
Tên khách hàng: <?php echo $model->total; ?><br/>
Email: <?php echo $model->email; ?><br/>
Số điện thoại: <?php echo $model->phone; ?><br/>
Địa chỉ giao hàng: <?php echo $model->shipping_address; ?><br/>
Người nhận : <?php echo $model->delivery_person; ?><br/>
<?php if(!empty($model->note)): ?>
	Ghi chú: <?php echo $model->note; ?><br/>
<?php endif; ?>
<br/>
<?php if($model->payment_method == Order::PAYMENT_METHOD_TRANSFER && !empty($bankAccount)): ?>
	Bạn vui lòng chuyển khoản vào <?php if(count($bankAccount) > 1) echo 'một trong những'; ?> tài khoản sau:<br/>
		---------------------------------------------<br/>
	<?php foreach( $bankAccount as $b ): ?>
		Ngân hàng: <?php echo $b->bank_name; ?><br/>
		Chủ tài khoản: <?php echo $b->name; ?><br/>
		Số tài khoản: <?php echo $b->number; ?><br/>
		---------------------------------------------<br/>
	<?php endforeach; ?>
		Vui lòng gửi kèm mã đơn hàng của bạn: <?php echo strtoupper($model->code); ?> trong thông tin chuyển khoản<br/>
		---------------------------------------------<br/>
<?php endif; ?>
Nhân viên giao nhận của chúng tôi sẽ liên hệ và giao sản phẩm cho bạn trong vòng 2 đến 3 ngày làm việc.
<br/>
Trân trọng,<br/>
<?php echo Yii::app()->name; ?>.
