<!-- this is email is sent to shop owner as soon as a customer has finished checking out -->
<?php echo Yii::app()->name; ?> vừa ghi nhận thông tin đơn hàng mới.<br/>
<br/>
---------------------------------------------<br/>
Mã đơn hàng: <?php echo strtoupper($order->code); ?><br/>
---------------------------------------------<br/>
<?php 
	$totalValue = 0;
	foreach( $cartSession as $cs ): 
		$totalValue += $cs['quantity'] * $cs['price'];
?>
<?php echo $cs['name']; ?><br/>
<?php if(!empty($cs['attribute'])) : ?>
      <?php foreach( $cs['attribute'] as $i => $attributeId ): ?>
      <?php 
      	$attrList = CHtml::listData( $attribute, 'id', 'name' );
      	$attrValList = CHtml::listData( $attributeValue, 'id', 'value' );
      ?>
        <span><?php echo $attrList[$attributeId]; ?>:</span>
        <span class="value"><?php echo $attrValList[$cs['attribute_value'][$i]]; ?></span>
        <br/>
  	  <?php endforeach; ?>
<?php endif; ?>
Số lượng: <?php echo $cs['quantity']; ?><br/>
Đơn giá: <?php echo number_format($cs['price']); ?><br/>
Số tiền: <?php echo number_format($cs['price'] * $cs['quantity']); ?><br/>
---------------------------------------------<br/>
<?php endforeach; ?>
TỔNG SỐ TIỀN: <?php echo number_format($totalValue); ?><br/>
---------------------------------------------<br/><br/>
THÔNG TIN GIAO HÀNG: <br/>
<br/>
Tên khách hàng: <?php echo $order->customer_name; ?><br/>
Email: <?php echo $order->email; ?><br/>
Số điện thoại: <?php echo $order->phone; ?><br/>
Địa chỉ giao hàng: <?php echo $order->shipping_address; ?><br/>
Người nhận : <?php echo $order->delivery_person; ?><br/>
<?php if(!empty($order->note)): ?>
	Ghi chú: <?php echo $order->note; ?><br/>
<?php endif; ?>
<br/>
<?php echo Yii::app()->name; ?>.
