<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('name')); ?>:
	<?php echo GxHtml::encode($data->name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_published')); ?>:
	<?php echo GxHtml::encode($data->is_published); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('shop_id')); ?>:
	<?php echo GxHtml::encode($data->shop_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('description')); ?>:
	<?php echo GxHtml::encode($data->description); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('seo_description')); ?>:
	<?php echo GxHtml::encode($data->seo_description); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('created_by')); ?>:
	<?php echo GxHtml::encode($data->created_by); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('modified_by')); ?>:
	<?php echo GxHtml::encode($data->modified_by); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('created_at')); ?>:
	<?php echo GxHtml::encode($data->created_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('modified_at')); ?>:
	<?php echo GxHtml::encode($data->modified_at); ?>
	<br />
	*/ ?>

</div>