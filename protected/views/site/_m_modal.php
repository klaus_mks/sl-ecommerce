<!-- this is the view for modal box on mobile -->
<div class="modal-wrapper">
	<div class="modal-inner">
		<div class="modal-box">
			<p class="m-modal-close"><span><?php echo Yii::t('front', 'Close'); ?></span></p>
			<div class="detail-info data-container1">
				<div class="detail-table data-container">
					<table>
	                    <tbody>
		                    <tr>
		                        <th><div class="x-positionR">保存条件名<a href="save/edit/000/" class="icnEdit">編集する</a></div></th>
		                    </tr>
		                    <tr>
		                        <td>
		                        	Javaエンジニア
		                        </td>
		                    </tr>
		                    <tr>
		                        <th>検索条件</th>
		                    </tr>
		                    <tr>
		                        <td>
		                            経験言語：Java<br>
		                                                            職種：エンジニア<br>
		                                                            希望契約形態：業務委託
		                        </td>
		                    </tr>
		                </tbody>
	                </table>	
				</div>
				
			</div>
		</div>
	</div>
</div>

