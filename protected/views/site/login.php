<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
	$this->pageTitle = 'Login';
	$this->breadcrumbs = array(
		'Login',
	);
?>

<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
		'htmlOptions' => array('class' => 'form-2')
	)); 

	
?>

	<h1><!-- <span class="log-in">Log in</span> or  --><span class="sign-up"><a href="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>">login</a></span></h1>
	<p class="float">
		<label for="login"><i class="icon-user"></i>Email</label>
		<?php echo $form->textField($model,'username', array('placeholder' => 'john@mail.com')); ?>
	</p>
	<p class="float">
		<label for="password"><i class="icon-lock"></i>Password</label>
		<?php echo $form->passwordField($model,'password'); ?>
	</p>
	<p class="clearfix"> 
		<?php echo $form->error($model,'username'); ?>
		<?php echo $form->error($model,'password'); ?>
	</p>

	<p class="clearfix"> 
		<a href="<?php echo Yii::app()->createUrl('site/forgotPassword'); ?>" class="forgot-password">Forgot password?</a>    
		<input type="submit" name="submit" value="Log in">
	</p>
<?php $this->endWidget(); ?>


