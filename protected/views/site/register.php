<style type="text/css">
    small {
        font-size: 100%;
    }
</style>
<div role="main" class="container products grid">  
    <?php $this->breadcrumbs = array('Hình thức thanh toán');   ?>      
    <div class="row">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'register-form',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>false,
        ),
    )); ?>
    <?php if(Yii::app()->user->hasFlash('success')) : ?> 
    
    <div class="alert alert-success alert-dismissable">
        <span><?php echo Yii::app()->user->getFlash('success'); ?><span> 
    </div>
    
    <?php endif; ?>
     <!-- <form id="defaultForm"> -->
        <fieldset>
            <div class="span5 checkout checkout-list right-col input">
                <div class="reg_left fleft">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Email</label>
                        <div class="col-lg-5">
                            <?php echo $form->textField($model, 'email', array('placeholder' => 'Email...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'email'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Password'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->passwordField($model, 'password', array('type' => 'password', 'placeholder' => 'Mật khẩu...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'password'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Reenter password'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->passwordField($model, 'repeat_password', array('type' => 'password', 'placeholder' => 'Nhập lại mật khẩu...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'repeat_password'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Phone'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->textField($model, 'phone', array('placeholder' => 'Điện thoại...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'phone'); ?>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="span6 offset1 checkout checkout-list right-col input">
                <div class="reg_left fleft">
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Last name'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->textField($model, 'last_name', array('placeholder' => 'Họ...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'last_name'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'First name'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->textField($model, 'first_name', array('placeholder' => 'Tên...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'first_name'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo Yii::t('front', 'Birthday'); ?></label>
                        <div class="col-lg-5">
                            <?php echo $form->textField($model, 'birthday', array('placeholder' => Yii::t('front', 'Ex: 14-07-1996'). '...','class' => 'text-input')); ?>
                            <?php echo $form->error($model,'birthday'); ?>
                        </div>
                    </div>

                    <div class="reg_btn_div">
                        <a style="margin-top: 25px" class="sbtn sbtn-normal lnk-submit" href="javascript:void(0);">
                            <span class="gradient"><?php echo Yii::t('front', 'Register'); ?></span>
                        </a>
                    </div> 
                </div>
            </div>
    <!-- </form> -->
    <?php $this->endWidget(); ?>
    </div>
</div>
<?php Yii::app()->clientScript->registerScriptFile('/js/common.js', CClientScript::POS_BEGIN);?>