<?php 
    $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'product-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('class' => 'form-horizontal', 'role' => "form")
    ));
?>
<h3><?php echo Yii::t('Activate','Activate your Shoppy account');?></h3>
<div class="span5 checkout checkout-list right-col input">
    <div class="reg_left fleft">
<?php 
if(isset($_GET['user']) && isset($_GET['pass']))
{
	$listId = CHtml::listData(
        User::model()->findAll(array(
                    'select' => 'id',
                    'condition' => 'email = "'.$_GET['user'].'" and password = "'.$_GET['pass'].'" and is_activated=0 '
        )), 
        'id', 'id'
    );
    if(!empty($listId)){
        //convert array id to string
        $ids = "('".implode("','",$listId)."')";
        echo "Successfull Activate";
        //update status
        User::model()->updateAll(
            array('is_activated'=> 1),
            'id in'.$ids
        );
    }
    else
    {
    	$this->redirect(array('/site/home'));
   	}
}
?>
</div>
</div>

<?php $this->endWidget(); ?>
