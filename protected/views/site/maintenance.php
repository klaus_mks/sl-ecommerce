<!-- maintenance -->
<?php
	Yii::app()->clientScript->registerCss('maintenance', '.center-block {
       position: absolute;
       width: 500px;
       height: 300px;
       top: 50%;
       left: 50%;
       margin-left: -250px;
       margin-top: -150px;
       border: 1px solid black;
}
body {
	background-color: #000000;
	position: relative;
}
.login-link
{
	position: absolute;
	bottom: 20px;
	right: 20px;
	color: #424142;
}');

?>

<div class="center-block">
	<img src="/images/under_construction.jpg">
</div>
<a class="login-link" href="<?php echo Yii::app()->createUrl('site/login'); ?>">Login</a>