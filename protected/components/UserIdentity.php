<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 * @link http://www.yiiframework.com/wiki/425	-- comparing password
	 * @link http://php.net/manual/en/function.crypt.php
	 */
	public function authenticate()
	{
		$record=User::model()->findByAttributes(array('email'=>$this->username));	// use email versus authentication rather than username
        if($record===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if($record->password!==crypt($this->password,$record->password))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
            $this->_id=$record->id;
            $this->setState('email', $record->email);
            $this->setState('first_name', $record->first_name);
            $this->setState('last_name', $record->last_name);
            $this->setState('shop_id', $record->shop_id );
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
	}

	public function getId()
    {
        return $this->_id;
    }

}