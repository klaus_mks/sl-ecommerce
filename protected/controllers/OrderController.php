<?php
Yii::import('application.controllers.AdminController');
class OrderController extends AdminController 
{

	public function filters() {
		return array(
			'accessControl', 
		);
	}

	public function accessRules() {
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','create', 'update', 'delete', 'statistic', 'imageUpload', 'imageLoad', 'deleteImage', 'activeImage'),
				//'users'=>array('admin@mail.com', 'shop1@mail.com'),
				'roles' => array('admin', 'shop')
			),

			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionStatistic()
	{	
		$model = new StatisticForm;

		$criteria = new CDbCriteria;
		$criteria->select = '*';
		$criteria->with = array('products', 'orderProductValues');
		$criteria->addCondition('t.status = :status');
		$criteria->params = array(':status' => Order::STATUS_DELIVERED);
		$criteria->order = 't.order_date DESC';

		if (Yii::app()->getRequest()->getIsPostRequest()) 
		{
			if(isset($_POST['StatisticForm']))
			{
				$model->attributes = $_POST['StatisticForm'];
				if($model->validate())
				{
					if(!empty($_POST['StatisticForm']['from_date']))
					{
						$criteria->addCondition('t.order_date >= :from');
						$criteria->params = array_merge($criteria->params, 
							array(':from' => date('Y-m-d H:i:s', strtotime($_POST['StatisticForm']['from_date']))));
					}

					if(!empty($_POST['StatisticForm']['to_date']))
					{
						$criteria->addCondition('t.order_date <= :to');
						$criteria->params = array_merge($criteria->params, 
							array(':to' => date('Y-m-d H:i:s', strtotime($_POST['StatisticForm']['to_date']))));
					}
				}
			}
		}

		$sumCriteria = clone $criteria;
		$sumCriteria->select = 'sum(t.total) as total';
		$sumCriteria->with = NULL;
		$total = Order::model()->find($sumCriteria);

		$dataProvider = new CActiveDataProvider( Order::model(), array('criteria' => $criteria));
		$this->render('statistic', array('total' => $total, 'dataProvider' => $dataProvider, 'model' => $model));
	}

	/**
	* @author 	Oanh Le
	* @since 	2015 - 01 - 05
	*/
	public function actionUpdate($id) {

		$model = $this->loadModel($id, 'Order');
		$orderProducts = OrderProduct::model()
			->with('product', 'orderAttributes')
			->findAll('t.order_id=:id', array(':id' => $id));

		$bankAccount = BankAccount::model()->findAll();

		if(isset($_POST['Order'])){
			if($_POST['Order']['status'] == Order::STATUS_APPROVED)
				$_POST['Order']['approved_date'] = date('Y-m-d h:i:s');
			elseif($_POST['Order']['status'] == Order::STATUS_APPROVED){
				$_POST['Order']['estimated_delivery_time'] = date('Y-m-d h:i:s');
				$_POST['Order']['actual_delivery_time'] = date('Y-m-d h:i:s');
			}
			$model->setAttributes($_POST['Order']);
			if($model->save()){
				Yii::app()->user->setFlash('update-success', Yii::t('app', 'Updated successfully'));
				if($_POST['Order']['status'] == Order::STATUS_APPROVED){
					$params              = array('model' => $model, 'bankAccount' => $bankAccount);
					$message            = new YiiMailMessage;
					$message->view = "approved";
			        $message->subject    = Yii::app()->name. ' - '. Order::MAIL_SUBJECT_APPROVED;
			        $message->setBody($params, 'text/html');                
			        $message->addTo($model->email);
			        $message->from = Yii::app()->params['order']['checkout_mail_address'];   

			        if(Yii::app()->mail->send($message)) {
		            	echo "Mailer sent";
			        }else {
			            echo "Message error!";
			        }
			    }
			}
		}

		if (!Yii::app()->getRequest()->getIsAjaxRequest())
			$this->render('update', array('model' => $model, 'orderProducts' => $orderProducts));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Order')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdmin() {
		$model = new Order('search');
		$model->unsetAttributes();

		if (isset($_GET['Order']))
			$model->setAttributes($_GET['Order']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
}