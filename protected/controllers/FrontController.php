<?php

class FrontController extends GxController
{

	public $meta_tag = array();
	public $banner;
	public $isMobile = false;

	public function init()
	{

        if (Yii::app()->detectMobileBrowser->showMobile) 
            $this->isMobile = true;
    }

	protected function beforeAction($action)
	{
		// set language	
		if(!empty(Yii::app()->session['language']))
			Yii::app()->setLanguage(Yii::app()->session['language']);
		else
			Yii::app()->setLanguage('vi');
		
		Yii::app()->theme = 'menstyle';

		// set banner
		$this->banner = Yii::app()->db->createCommand()
		    ->select('id, name, url, position, description')
		    ->from('banner')
		    ->where('position='. Banner::POS_1)
		   	->queryAll();

		// setting information
		if( empty(Yii::app()->session['setting'] ) )
		{
			$setting = Yii::app()->db->createCommand()
			    ->select('*')
			    ->from('setting')
			    ->queryRow();
			Yii::app()->session['setting'] = !empty($setting) ? $setting : array();	
		}

		$this->meta_tag['url'] = Yii::app()->createAbsoluteUrl(Yii::app()->request->url);
		$this->meta_tag['type'] = 'website';
		$this->meta_tag = array_merge($this->meta_tag, Yii::app()->session['setting']);

		if(!empty($this->meta_tag['logo']))
		{
			$w = 150;
			$h = 20;	// for desktop version
			if($this->isMobile)
				$h = 50;
			
			$this->meta_tag['image'] = Yii::app()->createAbsoluteUrl('/'). 
				ShoppyHelper::getThumbnailPath(Yii::app()->params['general']['logo_path']. DIRECTORY_SEPARATOR. $this->meta_tag['logo'], $w, $h, array('zc' => 2));
		}
		else
			$this->meta_tag['image'] = NULL;

		return true;
	}	

	protected function afterRender($view, &$output)
	{
		// set custom page title, url, image, upload logo
		if(!empty($this->category))
			$this->meta_tag['image'] = Yii::app()->createAbsoluteUrl('/'). 
				ShoppyHelper::getThumbnailPath(Yii::app()->params['category']['image_path']. DIRECTORY_SEPARATOR. $this->category['image'], 150, 150);
		else if(!empty($this->product))
			$this->meta_tag['image'] = Yii::app()->createAbsoluteUrl('/'). 
				$this->product->getActiveProductImage( $this->product->productImages, 150, 150, array('zc' => 2));
		else if(!empty($this->post))
			$this->meta_tag['image'] = Yii::app()->createAbsoluteUrl('/'). 
				$this->post->getPostImage(150, 150);

		if(!empty($this->meta_tag))
		{
			Yii::app ()->clientScript->registerMetaTag ( $this->meta_tag['meta_description'], 'description' );
			Yii::app ()->clientScript->registerMetaTag ( $this->meta_tag['meta_keyword'], 'keywords' );
			
			Yii::app ()->clientScript->registerMetaTag( $this->meta_tag['meta_title'], null, null, array('property'=>'og:title'));
			Yii::app ()->clientScript->registerMetaTag( $this->meta_tag['type'], null, null, array('property'=>'og:type'));
			Yii::app ()->clientScript->registerMetaTag( $this->meta_tag['url'], null, null, array('property'=>'og:url'));
			Yii::app ()->clientScript->registerMetaTag( $this->meta_tag['image'], null, null, array('property'=>'og:image'));
			Yii::app ()->clientScript->registerMetaTag( $this->meta_tag['meta_description'], null, null,array('property'=>'og:description'));	
		}

		
	}
	
}