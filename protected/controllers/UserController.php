<?php
Yii::import('application.helpers.ShoppyHelper');
Yii::import('application.controllers.AdminController');
class UserController extends AdminController 
{

	public function filters() {
		return array(
			'accessControl', 
		);
	}

	public function accessRules() {
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','create', 'update', 'delete', 'imageUpload', 'imageLoad', 'deleteImage', 'activeImage', 'register'),
				//'users'=>array('admin@mail.com', 'shop1@mail.com'),
				'roles' => array('admin', 'shop')
			),

			array('deny',  // deny all users
				'actions'=>array('*'),
                'users'=>array('*'),
			),
		);
	}

	public function actionCreate() 
	{
		$model = new User;

		if (isset($_POST['User'])) 
		{
			$model->setAttributes($_POST['User']);
			if ($model->save()) 
			{
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
			else
				Yii::app()->user->setFlash('update-unsuccess', Yii::t('app', 'updated unsuccessfully!'));
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'User');
		
		if (isset($_POST['User'])) {
			$model->setAttributes($_POST['User']);
			if ($model->save()) {
				Yii::app()->user->setFlash('update-success', Yii::t('app', 'updated successfully!'));
				$this->redirect(array('update', 'id' => $model->id));
			}
			else
				Yii::app()->user->setFlash('update-unsuccess', Yii::t('app', 'updated unsuccessfully!'));
		}

		$this->render('update', array(
				'model' => $model,
				));
		
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'User')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdmin() {
		$model = new User('search');
		$model->unsetAttributes();

		if (isset($_GET['User']))
			$model->setAttributes($_GET['User']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
}