<?php

class AdminController extends GxController
{
	public $meta_tag = array();
	
	protected function beforeAction($action)
	{
		Yii::app()->theme = 'admin';
		$this->layout = 'table';
		return true;
	}	
}