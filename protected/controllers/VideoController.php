<?php
Yii::import('application.controllers.AdminController');
class VideoController extends AdminController 
{

	public function filters() {
		return array(
			'accessControl', 
		);
	}

	public function accessRules() {
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','create', 'update', 'delete'),
				'roles' => array('admin', 'shop')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionCreate() 
	{
		$model = new Video;
		if (isset($_POST['Video'])) {
			$model->setAttributes($_POST['Video']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	
	public function actionUpdate($id) 
	{
		$model = $this->loadModel($id, 'Video');
		if (isset($_POST['Video'])) 
		{
			$model->setAttributes($_POST['Video']);
			if ($model->save()) 
				$this->redirect(array('admin'));
		}
		$this->render('update', array(
			'model' => $model,
		));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Video')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdmin() {
		$model = new Video('search');
		$model->unsetAttributes();

		if (isset($_GET['Video']))
			$model->setAttributes($_GET['Video']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
}