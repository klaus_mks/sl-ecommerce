<?php

Yii::import('application.controllers.AdminController');
class BankAccountController extends AdminController {


	/**
	 * @return array action filters
	 */
	public function filters()
	{

		return array(
			'accessControl', // perform access control for CRUD operations
			//array('ext.yiibooster.filters.BootstrapFilter - delete')
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','create', 'update', 'delete'),
				//'users'=>array('admin@mail.com', 'shop1@mail.com'),
				'roles' => array('admin', 'shop')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'BankAccount'),
		));
	}

	public function actionCreate() 
	{
		$model = new BankAccount;


		if (isset($_POST['BankAccount'])) 
		{
			if( empty( $_POST['BankAccount']['image']))
				unset( $_POST['BankAccount']['image']);

			$model->setAttributes($_POST['BankAccount']);

			// process image uploading
			$fileImage = CUploadedFile::getInstance($model, 'image');
            if($fileImage)
                $model->image = md5($fileImage->name . time()). "_". $fileImage->name . '.'. $fileImage->extensionName;

            $imagePath = YiiBase::getPathOfAlias('webroot'). Yii::app()->params['BankAccount']['image_path'] . DIRECTORY_SEPARATOR;
            if(!is_dir($imagePath))
                mkdir($imagePath, 0777, true);

			if ($model->save()) 
			{
				if ($fileImage) 
                    $fileImage->saveAs( $imagePath . $model->image);

				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) 
	{
		$model = $this->loadModel($id, 'BankAccount');
			
		if (isset($_POST['BankAccount'])) {
			$model->setAttributes($_POST['BankAccount']);

			if ($model->save()) {
				Yii::app()->user->setFlash('update-success', Yii::t('app', 'Updated successfully!'));
			}
			else
				Yii::app()->user->setFlash('update-usuccess', Yii::t('app', 'Updated unsuccessfully!'));
		}
		$this->render('update', array('model' => $model));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'BankAccount')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('BankAccount');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() 
	{
		$model = new BankAccount('shopSearch');
		$model->unsetAttributes();

		if (isset($_GET['BankAccount']))
			$model->setAttributes($_GET['BankAccount']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}