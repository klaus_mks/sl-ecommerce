<?php
Yii::import('application.helpers.ShoppyHelper');
Yii::import('application.controllers.FrontController');
class ListController extends FrontController
{
	public $category;
	public $product;
	public $post;
	public $offset;
	public $num;

	public function __construct($id, $module = null)
	{
		$this->num = Yii::app()->params['general']['item_per_page'];
		parent::__construct($id, $module);
	}

	public function listProduct($categoryId)
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('productImages');
		$criteria->condition = 't.category_id = :categoryId and t.is_published = 1';
		$criteria->params = array(':categoryId' => $categoryId);
		$criteria->limit = $this->num;

		// process product attribute filters
		$attribute = null;
		if(Yii::app()->request->isPostRequest && !Yii::app()->request->isAjaxRequest )
		{
			$attribute = Yii::app()->request->getPost('attribute');
			if(!empty($attribute))
				Yii::app()->session['attribute'] = $attribute;
			else
				unset(Yii::app()->session['attribute']);
		}

		if( !empty(Yii::app()->session['attribute'] ) )
		{
			$arrCondition = array();
			foreach( Yii::app()->session['attribute'] as $attrName => $attrValue )
				foreach( $attrValue as $av )
					$arrCondition[] = 'pa.value = "'. $av. '"';

			$criteria->join = 'INNER JOIN product_attribute pa ON pa.product_id = t.id';
			$criteria->condition .= ' AND ('. implode($arrCondition, ' OR '). ')';
		}

		// process ajax lazyload
		$this->offset = Yii::app()->request->getPost('offset', 0);
		$criteria->offset = $this->offset;
		$this->product = Product::model()->findAll( $criteria );
		if( Yii::app()->request->isAjaxRequest )
		{
			$res = array(
				'html' => $this->renderPartial('_product', array('product' => $this->product, 'newOffset' => $this->offset + $this->num ), true),
				'offset' => $this->offset + $this->num
			);

			echo json_encode( $res );
			Yii::app()->end();
		}
	}

	/**
    * @author 	Klaus
    * @since 	2014 - 08 - 17
    * @param 	@id: category id
    * list products in a category
    * 
    */
	public function actionListCategoryProduct()
	{
		$this->layout = 'product';
		$id = Yii::app()->request->getParam('id');
		$category = Category::model()->with(array('parent', 'parent.parent' => array('alias' => 'p2')))
			->findByPk( $id );

		$this->listProduct( $id );

		$this->category = $category;	// used for category widget

		$this->setPageTitle(Yii::app()->name. ' - '. $this->category->name);
		$this->render('product', array('product' => $this->product, 'category' => $category, 'offset' => $this->offset + $this->num ));
	}

	/**
    * @author 	Klaus
    * @since 	2014 - 08 - 17
    * list products in a random category
    * 
    */
	public function actionListRandomCategory()
	{
		$this->layout = 'product';
		$criteria = new CDbCriteria;
		$criteria->condition = 't.is_published = 1';
		// query category that has products only
		$criteria->join = 'INNER JOIN product on product.category_id = t.id';	
		$criteria->order = 'rand()';

		$this->category = Category::model()->with('parent')->find($criteria);	// used for category widget

		if(!$this->category)
			$this->render('product', array());
		
		$this->listProduct($this->category->id);
		$this->setPageTitle(Yii::app()->name. ' - '. $this->category->name);
		$this->render('product', array('product' => $this->product, 'category' => $this->category, 'offset' => $this->offset + $this->num ));

	}

	/**
    * @author 	Klaus
    * @since 	2014 - 08 - 17
    * list all posts ordered by created date
    *
    */
	public function actionListPost()
	{
		$this->layout = 'product';
		$criteria = new CDbCriteria;
		$criteria->condition = 't.is_published = 1';
		$criteria->order = 't.created_at desc';
		$criteria->limit = $this->num;

		// process ajax lazyload
		$this->offset = Yii::app()->request->getPost('offset', 0);
		$criteria->offset = $this->offset;

		$post = Post::model()->findAll($criteria);	
		
		// return ajax content for lazyload
		if( Yii::app()->request->isAjaxRequest )
		{
			$res = array(
				'html' => $this->renderPartial('_post', array('post' => $post, 'newOffset' => $this->offset + $this->num ), true),
				'offset' => $this->offset + $this->num
			);

			echo json_encode( $res );
			Yii::app()->end();
		}

		$this->setPageTitle(Yii::app()->name. ' - '. Yii::t('front', 'Articles'));
		$this->render('post', array('post' => $post, 'offset' => $this->offset + $this->num));
	}

	/**
    * @author 	Klaus
    * @since 	2014 - 08 - 17
    *
    */
	public function actionViewPost()
	{
		$this->layout = 'product';
		$id = Yii::app()->request->getParam('id');

		$criteria = new CDbCriteria;
		$criteria->condition = 't.is_published = 1 and t.id = :ID';
		$criteria->params = array(':ID' => $id);
		
		$this->post = Post::model()->find( $criteria );

		$this->setPageTitle($this->post->title);
		$this->render('post_view', array('post' => $this->post));
	}

	/**
	* @author 	Oanh Le
	* @since 	2014 - 06 - 22
	* view a product details
	*/
	public function actionViewProduct($id) 
	{
		Yii::app()->theme = 'menstyle';
		$this->layout = 'cart';

		$this->product = $model = $this->loadModel($id, 'Product');
		$categoryAttribute = Yii::app()->db->createCommand()
		   	->select('a.id, a.name')
		   	->from(' product p')
		   	->join('product_attribute pa', 'pa.product_id = p.id')
		   	->join('attribute a', 'pa.attribute_id = a.id')
		   	->join('category c', 'c.id = p.category_id')
		   	->where('p.category_id=:category_id', array(':category_id'=>$model->category_id))
		   	->group('a.name')
		   	->queryAll();
		
		$criteria = new CDbCriteria;
		$criteria->condition = 't.category_id = :ID and t.id <> :currentID';
		$criteria->order = 'rand()';
		$criteria->limit = 4;
		$criteria->params = array(':ID' => $this->product->category_id, ':currentID' => $this->product->id );

		$randomProduct = Product::model()->findAll($criteria);

		$this->setPageTitle($model->name);							   
		$this->render('product_detail', compact('model', 'attribute', 'categoryAttribute', 'randomProduct'));
	}


}