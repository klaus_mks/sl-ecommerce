<?php

Yii::import('application.helpers.ShoppyHelper');
Yii::import('application.controllers.FrontController');
class SiteController extends FrontController
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('contact'),
                'roles'=>array('shop'),
            ),
            array('allow',
                'actions'=>array('contact', 'forgotPassword', 'captcha'),
                'users' =>array('*')
            ),
            array('deny',
                'actions'=>array('contact'),
                'users'=>array('*'),
            ),
        );
    }

    public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CaptchaExtendedAction',
                // if needed, modify settings
                'mode'=>CaptchaExtendedAction::MODE_MATH,
            ),
        );
    }
    
    public function actionHome()
    {
    	$this->setPageTitle(Yii::app()->name);
    	$this->layout = 'home';	

		$newArrival = Product::model()->with('productImages')->findAll(array(
			'condition' => 't.is_published=1 and t.is_new=1',
			'order' => 't.modified_at desc'
		));
		
		$bestSeller = Product::model()->with('productImages')->findAll(array(
			'condition' => 't.is_published=1 and t.is_on_sale=1',
			'order' => 't.modified_at desc'
		));
			
		$banner = Banner::model()->findAll('position='. Banner::POS_2);
		$promoCategory = Category::model()->with('products')->findAll('t.is_published=1 and t.is_promoted=1 and products.is_promoted=1 and products.is_published=1');
		$news = Post::model()->findAll('t.is_published = 1 and t.is_on_homepage = 1');
    	$this->render('home', compact('newArrival', 'bestSeller', 'banner', 'promoCategory', 'news'));
    }

    public function actionForgotPassword()
    {
        $this->layout = 'register'; 
        $model = new User;
        $model->scenario = 'forgotPassword';

        if(!empty($_POST))
        {
            $model->setAttributes($_POST['User']);

            if($model->validate())
            {
                $criteria = new CDbCriteria;
                $criteria->condition = 't.email = :email';
                $criteria->params = array(':email' => $_POST['User']['email']);
                

                $model = User::model()->find($criteria);
                $newPassword = $model->generateNewPassword();
                if($newPassword)
                {
                    Yii::app()->user->setFlash('success', Yii::t('front', 'An email has been sent to you. Please check your email'));

                    $params = array('user' => $model, 'password' => $newPassword);
                    $message = new YiiMailMessage;
                    $message->view = "got_new_password";
                    $message->subject    = Yii::app()->name . ' - '. User::MAIL_SUBJECT_GOT_NEW_PASSWORD;
                    $message->setBody($params, 'text/html');                
                    $message->addTo($model->email);
                    $message->setFrom(Yii::app()->params['account']['account_mail_address']);

                    Yii::app()->mail->send($message);
                }
                $this->redirect(array('site/forgotPassword'));
            }
        }

        $this->render('forgot_password', compact('model'));
    }


    public function actionChangePassword()
    {
        $user = User::model()->findByPk(1);
        $user->scenario = 'updatePassword';
        $user->password = 'zdravei90';
        $user->save(false);        
    }

    public function actionChangeLanguage($l = NULL)
    {
    	if(in_array($l, array('en', 'vi')))
    		Yii::app()->session['language'] = $l;
		$this->redirect(Yii::app()->request->urlReferrer);
    }

    public function actionTest()
    {

    	$auth=Yii::app()->authManager;
  		$user = $auth->createRole('user');
  		exit;

    	$message            = new YiiMailMessage;
       
        //this points to the file test.php inside the view path
        $message->view = "testemail";
        $sid                 = 10;
        $criteria            = new CDbCriteria();
        $criteria->condition = "id=".$sid."";            
        $product          	 = Product::model()->findByPk($sid);        
        $params              = array('product'=>$product);

        $message->subject    = 'My TestSubject';
        $message->setBody($params, 'text/html');                
        $message->addTo('bondvodkamartini@gmail.com');
        $message->from = 'admin@shoppy.com';   
        Yii::app()->mail->send($message);     

    	exit;
    	$pLink = Yii::app()->createUrl('list/listCategoryProduct', array('id' => 27, 'title' => ShoppyHelper::getSlug('sản phẩm thời trang')));
    	$dLink = Yii::app()->createUrl('list/viewProductDetail', array('id' => 27, 'title' => ShoppyHelper::getSlug('son dưỡng môi Vera')));
    	var_dump($pLink);
    	echo '<br/>';
    	var_dump($dLink);
    }

	/**
	 * Declares class-based actions.
	 */
	// public function actions()
	// {
	// 	return array(
	// 		// captcha action renders the CAPTCHA image displayed on the contact page
	// 		'captcha'=>array(
	// 			'class'=>'CCaptchaAction',
	// 			'backColor'=>0xFFFFFF,
	// 		),
	// 		// page action renders "static" pages stored under 'protected/views/site/pages'
	// 		// They can be accessed via: index.php?r=site/page&view=FileName
	// 		'page'=>array(
	// 			'class'=>'CViewAction',
	// 		),
	// 	);
	// }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->layout = 'table';
		Yii::app()->theme = 'admin';

		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	public function actionInfo()
	{
		
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$this->layout = 'cart';
		Yii::app()->theme = 'menstyle';
		
		$setting = $this->loadModel('1', 'Setting');
		$bankAccount = BankAccount::model()->findAll();

		$this->setPageTitle(Yii::t('front', 'Contact'));
		$this->render('contact', array('setting' => $setting, 'bankAccount' => $bankAccount ));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout = 'login';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	public function actionRegister()
    {
    	$this->setPageTitle(Yii::t('front', 'Register'));
    	$this->layout = 'register';	
    	$model = new User;
    	if(!empty($_POST)){
    	
    		$model->setAttributes($_POST['User']);
    		$model->setAttribute('is_activated', true);
    		$linkActive = 'http://'.$_SERVER['HTTP_HOST'].'/active?user='.$_POST['User']['email'].'&pass='.crypt($_POST['User']['password'], User::generateSalt());
    		$dataMail = array(
				'name' => '',
				'link' => 'http://'.$_SERVER['HTTP_HOST'].'/',
				'linkActive' => $linkActive,
    		);
    		if($model->save()){
    			Yii::app()->user->setFlash('success', Yii::t('front', 'Your account has been successfully created'));
    			$params              = array('dataMail'=> $dataMail, 'user' => $model, 'password' => $_POST['User']['password']);
				$message            = new YiiMailMessage;
				$message->view = "register";
		        $message->subject    = Yii::app()->name . ' - '. User::MAIL_SUBJECT_REGISTER;
		        $message->setBody($params, 'text/html');                
		        $message->addTo($model->email);
		        $message->setFrom(Yii::app()->params['account']['account_mail_address']);

		        Yii::app()->mail->send($message);

		        // force login and return back to checkout page
		        // if registered from checkout page
		        if(!empty(Yii::app()->session['register_return']))
		        {
		        	$returnUrl = Yii::app()->session['register_return'];
		        	unset(Yii::app()->session['register_return']);

		        	// force user to login
		        	$identity = new UserIdentity($model['email'], $_POST['User']['password']);
		        	$identity->authenticate();
		        	if($identity->errorCode === UserIdentity::ERROR_NONE)
						Yii::app()->user->login($identity, 0);						

		        	$this->redirect($returnUrl);
		        }
		        else
		        	$this->redirect(array('site/register', 'act' => 'thank'));
    		}
    	}
    	
    	if(isset($_GET['act']) && $_GET['act'] == 'thank')
    		$this->render('register_thank');	
    	else
    	$this->render('register', array( 'model' => $model));
    }

    public function actionUpdateInfo()
    {
    	$this->setPageTitle(Yii::t('front', 'Update Account'));

    	if(Yii::app()->user->isGuest)
    		$this->redirect('site/home');

    	$this->layout = 'register';	
    	$model = User::model()->findByPk(Yii::app()->user->id);
    	$model->scenario = 'updateInfo';

    	if(!empty($_POST))
    	{
    		if(!empty($_POST['updatePassword']))	
    			$model->scenario = 'updatePassword';

    		$model->setAttributes($_POST['User']);

    		if($model->save())
    		{
    			Yii::app()->user->setFlash('success', Yii::t('front', 'Your account has been successfully updated'));
    			$this->redirect(array('site/updateInfo'));
    		}
    	}
    	
		$this->render('update_info', array( 'model' => $model));
    }


    public function actionActive()
    {
    	$this->layout = 'register';	
    	$this->render('active');
    }

    public function actionMaintenance()
    {
    	$this->layout = false;
    	$this->render('maintenance');
    }

    public function actionHowToBuy()
    {
    	$this->layout = 'cart';
    	$this->setPageTitle(Yii::app()->name. ' - '. Yii::t('front', 'How to buy'));
    	$this->render('how_to_buy');
    }

    public function actionMyOrder(){
    	$this->layout = 'checkout';
    	$order = Order::model()->getOrderByUserId(Yii::app()->user->id);
    	$this->render('my_order', array('order' => $order));
    }

}