<?php
Yii::import('application.helpers.ShoppyHelper');
Yii::import('application.helpers.CartHelper');
Yii::import('application.controllers.AdminController');

class ProductController extends AdminController 
{
	public $product;
	public $isMobile = false;
	public function init()
	{
        if (Yii::app()->detectMobileBrowser->showMobile) 
            $this->isMobile = true;
    }
    
	public function filters() {
		return array(
			'accessControl', 
		);
	}

	public function accessRules() {
		return array(
			array('allow',  // allow all users 
				'actions'=>array('addToCart', 'removeFromCart', 'changeQuantity', 'viewCart', 'view', 'getProductSelection'),
				'users'=>array('*'),
			),
			array('allow',  
				'actions'=>array('admin','create', 'update', 'delete', 
					'imageUpload', 'imageLoad', 'deleteImage', 'activeImage'
				),
				//'users'=>array('admin@mail.com', 'shop1@mail.com'),
				'roles' => array('admin', 'shop')
			),

			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 23
	* get product info and show on "add to cart" modal
	*/
	public function actionGetProductSelection()
	{
		Yii::app()->setLanguage('vi');	// temporary set it here
		if( Yii::app()->request->isAjaxRequest )
		{
			Yii::app()->theme = 'menstyle';
			$id = Yii::app()->request->getPost('id');	// product id
			$product = Product::model()->with('attributeValues')->findByPk($id);

			$attrCriteria = new CDbCriteria();
			$attrCriteria->distinct = true;
			$attrCriteria->join = 'INNER JOIN product_attribute pa ON pa.attribute_id = t.id';
			$attrCriteria->condition = 'pa.product_id=:id';
			$attrCriteria->params = array(':id'=> $id);		

			$attributes = Attribute::model()->findAll($attrCriteria);
			$this->renderPartial("_productSelection", compact('product', 'attributes'));	
		}
	}


	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 12
	*/
	public function actionViewCart()
	{
		// set language
		if(!empty(Yii::app()->session['language']))
			Yii::app()->setLanguage(Yii::app()->session['language']);
		else
			Yii::app()->setLanguage('vi');

		Yii::app()->theme = 'menstyle';
		$this->layout = 'cart';
		$this->setPageTitle(Yii::t('front', 'Shopping cart'));

		////////////////// setting information
		if( empty(Yii::app()->session['setting'] ) )
		{
			$setting = Yii::app()->db->createCommand()
			    ->select('*')
			    ->from('setting')
			    ->queryRow();
			Yii::app()->session['setting'] = $setting;	
		}

		$this->meta_tag['url'] = Yii::app()->createAbsoluteUrl(Yii::app()->request->url);
		$this->meta_tag['type'] = 'website';
		$this->meta_tag = array_merge($this->meta_tag, Yii::app()->session['setting']);

		if(!empty($this->meta_tag['logo']))
		{
			$w = 150;
			$h = 20;	// for desktop version
			if($this->isMobile)
				$h = 50;
			
			$this->meta_tag['image'] = Yii::app()->createAbsoluteUrl('/'). 
				ShoppyHelper::getThumbnailPath(Yii::app()->params['general']['logo_path']. DIRECTORY_SEPARATOR. $this->meta_tag['logo'], $w, $h, array('zc' => 2));
		}
		else
			$this->meta_tag['image'] = NULL;

		///////////////////////////
		$this->_viewCart();
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 26
	*/
	private function _viewCart()
	{
		$cartSession = !empty(Yii::app()->session['cart']) ? Yii::app()->session['cart'] : array();
		$ids = CartHelper::getCartIdList( $cartSession );
		$attrIds = CartHelper::getAttributeIdList( $cartSession );
		$prodAttrIds = CartHelper::getProductAttributeIdList( $cartSession );

		$criteria = new CDbCriteria;
		$attrCriteria = new CDbCriteria;
		$prodAttrCriteria = new CDbCriteria;
		
		$criteria->with = array('productImages');
		$criteria->condition = 't.is_published = 1';
		$criteria->addInCondition('t.id', $ids );
		
		$attrCriteria->addInCondition('t.id', $attrIds);
		$prodAttrCriteria->addInCondition('t.id', $prodAttrIds);

		$product = Product::model()->findAll( $criteria );
		$attribute = Attribute::model()->findAll($attrCriteria);
		$attributeValue = ProductAttribute::model()->findAll($prodAttrCriteria);

		$view = 'view_cart';		
		if (Yii::app()->detectMobileBrowser->showMobile) 
			$view = 'm_view_cart';

		if( Yii::app()->request->isAjaxRequest)
		{
			$this->renderPartial($view, compact('product', 'cartSession', 'attribute', 'attributeValue'));
			Yii::app()->end();
		}

		$this->render($view, compact('product', 'cartSession', 'attribute', 'attributeValue'));
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 26
	*/	
	public function actionChangeQuantity()
	{
		$newQuantity = Yii::app()->request->getPost('quantity');
		$cartSession = !empty(Yii::app()->session['cart']) ? Yii::app()->session['cart'] : array();
		CartHelper::changeQuantity( $newQuantity, $cartSession );
		Yii::app()->session['cart'] = $cartSession;

		echo json_encode( array('res' => true));

	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 12
	*/
	public function actionAddToCart()
	{
		if( Yii::app()->request->isAjaxRequest )
		{
			$id = Yii::app()->request->getPost('id');		// product id
			$name = Yii::app()->request->getPost('name');
			$price = Yii::app()->request->getPost('price');
			$quantity = Yii::app()->request->getPost('quantity');

			$attribute = Yii::app()->request->getPost('attribute');
			$attributeKey = $attribute ?  array_keys( $attribute ) : NULL;
			$attributeValue = $attribute ? array_values( $attribute ) : NULL;

			$cartData = !empty(Yii::app()->session['cart']) ? Yii::app()->session['cart'] : array();
			$cartTotal = !empty(Yii::app()->session['cart-total']) ? Yii::app()->session['cart-total'] : array();

			$item = array('id' => $id, 'name' => $name, 'quantity' => $quantity, 'price' => $price,
				'attribute' => $attributeKey, 'attribute_value' => $attributeValue
			);

			if(empty($cartData) || !CartHelper::isInCart($item, $cartData))
			{
				$cartData[] = $item;
				$cartTotal = CartHelper::getTotalCartItem( $cartData );
			}
			else
			{
				$index = CartHelper::getItemIndex( $item, $cartData );
				if($index)
					$cartData[$index] = $item;
			}

			Yii::app()->session['cart'] = $cartData;
			Yii::app()->session['cart-total'] = $cartTotal;

			echo json_encode( array('total' => Yii::app()->session['cart-total']) );
			Yii::app()->end();
		}	
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 12
	*/
	public function actionRemoveFromCart()
	{
		if( Yii::app()->request->isAjaxRequest )
		{
			$id = Yii::app()->request->getPost('id');
			$attribute = Yii::app()->request->getPost('attribute');
			$attributeKey = $attribute ?  array_keys( $attribute ) : NULL;
			$attributeValue = $attribute ? array_values( $attribute ) : NULL;

			$item = array('id' => $id, 'attribute' => $attributeKey, 'attribute_value' => $attributeValue);
					
			$cartData = !empty(Yii::app()->session['cart']) ? Yii::app()->session['cart'] : array();
			$cartTotal = !empty(Yii::app()->session['cart-total']) ? Yii::app()->session['cart-total'] : array();
			

			if(empty($cartData) )
				Yii::app()->end();
			else
			{
				$index = CartHelper::getItemIndex( $item, $cartData );
				unset($cartData[$index]);
				$cartTotal = CartHelper::getTotalCartItem( $cartData );
			}

			Yii::app()->session['cart'] = $cartData;
			Yii::app()->session['cart-total'] = $cartTotal;

			echo json_encode( array(
				'total' => Yii::app()->session['cart-total'],
				'url' => Yii::app()->createUrl('product/viewCart')
			));
			Yii::app()->end();
		}	
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 06 - 23
	*/
	public function actionDeleteImage($id)
	{
		if( Yii::app()->request->isAjaxRequest )
		{
			$productImage = ProductImage::model()->findByPk( $id );
			if(empty( $productImage))
				return false;	
			$imgFolder = YiiBase::getPathOfAlias('webroot'). DIRECTORY_SEPARATOR. Yii::app()->params['product']['image_path']. DIRECTORY_SEPARATOR;
			$filePath = $imgFolder. $productImage->name;
			
			if($productImage->delete())
				@unlink($filePath);

		}	
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 06 - 24
	* @param 	$id : image id
	*/
	public function actionActiveImage($id)
	{
		if( Yii::app()->request->isAjaxRequest )
		{
			$active = ProductImage::model()->makeActive( $id );
		}	
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 06 - 22
	*/
	public function actionImageLoad()
	{
		if( Yii::app()->request->isAjaxRequest )
		{
			$id = Yii::app()->request->getPost('id');
			$image = Yii::app()->db->createCommand()
			    ->select('id, product_id, name, url, is_actived')
			    ->from('product_image')
			    ->where('product_id=:id', array(':id' => $id))
			   	->queryAll();
				
			$this->renderPartial("_imageList", compact('image'));	

		}
		
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 06 - 22
	*/
	public function actionImageUpload()
	{
		$ds          = DIRECTORY_SEPARATOR;  //1
 		$productId = Yii::app()->request->getPost('id');

		$storeFolder = YiiBase::getPathOfAlias('webroot'). Yii::app()->params['product']['image_path'];   //2
		if (!empty($_FILES)) 
		{
		    $tempFile = $_FILES['file']['tmp_name'];          //3             
		    $targetPath = $storeFolder . $ds;  //4
		
			if(!is_dir($targetPath))
                mkdir($targetPath, 0777, true);

		    $fileName = md5($_FILES['file']['name'] . time()). "_". $_FILES['file']['name'];
		    $targetFile =  $targetPath. $fileName;  //5

		    $image = new ProductImage();
		    $image->name = $fileName;
		    $image->product_id = $productId;
		    $image->is_published = 1;

		    if( $image->save())
		    	move_uploaded_file($tempFile,$targetFile); //6	
		    else
		    	var_dump($image->errors );

		}


	}

	public function actionCreate() 
	{
		$model = new Product;
		$pAttribute = new ProductAttribute;

		if (isset($_POST['Product'])) 
		{
			$model->setAttributes($_POST['Product']);
			$transaction = $model->dbConnection->beginTransaction();

			if( $model->save())
			{
				foreach( $_POST['ProductAttribute']['attribute_id'] as $i => $attribute_id )
				{
					if(empty($attribute_id) || empty($_POST['ProductAttribute']['value'][$i]))
						continue;

					$pAttribute = new ProductAttribute;
					$pAttribute->setAttributes( array(
						'product_id' => $model->id,
						'attribute_id' => $attribute_id,
						'value' => $_POST['ProductAttribute']['value'][$i]
					));

					if(!$pAttribute->save())
					{
						$transaction->rollback();
						break;
					}

				}
				$transaction->commit();

				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model, 'pAttribute' => $pAttribute));
	}

	public function actionUpdate($id) 
	{
		$model = $this->loadModel($id, 'Product');
		$pAttribute = new ProductAttribute;
		$pAttributeData = ProductAttribute::model()->findAll('product_id=:productID', array(':productID' => $id));
		
		if (isset($_POST['Product'])) 
		{
			$model->setAttributes($_POST['Product']);
			$transaction = $model->dbConnection->beginTransaction();
			if ($model->save()) 
			{
				if(!empty($_POST['ProductAttribute']))
				{
					ProductAttribute::model()->deleteAll('product_id=:productID', array(':productID' => $id));
					foreach( $_POST['ProductAttribute']['attribute_id'] as $i => $attribute_id )
					{
						if(empty($attribute_id) || empty($_POST['ProductAttribute']['value'][$i]))
							continue;

						$pAttribute = new ProductAttribute;
						$pAttribute->setAttributes( array(
							'product_id' => $model->id,
							'attribute_id' => $attribute_id,
							'value' => $_POST['ProductAttribute']['value'][$i]
						));

						if(!$pAttribute->save())
						{
							$transaction->rollback();
							break;
						}

					}	
				}
				
				$transaction->commit();
				Yii::app()->user->setFlash('update-success', Yii::t('app', 'updated successfully!'));
				$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('update', array(
			'model' => $model, 'pAttribute' => $pAttribute, 'pAttributeData' => $pAttributeData
		));
		
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Product')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Product');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Product('search');
		$model->unsetAttributes();

		if (isset($_GET['Product']))
			$model->setAttributes($_GET['Product']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	protected function gridCategoryName($data,$row)
    {
    	$cat= Category::model()->findByPk($data->category_id);
    	return $data->category_id==null ? null : $cat->name; 
    }


}