<?php
Yii::import('application.helpers.ShoppyHelper');
Yii::import('application.controllers.AdminController');
class SettingController extends AdminController {

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('update'),
				'roles' => array('admin', 'shop')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Setting'),
		));
	}

	
	public function actionUpdate($id) 
	{
		if(Setting::model()->findByPk($id) == NULL)
			Setting::model()->generateData();
		$model = $this->loadModel($id, 'Setting');
			
		if (isset($_POST['Setting'])) 
		{	
			if( empty( $_POST['Setting']['logo']))
				unset( $_POST['Setting']['logo']);
			
			$model->setAttributes($_POST['Setting']);

			// process image uploading
			$fileImage = CUploadedFile::getInstance($model, 'logo');
            if($fileImage)
                $model->logo = md5($fileImage->name . time()). "_". $fileImage->name . '.'. $fileImage->extensionName;

            $imagePath = YiiBase::getPathOfAlias('webroot'). Yii::app()->params['general']['logo_path'] . DIRECTORY_SEPARATOR;
            if(!is_dir($imagePath))
                mkdir($imagePath, 0777, true);

			if ($model->save()) 
			{
				if ($fileImage) 
                    $fileImage->saveAs( $imagePath . $model->logo);

				Yii::app()->user->setFlash('update-success', Yii::t('app', 'Updated successfully!'));
			}
			else
				Yii::app()->user->setFlash('update-usuccess', Yii::t('app', 'Updated unsuccessfully!'));
		}
		
		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 05
	*/
	public function actionBanner()
	{
		$model = Banner::model()->findAll();
		$position = Banner::model()->getPosition();
		$this->render('banner', compact('model', 'position'));
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 04
	*/
	public function actionDeleteBanner($id)
	{
		if( Yii::app()->request->isAjaxRequest )
		{
			$banner = Banner::model()->findByPk( $id );
			if(empty( $banner))
				return false;	
			$imgFolder = YiiBase::getPathOfAlias('webroot'). DIRECTORY_SEPARATOR. Yii::app()->params['general']['banner_path']. DIRECTORY_SEPARATOR;
			$filePath = $imgFolder. $banner->name;
			
			if($banner->delete())
				@unlink($filePath);
		}	
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 04
	*/
	public function actionUpdateBanner()
	{
		if( Yii::app()->request->isAjaxRequest )
		{
			$this->layout = false;
			$banner = $this->loadModel(Yii::app()->request->getPost('id'), 'Banner');
			$banner->url = Yii::app()->request->getPost('url');
			$banner->position = Yii::app()->request->getPost('position');
			if($banner->save())
				echo json_encode(array('success' => true ));	
			else
				var_dump( $banner->errors);
			Yii::app()->end();
		}
		
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 04
	*/
	public function actionBannerLoad()
	{
		if( Yii::app()->request->isAjaxRequest )
		{
			$upperBanner = Yii::app()->db->createCommand()
			    ->select('id, name, url, position, description')
			    ->from('banner')
			    ->where('position='. Banner::POS_1)
			   	->queryAll();
			
			$lowerBanner = Yii::app()->db->createCommand()
			    ->select('id, name, url, position, description')
			    ->from('banner')
			    ->where('position='. Banner::POS_2)
			   	->queryAll();

			$this->renderPartial("_imageList", compact('upperBanner', 'lowerBanner'));	
		}
	}

	/**
	* @author 	Klaus
	* @since 	2014 - 07 - 04
	*/
	public function actionBannerUpload()
	{
		$ds          = DIRECTORY_SEPARATOR;  //1

		$storeFolder = YiiBase::getPathOfAlias('webroot'). Yii::app()->params['general']['banner_path'];   //2
		if (!empty($_FILES)) 
		{
		    $tempFile = $_FILES['file']['tmp_name'];          //3             
		    $targetPath = $storeFolder . $ds;  //4
		    
		    if(!is_dir($targetPath))
                mkdir($targetPath, 0777, true);

		    $fileName = md5($_FILES['file']['name'] . time()). "_". $_FILES['file']['name'];
		    $targetFile =  $targetPath. $fileName;  //5

		    $image = new Banner();
		    $image->name = $fileName;
		    $image->position = Banner::POS_2;

		    if( $image->save())
		    	move_uploaded_file($tempFile,$targetFile); //6	
		    else
		    	var_dump($image->errors );
		}
	}

}