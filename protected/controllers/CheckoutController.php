<?php
Yii::import('application.helpers.ShoppyHelper');
Yii::import('application.helpers.CartHelper');
Yii::import('application.controllers.FrontController');
class CheckoutController extends FrontController
{
	public function __construct($id, $module = null)
	{
		$this->layout = 'checkout';
		parent::__construct($id, $module = null);
	}
	public function actionCheckout()
	{
		if (Yii::app()->getRequest()->getIsPostRequest()) 
		{
			$submitQuantity = Yii::app()->request->getPost('quantity');
			if( $submitQuantity )
			{
				$cartData = Yii::app()->session['cart'];
				foreach( $cartData as $productId => $d )
					$cartData[$productId]['quantity'] = $submitQuantity[$productId];
			}

			$method = Yii::app()->request->getPost('checkout_method');
			if( $method && $method == 'register')
			{
				Yii::app()->session['register_return'] = Yii::app()->createUrl('checkout/info');
				$this->redirect('site/register');
			}
			else if( $method && $method == 'guest')
				$this->redirect('checkout/info');
		}
		
		$model = new LoginForm;
		if(!Yii::app()->user->isGuest)
			$this->redirect('checkout/info');

		$this->render('checkout', array('model' => $model));
	}

	public function actionInfo()
	{
		$model = new CheckoutForm;

		// populate default values
		$model->getLoginInfo();

		// collect user input data
		if(isset($_POST['CheckoutForm']))
		{
			$model->attributes=$_POST['CheckoutForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate())
			{
				Yii::app()->session['cart-info'] = $_POST['CheckoutForm'];
				$this->redirect(Yii::app()->createUrl('checkout/finish'));
			}
		}

		$this->render('info', array('model' => $model));
	}

	public function actionFinish()
	{
		$cartSession = !empty(Yii::app()->session['cart']) ? Yii::app()->session['cart'] : array();
		$ids = CartHelper::getCartIdList( $cartSession );
		$attrIds = CartHelper::getAttributeIdList( $cartSession );
		$prodAttrIds = CartHelper::getProductAttributeIdList( $cartSession );

		$criteria = new CDbCriteria;
		$attrCriteria = new CDbCriteria;
		$prodAttrCriteria = new CDbCriteria;
		
		$criteria->with = array('productImages');
		$criteria->condition = 't.is_published = 1';
		$criteria->addInCondition('t.id', $ids );
		
		$attrCriteria->addInCondition('t.id', $attrIds);
		$prodAttrCriteria->addInCondition('t.id', $prodAttrIds);

		$product = Product::model()->findAll( $criteria );
		$attribute = Attribute::model()->findAll($attrCriteria);
		$attributeValue = ProductAttribute::model()->findAll($prodAttrCriteria);

		$bankAccount = BankAccount::model()->findAll();

		if (Yii::app()->getRequest()->getIsPostRequest()) 
		{
			$cartInfo = Yii::app()->session['cart-info'];
			$cartInfo['code'] = Order::model()->generateOrderCode();
			$cartInfo['order_date'] = new CDbExpression('NOW()');
			$cartInfo['status'] = Order::STATUS_NEW;
			$cartInfo['user_id'] = Yii::app()->user->id ? Yii::app()->user->id : NULL;

			$order = new Order;
			$order->setAttributes( $cartInfo);

			$transaction = $order->dbConnection->beginTransaction();
			try 
			{
				if($order->save())
				{
					$price = 0;
					foreach( $cartSession as $c )
					{
						$orderProduct = new OrderProduct;
						$orderProduct->product_id = $c['id'];
						$orderProduct->order_id = $order->id;
						$orderProduct->price = $c['price'];
						$orderProduct->quantity = $c['quantity'];
						$orderProduct->total_price = $c['price'] * $c['quantity'];

						$price += $orderProduct->total_price;

						if(!$orderProduct->save())
						{
							throw new Exception(json_encode($orderProduct->getErrors()));
						}

						if(!empty($c['attribute_value']) && is_array($c['attribute_value']))
						{
							foreach( $c['attribute_value'] as $av )
							{
								$orderAttribute = new OrderAttribute;
								$orderAttribute->order_product_id = $orderProduct->id;
								$orderAttribute->product_attribute_id = $av;
								if(!$orderAttribute->save())
								{
									throw new Exception(json_encode($orderAttribute->getErrors()));
								}
							}
						}
					}

					$order->total = $price;
					$order->save();
				}
				
				// commit transaction
				$transaction->commit();

				// send notice email to shop owner
				$shopMessage            	= new YiiMailMessage;
		        $shopMessage->view 			= "checkout_owner";
		        
		        $params              		= compact('cartSession', 'order', 'product', 'attribute', 'attributeValue', 'bankAccount');
		        $shopMessage->subject    	= Yii::app()->name. ' - '. Order::MAIL_OWNER_SUBJECT_CHECKOUT;
		        $shopMessage->setBody($params, 'text/html');                
		        $shopMessage->setTo(Yii::app()->params['order']['owner_email']);
		        $shopMessage->setFrom(array(Yii::app()->params['order']['checkout_mail_address'] => 'AnaStores'));   
		        Yii::app()->mail->send($shopMessage);   	

				// send notice email to customer
				$message            	= new YiiMailMessage;
		        $message->view 			= "checkout";
		        
		        $params              	= compact('cartSession', 'order', 'product', 'attribute', 'attributeValue', 'bankAccount');
		        $message->subject    	= Yii::app()->name. ' - '. Order::MAIL_SUBJECT_CHECKOUT;
		        $message->setBody($params, 'text/html');                
		        $message->setTo(trim($order->email));

		        $message->setFrom(array(Yii::app()->params['order']['checkout_mail_address'] => 'AnaStores'));   
		        Yii::app()->mail->send($message);   	

		        // clear session
		        unset(Yii::app()->session['cart']);
				unset(Yii::app()->session['cart-info']);
				unset(Yii::app()->session['cart-total']);
				return $this->render('thank', compact('order', 'bankAccount'));

			} 
			catch (Exception $e) 
			{
				$transaction->rollback();
			}

		}

		$view = 'finish';		
		if (Yii::app()->detectMobileBrowser->showMobile) 
			$view = 'm_finish';

		$this->render($view, compact('product', 'cartSession', 'attribute', 'attributeValue'));
	}

	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->createUrl('checkout/info'));
		}
		// display the login form
		$this->render('checkout',array('model'=>$model));
	}

}