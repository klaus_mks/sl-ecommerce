<?php
Yii::import('application.controllers.AdminController');
class AttributeController extends AdminController 
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{

		return array(
			'accessControl', // perform access control for CRUD operations
			//array('ext.yiibooster.filters.BootstrapFilter - delete')
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','create', 'update', 'delete'),
				'roles' => array('admin', 'shop')
			),

			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Attribute'),
		));
	}

	public function actionCreate() {
		$model = new Attribute;


		if (isset($_POST['Attribute'])) {
			$model->setAttributes($_POST['Attribute']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Attribute');


		if (isset($_POST['Attribute'])) {
			$model->setAttributes($_POST['Attribute']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Attribute')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Attribute');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Attribute('search');
		$model->unsetAttributes();

		if (isset($_GET['Attribute']))
			$model->setAttributes($_GET['Attribute']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}