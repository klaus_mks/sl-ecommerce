<?php

Yii::import('application.controllers.AdminController');
class CategoryController extends AdminController {
	/**
	 * @return array action filters
	 */
	public function filters()
	{

		return array(
			'accessControl', // perform access control for CRUD operations
			//array('ext.yiibooster.filters.BootstrapFilter - delete')
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','create', 'update', 'delete'),
				//'users'=>array('admin@mail.com', 'shop1@mail.com'),
				'roles' => array('admin', 'shop')
			),

			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Category'),
		));
	}

	public function actionCreate() 
	{
		$model = new Category;


		if (isset($_POST['Category'])) 
		{
			if( empty( $_POST['Category']['image']))
				unset( $_POST['Category']['image']);

			$model->setAttributes($_POST['Category']);

			// process image uploading
			$fileImage = CUploadedFile::getInstance($model, 'image');
            if($fileImage)
                $model->image = md5($fileImage->name . time()). "_". $fileImage->name . '.'. $fileImage->extensionName;

            $imagePath = YiiBase::getPathOfAlias('webroot'). Yii::app()->params['category']['image_path'] . DIRECTORY_SEPARATOR;
            if(!is_dir($imagePath))
                mkdir($imagePath, 0777, true);

			if ($model->save()) 
			{
				if ($fileImage) 
                    $fileImage->saveAs( $imagePath . $model->image);

				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) 
	{
		$model = $this->loadModel($id, 'Category');

		if (isset($_POST['Category'])) 
		{
			if( empty( $_POST['Category']['image']))
				unset( $_POST['Category']['image']);

			$model->setAttributes($_POST['Category']);

			// process image uploading
			$fileImage = CUploadedFile::getInstance($model, 'image');
            if($fileImage)
                $model->image = md5($fileImage->name . time()). "_". $fileImage->name . '.'. $fileImage->extensionName;

            $imagePath = YiiBase::getPathOfAlias('webroot'). Yii::app()->params['category']['image_path'] . DIRECTORY_SEPARATOR;
            if(!is_dir($imagePath))
                mkdir($imagePath, 0777, true);

			if ($model->save()) 
			{
				if ($fileImage) 
                    $fileImage->saveAs( $imagePath . $model->image);

				Yii::app()->user->setFlash('update-success', Yii::t('app', 'updated successfully!'));
				$this->redirect(array('update', 'id' => $model->id));
			}
		}
		$this->render('update', array(
			'model' => $model
		));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Category')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Category');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() 
	{
		$model = new Category('shopSearch');
		$model->unsetAttributes();

		if (isset($_GET['Category']))
			$model->setAttributes($_GET['Category']);
		$this->render('admin', array(
			'model' => $model
		));
	}
}