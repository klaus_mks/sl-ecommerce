<?php
Yii::import('application.helpers.ShoppyHelper');
Yii::import('application.controllers.AdminController');
class PostController extends AdminController {

	public function actions()
    {
        return array(
            'fileUpload'=>array(
                'class'=>'ext.redactor.actions.FileUpload',
                'uploadPath'=>'/images',
                'uploadUrl'=>'/post/fileUpload',
                //'uploadCreate'=>true,
                'permissions'=>0755,
            ),
            'imageUpload'=>array(
                'class'=>'ext.redactor.actions.ImageUpload',
                'uploadPath' => YiiBase::getPathOfAlias('webroot'). '/images',
                'uploadUrl'=> '/images',
                //'uploadCreate'=>true,
                'permissions'=>0755,
            ),
            'imageList'=>array(
                'class'=>'ext.redactor.actions.ImageList',
                'uploadPath'=>'/images',
                'uploadUrl'=>'/post/imageList',
            ),
        );
    }

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','create', 'update', 'delete', 'fileUpload', 'imageUpload', 'imageList'),
				//'users'=>array('admin@mail.com', 'shop1@mail.com'),
				'roles' => array('admin', 'shop')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//array('ext.yiibooster.filters.BootstrapFilter - delete')
		);
	}
	
	public function actionView($id) {	

		Yii::app()->setLanguage('vi');
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Post'),
		));
	}

	

	public function actionCreate()
	{
		$model=new Post;

		if(isset($_POST['Post']))
		{
			if( empty( $_POST['Post']['image']))
				unset( $_POST['Post']['image']);

			$model->attributes=$_POST['Post'];

			$fileImage = CUploadedFile::getInstance($model, 'image');
			$imagePath = NULL;
            if($fileImage)
            {
                $model->image = md5($fileImage->name . time()). "_". $fileImage->name . '.'. $fileImage->extensionName;
            	$imagePath = Yii::app()->params['post']['image_path'] . DIRECTORY_SEPARATOR; 
            	$imagePath = YiiBase::getPathOfAlias('webroot'). DIRECTORY_SEPARATOR. $imagePath;
	            if(!is_dir($imagePath))
	                mkdir($imagePath, 0777, true);    
            }

			if($model->save())
			{
				if ($fileImage) 
                    $fileImage->saveAs( $imagePath . $model->image);

                Yii::app()->user->setFlash('update-success', Yii::t('app', 'updated successfully!'));
                $this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id, 'Post');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Post']))
		{
			if( empty( $_POST['Post']['image']))
				unset( $_POST['Post']['image']);

			$model->attributes=$_POST['Post'];
			$fileImage = CUploadedFile::getInstance($model, 'image');

			$imagePath = NULL;
            if(!empty($fileImage) )
            {
                $model->image = md5($fileImage->name . time()). "_". $fileImage->name . '.'. $fileImage->extensionName;
                $imagePath = Yii::app()->params['post']['image_path'] . DIRECTORY_SEPARATOR;
                $imagePath = YiiBase::getPathOfAlias('webroot'). DIRECTORY_SEPARATOR. $imagePath;
	            if(!is_dir($imagePath))
	                mkdir($imagePath, 0777, true);
            }

			if($model->save())
			{
				if ($fileImage) 
                    $fileImage->saveAs( $imagePath . $model->image);

				Yii::app()->user->setFlash('update-success', Yii::t('app', 'updated successfully!'));
				$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id, 'Post')->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionIndex() {	//Yii::app()->language = 'en';
		$dataProvider = new CActiveDataProvider('Post');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin()
	{

		$model=new Post('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Post']))
			$model->attributes=$_GET['Post'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	// public function loadModel($id)
	// {
	// 	$model=Post::model()->findByPk($id);
	// 	if($model===null)
	// 		throw new CHttpException(404,'The requested page does not exist.');
	// 	return $model;
	// }

	public function generateTestData()
	{
		for( $i = 0; $i < 50; $i ++ )
		{
			
			$item = new Post();
			$item->title = 'Post Name' . ( $i + 1 );
			$item->content = 'Content' . md5(rand());
			$item->description = 'description' . md5(rand());
			$item->is_published = true;
			$item->created_by = Yii::app()->user->id;
			$item->created_at = date('Y-m-d H:i:s', time());
			$item->save();
		}
	}
}