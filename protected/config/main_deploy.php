
<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
//Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'AnAstores',
	'defaultController' => 'site/home',
	'sourceLanguage' => 'en',
	'language' => 'en',

	// preloading 'log' component
	'preload'=>array('log'),

	'aliases' => array(
        //'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'), // change this if necessary
        'vendor'  => dirname(__FILE__) . '/../vendor',
    	'i18n-columns' => 'vendor.neam.yii-i18n-columns',
    ),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.giix-components.*', // giix components
		'i18n-columns.behaviors.I18nColumnsBehavior',
		//'i18n-columns.commands.I18nColumnsCommand',
		// 'bootstrap.helpers.TbHtml',
		// 'bootstrap.helpers.*',
		// 'bootstrap.widgets.*',
		'ext.yii-mail.YiiMailMessage',
		//'application.vendor.swiftmailer.lib.swift_required',


	),
	//'theme' => 'bootstrap',
	'modules'=>array(
		'auth' => array(
			// 'strictMode' => true, // when enabled authorization items cannot be assigned children of the same type.
		 //  	'userClass' => 'User', // the name of the user model class.
		 //  	'userIdColumn' => 'id', // the name of the user id column.
		 //  	'userNameColumn' => 'name', // the name of the user name column.
		 //  	'defaultLayout' => 'application.views.layouts.main', // the layout used by the module.
		 //  	'viewDir' => null, // the path to view files to use with this module.
		),

		// uncomment the following to enable the Gii tool		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'shoppy',
			//'generatorPaths' => array('bootstrap.gii'),

			// default
			'generatorPaths' => array(
				'ext.giix-core', // giix generators
			),
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1', '192.168.1.*', '192.168.0.*'),
		),
		
	),

	// application components
	'components'=>array(
		// 'bootstrap' => array(
  //           'class' => 'bootstrap.components.TbApi',   
  //       ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class' => 'auth.components.AuthWebUser',
      		'admins' => array('admin', 'foo', 'bar'), // users with full access
		),

		'authManager'=>array(
            //'class'=>'CDbAuthManager',
            'class'=>'auth.components.CachedDbAuthManager',
            'connectionID'=>'db',
            'cachingDuration'=>3600,
            'behaviors' => array(
		        'auth' => array(
		          	'class' => 'auth.components.AuthBehavior',
		        ),
		    ),
        ),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'dang-ky' => 'site/register',
				'lien-he' => 'site/contact',
				'san-pham' =>'list/listRandomCategory',
				'san-pham/<id:\d+>/<title>' =>'list/listCategoryProduct',
				'chi-tiet/<id:\d+>/<title>' =>'product/view',
				'san-pham/gio-hang' =>'product/viewCart',

				'thanh-toan' => 'checkout/checkout',
				'thanh-toan/thong-tin-giao-hang' => 'checkout/info',
				'thanh-toan/hoan-tat' => 'checkout/finish',

				'gii'=>'gii',
	            'gii/<controller:\w+>'=>'gii/<controller>',
	            'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',


				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			'urlSuffix' => '.html',
			'showScriptName' => false,
		),
		
		// 'db'=>array(
		// 	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		// ),
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=shoppy',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// array(
	   //              'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
	   //              'ipFilters'=>array('127.0.0.1','::1', '192.168.1.*', '192.168.0.*'),
	   //              //'ipFilters'=>array('127.0.0.1','192.168.1.215'),
	   //          ),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'mail' => array(
				'class' => 'ext.yii-mail.YiiMail',
				'transportType'=>'smtp',
				'transportOptions'=>array(
					'host'=>'smtp.gmail.com',
					'username'=>'lethuyoanhbt@gmail.com', // change
					'password'=>'ok1910075*', // change
					'port'=>'465',
					'encryption'=>'ssl',
				),
				'viewPath' => 'application.views.mail',   
			// 'viewPath' => 'application.views.mail',
			// 'logging' => true,
			// 'dryRun' => false
		),

		// 'Smtpmail'=>array(
  //           'class'=>'application.extensions.smtpmail.PHPMailer',
  //           'Host'=>"smtp.gmail.com",
  //           'Username'=>'lethuyoanhbt@gmail.com',
  //           'Password'=>'ok1910075*',
  //           'Mailer'=>'smtp',
  //           'Port'=>587,
  //           'SMTPAuth'=>true,
  //           'SMTPSecure' => 'tls',
  //       ),
		'langHandler' => array(
			'languages' => array('en', 'vi')
		)

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'bondvodkamartini@gmail.com',
		'general' => array(
			'currency' => '',
			'thumb_filename' => 't.php',
			'image_path' => '/images',
			'banner_path' => '/images/banner',
			'no_image' => '/images/nocover.png',
			'item_per_page' => 9
		),
		'product' => array(
			//'image_path' => YiiBase::getPathOfAlias(Yii::app()->baseUrl). '/product'
			'image_path' => '/images/product'
		),
		'category' => array(
			'image_path' => '/images/category'
		),
		'order' => array(
			'checkout_mail_address' => 'checkout@anastores.vn',
			'owner_email' => array(
				//'bondvodkamartini@gmail.com', 
				'ndhdang@gmail.com',
				'bondvodkamartini@gmail.com'
			)
		),
		'account' => array(
			'account_mail_address' => 'account@anastores.vn'
		),


	),

);



/****************************************
http://www.yiiframework.com/extension/auth - https://github.com/Crisu83/yii-auth
http://www.yiiframework.com/extension/bootstrap - http://www.cniska.net/yii-bootstrap/index.html
http://www.yiiframework.com/extension/giix
http://www.yiiframework.com/extension/i18n-columns/
http://www.yiiframework.com/wiki/468/send-mail-using-yiimail-extension/

http://www.getyiistrap.com/site/started


http://www.ericmmartin.com/projects/simplemodal/

http://www.dropzonejs.com/
*****************************************/