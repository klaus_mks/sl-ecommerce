<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',
	
	'sourceLanguage' => 'en',
	'language' => 'en',

	// preloading 'log' component
	'preload'=>array('log'),

	'aliases' => array(
        //'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'), // change this if necessary
        'vendor'  => dirname(__FILE__) . '/../vendor',
    	'i18n-columns' => 'vendor.neam.yii-i18n-columns',
    ),

	// importing this block is a must for yii-i18n-columns
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.giix-components.*', // giix components
		'i18n-columns.behaviors.I18nColumnsBehavior',
		//'i18n-columns.commands.I18nColumnsCommand',
		// 'bootstrap.helpers.TbHtml',
		// 'bootstrap.helpers.*',
		// 'bootstrap.widgets.*',


	),

	// application components
	'components'=>array(
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=shoppy',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
		),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
		
	),
	'commandMap' => array(
	    'i18n-columns'    => array(
	        'class' => 'i18n-columns.commands.I18nColumnsCommand',
	    ),
	),
		
);