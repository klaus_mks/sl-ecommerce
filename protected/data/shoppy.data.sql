-- MySQL dump 10.13  Distrib 5.5.34, for Linux (x86_64)
--
-- Host: localhost    Database: shoppy
-- ------------------------------------------------------
-- Server version	5.5.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `AuthAssignment`
--

LOCK TABLES `AuthAssignment` WRITE;
/*!40000 ALTER TABLE `AuthAssignment` DISABLE KEYS */;
INSERT INTO `AuthAssignment` VALUES ('admin','1',NULL,'N;'),('shop','2',NULL,'N;'),('shop','3',NULL,'N;');
/*!40000 ALTER TABLE `AuthAssignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `AuthItem`
--

LOCK TABLES `AuthItem` WRITE;
/*!40000 ALTER TABLE `AuthItem` DISABLE KEYS */;
INSERT INTO `AuthItem` VALUES ('admin',2,'',NULL,'N;'),('shop',2,'',NULL,'N;'),('user',2,'',NULL,'N;');
/*!40000 ALTER TABLE `AuthItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,NULL,'admin@mail.com','$2a$10$4ae5fa9842a3075ccf17cb4eb8184cff9e738387','$2a$10$4ae5fa9842a3075ccf17cOGplHKQUsZFUOGBTRBw.wgOnQ5WPuiee','First name1','Last name1',NULL,NULL,1,NULL,NULL,0,NULL,'0000-00-00 00:00:00',NULL),(2,NULL,1,'shop1@mail.com','$2a$10$780a0c63ec086006fb56371aaf1edc45596723c6','$2a$10$780a0c63ec086006fb563uXR73tWCKeIvpkdTvS/1jE0uaVp6RlA.','First name2','Last name2',NULL,NULL,1,NULL,NULL,0,NULL,'0000-00-00 00:00:00',NULL),(3,NULL,2,'shop2@mail.com','$2a$10$5d44d587d5073847f09d8f106a1305519f760441','$2a$10$5d44d587d5073847f09d8ebFMfjYvPqVDAyVaYe26yAxO6JPSjog.','First name3','Last name3',NULL,NULL,1,NULL,NULL,0,NULL,'0000-00-00 00:00:00',NULL),(4,NULL,NULL,'user4@mail.com','$2a$10$ca9e6b589572045ec7092fa7153174408dd434fd','$2a$10$ca9e6b589572045ec7092eSEg7H9sS/41LII9vL.eJNs76yXXOYsa','First name4','Last name4',NULL,NULL,1,NULL,NULL,0,NULL,'2014-08-15 15:47:46',NULL),(5,NULL,NULL,'user5@mail.com','$2a$10$e64215c40a7ee759ed9b1fbfcbd4582d79c2dd08','$2a$10$e64215c40a7ee759ed9b1eYHRa61Rpj4mDRD92j2tUNX/9IdWzRAS','First name5','Last name5',NULL,NULL,1,NULL,NULL,0,NULL,'0000-00-00 00:00:00',NULL),(12,NULL,NULL,'dominguezmoon@yahoo.com','$2a$10$c3bb156b07a71452390f6452309dab5cb485f2bf','$2a$10$c3bb156b07a71452390f6ueQzlPiHYx6z0b0kHjNq4eZDfL3PXQqS','Klaus','Talves','84902123456','1992-09-04 16:04:51',NULL,1,NULL,0,NULL,'2014-08-03 16:04:51','2014-08-03 16:04:51');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;


/*Data for the table `setting` */
insert  into `setting`(`id`,`name`,`address`,`phone`,`cellphone`,`logo`,`google_maps`,`longtitude`,`latitude`,`contact_content`,`fb_id`,`email`,`skype_id`,`yahoo_id`,`currency`,`meta_description`,`meta_keyword`,`meta_title`) values (1,'My New Store','123 Mystic Grill, Mystic Fall','84902123456','123456789','mystore.png','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7835.3545937725685!2d106.7086312277326!3d10.912111205938444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d79b9764894f%3A0x51d86bd9af584108!2zOTcgxJDDtG5nIE5ow6wsIEzDoWkgVGhpw6p1LCB0eC4gVGh14bqtbiBBbiwgQsOsbmggRMawxqFuZywgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1406775936134\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\"></iframe>','123','4124','orem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tempor, urna vel ullamcorper venenatis, mauris lacus pharetra nibh, nec fringilla libero nibh vel dui. \r\n\r\norem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tempor, urna vel ullamcorper venenatis, mauris lacus pharetra nibh, nec fringilla libero nibh vel dui. ','https://www.facebook.com','store@mystore.com','skype','yahoo','VND','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in convallis mi, et convallis urna. Mauris ac ipsum vehicula dui suscipit consequat. In luctus, i','valar morghulis valar dohaeris','meta title this is title');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-07 23:02:29



