$(function(){
	// form submission
	$(document).on('click', 'a.lnk-submit', function(){
		$(this).closest('form').submit();
	});
});

var Common = {
	showAlert: function( containerClass, dataClass ){
		$(containerClass + ', ' + dataClass).fadeIn();
		setTimeout(function(){
			$(containerClass + ', ' + dataClass).fadeOut();
		}, 3000 );
	}
};
