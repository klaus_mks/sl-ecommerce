var _throttleTimer = null;
var _throttleDelay = 100;
var $window = $(window);
var $document = $(document);

function ScrollHandler(e) {
    //throttle event:
    clearTimeout(_throttleTimer);
    //console.log('123123');
    _throttleTimer = setTimeout(function () {
        //console.log('scroll');

        //do work
        //if ($window.scrollTop() + $window.height() > $document.height() - 200) {
        if ($window.scrollTop() + window.innerHeight > $document.height() - 300) { 
            LazyLoad.loadItem();
        }

    }, _throttleDelay);
}

$document.ready(function () {
    $window
        .off('scroll', ScrollHandler)
        .on('scroll', ScrollHandler);
});

var LazyLoad = {
	loadItem: function(){
		if($.active > 0 )
			return false;
		if($('#eol').length)
			return false;
		$.ajax({
			url: $('#url').val(),
			type: "post",
			data: { offset: $('#new-offset').val() },
			dataType: "json",
			success: function(response){
				$('.lazyload-dest').append( response.html );
				$('#new-offset').val( response.offset );
				$(document).trigger({
					type: 'afterLoad'
				});
			}
		});
	}
};



// var LazyLoad = {

// 	initialize: function(){
// 		// $(window).scroll(function() {   
// 		//    	if($(window).scrollTop() + $(window).height() == $(document).height() - 200) {
// 		//        	alert("bottom!");
// 		//    	}
// 		// });		

// 		$(window).scroll(function() {
// 		   if($(window).scrollTop() + window.innerHeight ==  $(document).height() + 300) {
// 		       alert("bottom!");
// 		   }
// 		});

// 	}
// };

// $(document).ready(function(){
// 	LazyLoad.initialize();
// });

