var ShoppingCart = {
	validateCartForm: function(containerSelector){
		$(containerSelector + ' .selection-error').hide();
		var res = true;
		var container = $(containerSelector); 
		var attrGrp = $(containerSelector + ' .attribute-group');
		attrGrp.each(function(index, value){
			if($(value).find('input:checked').length == 0)
			{
				$(value).find('.selection-error').hide();
				$(value).find('.selection-error').show();
				res = false;
			}
		});

		// check existing
		var quantity = $(containerSelector + ' .selection-quantity');
		if(!quantity.find('input').val())
		{
			quantity.find('.selection-error').hide();
			quantity.find('.selection-error').show();
			res = false;
		}

		// check numeric
		if( quantity.find('input').val() && !$.isNumeric(quantity.find('input').val()) || quantity.find('input').val() > 50 )
		{
			quantity.find('.numeric-error').hide();
			quantity.find('.numeric-error').show();
			res = false;	
		}


		return res;
	},

	changeQuantity: function(){
		$(document).on('change', '.quantity-input',function(){
			var form = $(this).closest('form');
			var refreshLink = form.attr('refresh-link');

			$.ajax({
				url: form.attr('action'),
				type: "post",
				data: form.serialize(),
				dataType: "json",
				success: function(response){
					if(response.res)
					{
						$.get('', function(data){
							$('div.container.cart').html(data);
						});
					}
				}
			});
		});
	},

	addToCart: function(){
		$('body').on('click', 'a.add-to-cart', function(e){
			$.ajax({
				url: $('#add-cart-url').val(),
				type: "post",
				data: {
					id : $(this).attr('id'),
					name: $(this).attr('name')
				},
				dataType: "html",
				success: function(response){
					if(e.currentTarget.hasAttribute('mobile'))
					{
						$('.modal-box .data-container').html(response);
						$(".modal-box").slideToggle("fast");
      					$('body').toggleClass("position-f");	    	
					}
					else 
					{
						$('#shopping-cart-modal').html(response);
						$('#shopping-cart-modal').modal({
							escClose: true,
							overlayClose: true,
							//minHeight: $('#product-selection').height()
							//minHeight: $(window).height()
							// autoResize: true,		
							minHeight:550,
							// minWidth: 800,
							onShow: function(dialog){
								

								$('#simplemodal-container').css('position', 'absolute');
								//  console.log($('#product-selection').height());
								var height = $('#product-selection').height();
								if( height > dialog.container.height())
									dialog.container.height($('#product-selection').height());
								//dialog.container.height($('#product-selection').height());
								//dialog.container.height(100);
							}
						});
					}
				}
			});

			e.preventDefault();

		});

		$('body').on('click', 'a.save-cart-item', function(e){
			if(!ShoppingCart.validateCartForm('#product-selection'))
				return false;

			var form = $(this).closest('form');
			$.ajax({
				url: form.attr('action'),
				type: "post",
				data: form.serialize(),
				dataType: "json",
				success: function(response){
					if(e.currentTarget.hasAttribute('mobile'))
					{
						$('.cart-item-no').html(response.total);
					
						$(".modal-box").slideToggle("normal");
	      				$('body').removeClass("position-f");	    

						Common.showAlert('.global-alert', '.add-to-cart-success');		
					}
					else
					{
						$('.cart-item-no').html(response.total);
						Common.showAlert('.global-alert', '.add-to-cart-success');	
						$.modal.close();
					}
					
					
				}
			});
		});

	},
	
	removeFromCart: function(){
		// $('body').on('click', 'a.remove-from-cart', function(){
		// 	$.ajax({
		// 		url: $('#remove-cart-url').val(),
		// 		type: "post",
		// 		data: {
		// 			id : $(this).attr('id'),
		// 		},
		// 		dataType: "json",
		// 		success: function(response){
		// 			window.location = response.url;

		// 		}
		// 	});
		// });
		$('body').on('click', 'a.remove-from-cart', function(){
			var form = $(this).closest('form');
			// var id = $(this).closest('.item-attribute').find('input[name="id"]').val();
			// var attribute = $(this).closest('.item-attribute').find('input[name="attribute"]').val();
			//var attribute_value = $(this).closest('.item-attribute').find('input[name="attribute_value[]"]').val();
			$.ajax({
				//url: form.attr('action'),
				url: $('#remove-cart-url').val(),
				type: "post",
				data: form.serialize(),
				dataType: "json",
				success: function(response){
					window.location = response.url;

				}
			});
		});
	}
};

$(document).ready(function(){
	ShoppingCart.addToCart();
	ShoppingCart.removeFromCart();
	ShoppingCart.changeQuantity();
});