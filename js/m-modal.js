$(function(){
	var h = $(window).height();
	$('.modal-box').css('height', h + 'px');
});

$(function(){
	$(document).on('click', ".m-modal-open", function(){
      	$(".modal-box").slideToggle("fast");
      	$('body').toggleClass("position-f");	    
    });

    $(document).on('click', ".m-modal-close", function(){
      	$(".modal-box").slideToggle("normal");
      	$('body').removeClass("position-f");	    
    });

});